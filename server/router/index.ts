const Router = require('express')

import user from './userRouter'
import tweets from './tweetsRouter'
import whomtoread from './whomToReadRouter'
import topical from './topicalRouter'
import dialogs from './dialogsRouter'
import messages from './messagesRouter'
import followers from './followRouter'
import follow from './followRouter'

import uploadFile from './uploadFileRouter'

const router = new Router()

router.use('/user', user)
router.use('/tweets', tweets)
router.use('/whoomtoread', whomtoread)
router.use('/topicals', topical)
router.use('/dialogs', dialogs)
router.use('/messages', messages)
router.use('/followers', followers)
router.use('/follow', follow)

router.use('/upload', uploadFile)

export default router

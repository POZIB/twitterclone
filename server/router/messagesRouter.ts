import MessagesController from '../controllers/MessagesController'
import isAuthMiddleware from '../middlewares/isAuthMiddleware'

const Router = require('express')

const router = new Router()

router.get('', isAuthMiddleware, MessagesController.getAll)
router.get('/find/:value', isAuthMiddleware, MessagesController.findByText)
router.get('/with_dialog', isAuthMiddleware, MessagesController.findWithDialog)
router.get('/count_unread', isAuthMiddleware, MessagesController.countUnreadLastMessages)
router.post('/', isAuthMiddleware, MessagesController.create)

export default router

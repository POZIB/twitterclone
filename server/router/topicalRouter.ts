import TopicalController from '../controllers/TopicalController'

const Router = require('express')

const router = new Router()

router.get('/', TopicalController.getAll)

export default router

import FollowController from '../controllers/FollowController'
import isAuthMiddleware from '../middlewares/isAuthMiddleware'
import parseUserDataBody from '../middlewares/parseUserDataBody'

const Router = require('express')

const router = new Router()

router.get('/followers', parseUserDataBody, FollowController.getFollowers)
router.get('/followings', parseUserDataBody, FollowController.getFollowings)
router.post('/', isAuthMiddleware, FollowController.follow)
router.put('/', isAuthMiddleware, FollowController.unFollow)

export default router

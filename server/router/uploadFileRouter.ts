import multer from 'multer'

import UploadFileController from '../controllers/UploadFileController'
import { uploader } from '../core/cloudinary'
import isAuthMiddleware from '../middlewares/isAuthMiddleware'

const Router = require('express')

const router = new Router()

router.post('/', isAuthMiddleware, uploader.single('file'), UploadFileController.create)
// router.delete('/', UploadFileController.delete)

export default router

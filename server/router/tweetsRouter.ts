import isAuthMiddleware from '../middlewares/isAuthMiddleware'
import parseUserDataBody from '../middlewares/parseUserDataBody'
import { tweetsValidation } from '../middlewares/validations/tweetsValidation'
import TweetsController from '../controllers/TweetsController'

const Router = require('express')

const router = new Router()

router.get('/', parseUserDataBody, TweetsController.getAll)
router.get('/:id', TweetsController.getOne)
router.post('/', isAuthMiddleware, TweetsController.create)
router.delete('/', isAuthMiddleware, tweetsValidation, TweetsController.delete)

export default router

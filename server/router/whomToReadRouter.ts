const Router = require('express')
import parseUserDataBody from '../middlewares/parseUserDataBody'

import WhomToReadController from '../controllers/WhomToReadController'

const router = new Router()

router.get('/', parseUserDataBody, WhomToReadController.get)

export default router

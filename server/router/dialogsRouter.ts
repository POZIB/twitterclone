import DialogsController from '../controllers/DialogsController'
import isAuthMiddleware from '../middlewares/isAuthMiddleware'

const Router = require('express')

const router = new Router()

router.get('/', isAuthMiddleware, DialogsController.get)
router.post('/', isAuthMiddleware, DialogsController.openDialog)
router.delete('/', isAuthMiddleware, DialogsController.delete)

export default router

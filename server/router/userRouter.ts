const Router = require('express')

import UserController from '../controllers/UserController'
import isAuthMiddleware from '../middlewares/isAuthMiddleware'
import parseUserDataBody from '../middlewares/parseUserDataBody'
import { signInValidation } from '../middlewares/validations/signInValidation'
import { sighUpValidation } from '../middlewares/validations/signUpValidation'

const router = new Router()

router.get('/check_auth', isAuthMiddleware, UserController.checkAuth)
router.post('/signIn', signInValidation, UserController.signIn)
router.post('/signUp', sighUpValidation, UserController.signUp)
router.get('/activate', UserController.activateMail)
router.get('/users', parseUserDataBody, UserController.findUsers)
router.get('/find', UserController.findUserByEmail)
router.get('/with_dialog', isAuthMiddleware, UserController.findUsersWithDialog)
router.get('/', parseUserDataBody, UserController.getUserProfile)
router.put('/', isAuthMiddleware, UserController.updateProfile)
router.get('/password_reset', UserController.redirectLinkResetPassword)
router.post('/password_reset', UserController.sendLinkResetPassword)
router.put('/password_reset', UserController.resetPassword)

export default router

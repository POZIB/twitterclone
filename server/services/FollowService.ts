import { QueryResult } from 'pg'
import db from '../core/db'
import FollowModel from '../models/FollowModel'

class FollowService {
   async getFollowers(myId: number, userName: string) {
      const query: QueryResult<FollowModel> = await db.query(
         `SELECT json_agg(json_build_object('followId', F."id", 'id', U."id", 'avatar', U."avatar", 'fullName',U."fullName", 'userName', U."userName", 'about' , U."about", 
                                                           'isFollowing', (SELECT count(*) <> 0 FROM "Followers" WHERE "Followers"."followerId" = $1 AND "Followers"."followingId" = U."id" ) )ORDER BY F."id" DESC) as users,                             
                              (SELECT  json_build_object('id', UU."id", 'avatar', UU."avatar", 'fullName', UU."fullName", 'userName', UU."userName") as "selectUser"
                           FROM "Users"  UU
                           WHERE UU."userName" = $2) 
                           FROM "Users" U   
                           JOIN  "Followers" F ON F."followerId" = U."id"  
                           WHERE F."followingId"  = (SELECT id FROM "Users" I WHERE I."userName" = $2)`,
         [myId, userName]
      )

      return query.rows[0]
   }

   async getFollowings(myId: number, userName: string) {
      const query: QueryResult<FollowModel> = await db.query(
         `SELECT json_agg(json_build_object('followId', F."id", 'id', U."id", 'avatar', U."avatar", 'fullName',U."fullName", 'userName', U."userName", 'about' , U."about", 
                                                            'isFollowing', (SELECT count(*) <> 0 FROM "Followers" WHERE "Followers"."followerId" = $1 AND "Followers"."followingId" = U."id" ) )ORDER BY F."id" DESC) as users,                             
                              (SELECT json_build_object('id', UU."id", 'avatar', UU."avatar", 'fullName', UU."fullName", 'userName', UU."userName", 'about', UU."about") as "selectUser" 
                           FROM "Users"  UU
                           WHERE UU."userName" = $2) 
                           FROM "Users" U   
                           JOIN  "Followers" F ON F."followingId" = U."id"  
                           WHERE F."followerId"  = (SELECT id FROM "Users" I WHERE I."userName" = $2)`,
         [myId, userName]
      )

      return query.rows[0]
   }

   async follow(followerId: number, followingId: number) {
      const query: QueryResult<any> = await db.query(
         'INSERT INTO "Followers" ("followerId", "followingId") values ($1, $2) RETURNING *',
         [followerId, followingId]
      )

      return query.rows[0]
   }

   async unFollow(followerId: number, followingId: number) {
      const query: QueryResult<any> = await db.query(
         `DELETE FROM "Followers" WHERE "followerId" = $1 AND "followingId" = $2 RETURNING *`,
         [followerId, followingId]
      )

      return query.rows[0]
   }
}

export default new FollowService()

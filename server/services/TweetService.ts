import { QueryResult } from 'pg'
import db from '../core/db'
import ApiError from '../exeptions/ApiError'
import TweetModel from '../models/TweetModel'

class TweetService {
   async getOne(id: number) {
      const query: QueryResult<TweetModel> = await db.query(
         `SELECT "Tweets".*,
         json_build_object('id', "Users"."id", 'create', "Users"."create", 'avatar', "Users"."avatar", 'email', "Users"."email", 'fullName', "Users"."fullName", 'userName', "Users"."userName", 'about', "Users"."about", 'website', "Users"."website", 'location', "Users"."location", 'confirmed', "Users"."confirmed") as "user" 
         FROM "Tweets" JOIN "Users" ON "Users"."id" = "Tweets"."userId" WHERE "Tweets"."id" = $1`,
         [id]
      )

      return query.rows[0]
   }

   async getAll(myId: number) {
      const query: QueryResult<TweetModel> = await db.query(
         `SELECT "Tweets".*, 
         json_build_object('id', U."id", 'create', U."create", 'avatar', U."avatar", 'email', U."email", 'fullName', U."fullName", 'userName', U."userName", 'about', U."about", 'website', U."website", 'location', U."location", 'confirmed', U."confirmed") as "user" 
         FROM "Tweets" 
         JOIN "Users" U ON U."id" = "Tweets"."userId"
         JOIN "Followers" F ON F."followerId" = $1 AND F."followingId" = U."id" 
         ORDER BY date DESC`,
         [myId]
      )

      if (!query.rows.length) {
         const query: QueryResult<TweetModel> = await db.query(
            `SELECT "Tweets".*, 
            json_build_object('id', U."id", 'create', U."create", 'avatar', U."avatar", 'email', U."email", 'fullName', U."fullName", 'userName', U."userName", 'about', U."about", 'website', U."website", 'location', U."location", 'confirmed', U."confirmed") as "user" 
            FROM "Tweets" 
            JOIN "Users" U ON U."id" = "Tweets"."userId"
            ORDER BY date DESC`
         )
         return query.rows
      }

      return query.rows
   }

   async getAllByUser(userId: number) {
      const query: QueryResult<TweetModel> = await db.query(
         `SELECT "Tweets".*, 
         json_build_object('id', "Users"."id", 'create', "Users"."create", 'avatar', "Users"."avatar", 'email', "Users"."email", 'fullName', "Users"."fullName", 'userName', "Users"."userName", 'about', "Users"."about", 'website', "Users"."website", 'location', "Users"."location", 'confirmed', "Users"."confirmed") as "user" 
         FROM "Tweets" JOIN "Users" ON "Users"."id" = "Tweets"."userId" 
         WHERE "Users"."id" = $1
         ORDER BY date DESC`,
         [userId]
      )

      return query.rows
   }

   async create(text: string, images: string[], userId: number) {
      const queryTweets: QueryResult<{ id: number }> = await db.query(
         `INSERT INTO "Tweets" (text, images, "userId") values ($1, $2, $3) RETURNING "id"`,
         [text, images, userId]
      )

      const query: QueryResult<TweetModel> = await db.query(
         `SELECT "Tweets".*, 
      json_build_object('id', "Users"."id", 'create', "Users"."create", 'avatar', "Users"."avatar", 'email', "Users"."email", 'fullName', "Users"."fullName", 'userName', "Users"."userName", 'about', "Users"."about", 'website', "Users"."website", 'location', "Users"."location", 'confirmed', "Users"."confirmed") as "user" 
      FROM "Tweets" JOIN "Users" ON "Users"."id" = "Tweets"."userId" WHERE "Tweets"."id" = $1`,
         [queryTweets.rows[0].id]
      )

      return query.rows[0]
   }

   async delete(id: number, userId: number) {
      const query: QueryResult<TweetModel> = await db.query(
         `DELETE FROM "Tweets" WHERE id = $1 AND "userId" = $2 RETURNING *`,
         [id, userId]
      )

      if (!query.rows[0]) {
         throw ApiError.BadRequest('Ошибка при удалении')
      }

      return query.rows[0]
   }
}

export default new TweetService()

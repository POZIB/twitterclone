import { QueryResult } from 'pg'
import db from '../core/db'
import { UsersFollowModel } from '../models/FollowModel'

class WhomToReadService {
   async get(myId: number) {
      const query: QueryResult<UsersFollowModel> = await db.query(
         `SELECT U."id", U."avatar", U."fullName", U."userName", U."about", (F."followingId" = U."id")  as "isFollowing"
         FROM "Users" U
         LEFT JOIN "Followers" F ON F."followerId" = $1 AND F."followingId" = U."id" 
         WHERE F."id" IS NULL AND U."id" != $1 
         ORDER BY RANDOM() LIMIT 3`,
         [myId]
      )

      return query.rows
   }
}

export default new WhomToReadService()

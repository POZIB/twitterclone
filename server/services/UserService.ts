import bcrypt from 'bcrypt'
import { QueryResult } from 'pg'
import { v4 } from 'uuid'
import db from '../core/db'
import ApiError from '../exeptions/ApiError'
import mailService from './MailService'
import UserDto from '../dtos/UserDto'
import TokenService from './TokenService'
import UserModel from '../models/UserModel'
import UserProfileModel from '../models/UserProfileModel'

class UserService {
   async checkAuth(authToken: string) {
      const userData = TokenService.Validate(authToken)

      if (!userData) {
         throw ApiError.Unauthorized()
      }

      const query: QueryResult<UserModel> = await db.query(
         `SELECT * FROM "Users" WHERE id = $1`,
         [userData.id]
      )

      if (!query.rows[0]) {
         throw ApiError.Unauthorized()
      }

      const userDto = new UserDto(query.rows[0])
      const token = TokenService.Generate({ ...userDto })

      return { token, user: userDto }
   }

   //сделать вход по email или по username
   async signIn(password: string, email: string) {
      const query: QueryResult<UserModel> = await db.query(
         `SELECT * FROM "Users" WHERE email = $1`,
         [email]
      )

      if (!query.rows[0]) {
         throw ApiError.BadRequest('Мы не нашли учетную запись с таким Email')
      }

      const isPasswordEquals = await bcrypt.compare(password, query.rows[0].password)
      if (!isPasswordEquals) {
         throw ApiError.BadRequest(`Email или Пароль не верны`)
      }

      const userDto = new UserDto(query.rows[0])
      const token = TokenService.Generate({ ...userDto })

      return { token, user: userDto }
   }

   async signUp(email: string, fullName: string, userName: string, password: string) {
      const candidateByEmail: QueryResult<UserModel> = await db.query(
         `SELECT * FROM "Users" WHERE email = $1`,
         [email]
      )

      if (candidateByEmail.rows[0]) {
         throw ApiError.BadRequest(`Учетная запись с таким Email существует`)
      }

      const candidateByUserName: QueryResult<UserModel> = await db.query(
         `SELECT * FROM "Users" WHERE "userName" = $1`,
         [userName]
      )

      if (candidateByUserName.rows[0]) {
         throw ApiError.BadRequest(`Учетная запись с таким Логином существует`)
      }

      const hashPassword = await bcrypt.hash(password, 3)
      const confirmHash = v4()

      await mailService.sendActivationMail(email, confirmHash)
      
      const newUser: QueryResult<UserModel> = await db.query(
         `INSERT INTO "Users" (email, "fullName", "userName", password, "confirmHash") 
         values ($1, $2, $3, $4, $5) RETURNING *`,
         [email, fullName, userName, hashPassword, confirmHash]
      )


      const userDto = new UserDto(newUser.rows[0])
      const token = TokenService.Generate({ ...userDto })

      return { token, user: userDto }
   }

   async activateMail(activationLink: string) {
      const query: QueryResult<UserModel> = await db.query(
         `UPDATE "Users" SET confirmed = true WHERE "confirmHash" = $1 RETURNING *`,
         [activationLink]
      )
      if (!query.rows[0]) {
         throw ApiError.BadRequest('Неккоректная ссылка активации')
      }

      return query.rows[0]
   }

   async getUserProfile(myId: number, userName: string) {
      const query: QueryResult<UserProfileModel> = await db.query(
         `SELECT U."id", U."avatar", U."avatarBackground", U."create", U."email", U."fullName", U."userName", U."about", U."website", U."location", U."create", U."lastSeen",   
         count(case when F."followerId" = U."id"  then 1 else null end) as followings,
         count(case when F."followingId" = U."id"  then 1 else null end) as followers,
         (SELECT count(*) <> 0 FROM "Followers" WHERE "Followers"."followerId" = $1 AND "Followers"."followingId" = U."id" ) as "isFollowing"
         FROM "Users" U
         LEFT JOIN "Followers" F ON U."id" = F."followerId" OR U."id" = F."followingId"
         WHERE U."userName" = $2
         GROUP BY U."id"`,
         [myId, userName]
      )

      if (!query.rows[0]) {
         return null
      }

      return query.rows[0]
   }

   async updateLastSeen(userId: number, value: string) {
      const query: QueryResult<UserModel> = await db.query(
         `UPDATE "Users" SET "lastSeen" = $1 WHERE "id" = $2 RETURNING *`,
         [value, userId]
      )
      if (!query.rows[0]) {
         throw ApiError.BadRequest('Ошибка updateLastSeen бд')
      }

      return query.rows[0]
   }

   async findUserByEmail(email: string) {
      const query: QueryResult<UserModel> = await db.query(
         `SELECT * FROM "Users" WHERE "email" = $1`,
         [email]
      )

      return query.rows[0]
   }

   async findUsers(value: string) {
      const query: QueryResult<UserModel> = await db.query(
         `SELECT * FROM "Users" WHERE "fullName" LIKE $1 OR "userName" LIKE $1 OR "email" LIKE $1`,
         ['%' + value + '%']
      )

      return query.rows
   }

   async findUsersWithDialog(myId: number, value: string) {
      const query: QueryResult<UserModel> = await db.query(
         `SELECT U.* FROM  "Dialogs" D 
         JOIN "Users" U ON  D."ownerId" = U."id" OR D."companionId" = U."id"
         WHERE 
         (LOWER(U."fullName") LIKE LOWER($1) OR LOWER(U."userName") LIKE LOWER($1) OR LOWER(U."email") LIKE LOWER($1))
         AND (D."ownerId" = $2 OR D."companionId" = $2) AND U."id" != $2  GROUP BY U."id"`,
         ['%' + value + '%', myId]
      )

      return query.rows
   }

   async updateProfile(user: UserModel) {
      const query: QueryResult<UserModel> = await db.query(
         `UPDATE "Users" SET "fullName" = $1, "about" = $2, "website" = $3, "location" = $4, "avatar" = $5, "avatarBackground" = $6
          WHERE "id" = $7 RETURNING *`,
         [
            user.fullName,
            user.about,
            user.website,
            user.location,
            user.avatar,
            user.avatarBackground,
            user.id,
         ]
      )

      return query.rows[0]
   }

   async resetPassword(email: string, password: string) {
      const query: QueryResult<UserModel> = await db.query(
         `UPDATE "Users" SET "password" = $1 WHERE "email" = $2 RETURNING *`,
         [password, email]
      )

      return query.rows[0]
   }
}

export default new UserService()

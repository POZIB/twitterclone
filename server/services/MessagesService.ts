import { QueryResult } from 'pg'
import db from '../core/db'

import MessageModel from '../models/MessageModel'

class MessagesService {
   async getAll(dialogId: number) {
      const query: QueryResult<MessageModel[]> = await db.query(
         `SELECT * FROM "Messages" WHERE "dialogId" = $1 ORDER BY "createdAt"`,
         [dialogId]
      )
      return query.rows
   }

   async create(
      text: string,
      images: string[],
      voice: string,
      dialogId: number,
      authorId: number
   ) {
      const query: QueryResult<MessageModel> = await db.query(
         'INSERT INTO "Messages" (text, images, voice, "dialogId", "authorId") values ($1, $2, $3, $4, $5) RETURNING *',
         [text, images, voice || '', dialogId, authorId]
      )
      return query.rows[0]
   }

   async update(id: number, text: string, images?: string[]) {
      const query: QueryResult<MessageModel> = await db.query(
         `UPDATE "Messages" SET "text" = '${text}', "images" = '{${images}}', "isChanged" = true WHERE "id" = '${id}' RETURNING *`
      )

      return query.rows[0]
   }

   async delete(id: number, userId: number) {
      const query: QueryResult<MessageModel> = await db.query(
         `DELETE FROM "Messages" WHERE id = $1 AND "authorId" = $2 RETURNING *`,
         [id, userId]
      )

      return query.rows[0]
   }

   async lastMessage(dialogid: number) {
      const query: QueryResult<MessageModel> = await db.query(
         `SELECT distinct on("dialogId") * FROM "Messages" WHERE "dialogId" = $1 order by "dialogId", "createdAt" DESC `,
         [dialogid]
      )
      return query.rows[0]
   }

   async findByText(value: string) {
      const query: QueryResult<MessageModel> = await db.query(
         `SELECT M.*,
         json_build_object('id', U."id", 'fullName', U."fullName", 'userName',  U."userName", 'avatar', U."avatar", 'lastSeen', U."lastSeen", 'create', U."create") as "author"
         FROM "Messages" M
         JOIN "Users" U ON U."id" = M."authorId"
         WHERE "text" LIKE $1`,
         ['%' + value + '%']
      )
      return query.rows
   }

   async findWithDialog(myId: number, value: string) {
      const query: QueryResult<MessageModel> = await db.query(
         `SELECT  M.*, 
         json_build_object('id', U."id", 'fullName', U."fullName", 'userName',  U."userName", 'avatar', U."avatar") as "author"
         FROM "Messages" M
         JOIN "Dialogs" D ON  D."id" = M."dialogId"
         JOIN "Users" U ON 
                     CASE 
                        WHEN D."ownerId" = $1
                        THEN D."companionId" = U."id"	
                        WHEN D."companionId" = $1
                        THEN D."ownerId" = U."id"
                     END
         WHERE LOWER(M."text") Like LOWER($2) AND (D."companionId" = $1 OR D."ownerId" = $1) 
         ORDER BY M."createdAt" DESC`,
         [myId, '%' + value + '%']
      )

      return query.rows
   }

   async countUnreadLastMessages(myId: number) {
      const query: QueryResult<{ count: number }> = await db.query(
         `SELECT COUNT(*) 

         FROM (SELECT distinct on("dialogId") * FROM "Messages" WHERE "Messages"."authorId" != $1 order by "dialogId", "createdAt" DESC ) M 
         JOIN "Dialogs" D ON M."dialogId" = D."id" 
         
         WHERE M."isRead" = false AND (D."ownerId" = $1 OR D."companionId" = $1)`,
         [myId]
      )
      return query.rows[0].count
   }

   async reading(dialogId: number, myId: number) {
      const query: QueryResult<MessageModel> = await db.query(
         `UPDATE "Messages" SET "isRead" = true WHERE "isRead" = false AND "dialogId" = $1 AND "authorId" != $2 RETURNING *`,
         [dialogId, myId]
      )
      return query.rows
   }
}

export default new MessagesService()

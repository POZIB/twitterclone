import jwt from 'jsonwebtoken'
import UserDto from '../dtos/UserDto'
require('dotenv').config()

class TokenService {
   static Generate(payload: UserDto, expires: string = '3d') {
      const token = jwt.sign(payload, String(process.env.JWT_ACCESS_SECRET), {
         expiresIn: expires,
      })

      return token
   }

   static Validate(token?: string) {
      try {
         if (!token) return null

         const userData = jwt.verify(token, String(process.env.JWT_ACCESS_SECRET))

         return userData as UserDto
      } catch (error) {
         return null
      }
   }
}
export default TokenService

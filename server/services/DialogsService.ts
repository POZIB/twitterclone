import { QueryResult } from 'pg'
import db from '../core/db'
import DialogModel from '../models/DialogModel'
import UserModel from '../models/UserModel'

class DialogsService {
   async get(ownerId: number) {
      //присоединять к каждому диалогу последнее сообщение в диалоге c его отправителем и сорировать диалоги по дате последнего сообщения
      const query: QueryResult<DialogModel> = await db.query(
         `SELECT  
         D.*, 
         COUNT(CM."isRead") as "countUnreadMessages",
         json_build_object('id', U."id", 'fullName', U."fullName", 'userName',  U."userName", 'avatar', U."avatar", 'lastSeen', U."lastSeen", 'create', U."create") as "companion",
         json_build_object('id', M."id",
          'text', COALESCE(NULLIF(M."text", ''), CONCAT((CASE WHEN M."authorId" != U."id" THEN 'Вы отправили ' ELSE 'Получено 'END ),
                                                         (CASE WHEN M."voice" != '' THEN 'голосовое сообщение' END ),
                                                         (CASE WHEN M."images" != '{}' THEN CONCAT(cardinality(M."images"), ' фото') END )
                                                )),
            'createdAt', M."createdAt", 'authorId', M."authorId", 'isRead', M."isRead") as "lastMessage"
         FROM "Dialogs" D 
         LEFT JOIN (SELECT distinct on("dialogId") * FROM "Messages" order by "dialogId", "createdAt" DESC) as M ON M."dialogId" = D."id"
         LEFT JOIN "Messages" CM ON CM."dialogId" = D."id" AND CM."isRead" = false AND  CM."authorId" != $1
         JOIN "Users" U ON  
                        CASE 
                           WHEN D."ownerId" = $1
                           THEN D."companionId" = U."id"	
                           WHEN D."companionId" = $1 
                           THEN D."ownerId" = U."id"
                        END
         WHERE D."ownerId" = $1 OR D."companionId" = $1
         GROUP BY  D."id", M."id", M."text",  M."createdAt",  M."authorId", M."isRead", M."images", M."voice", U."id"
         ORDER BY M."createdAt" DESC`,
         [ownerId]
      )
      return query.rows
   }

   async getOneByIdAndUserId(id: number, userId: number) {
      //присоединять к каждому диалогу последнее сообщение в диалоге c его отправителем и сорировать диалоги по дате последнего сообщения
      const query: QueryResult<DialogModel> = await db.query(
         `SELECT  
         D.*, 
         COUNT(CM."isRead") as "countUnreadMessages",
         json_build_object('id', U."id", 'fullName', U."fullName", 'userName',  U."userName", 'avatar', U."avatar", 'lastSeen', U."lastSeen", 'create', U."create") as "companion",
         json_build_object('id', M."id", 
         'text', COALESCE(NULLIF(M."text", ''), CONCAT((CASE WHEN M."authorId" != U."id" THEN 'Вы отправили ' ELSE 'Получено 'END ),
                                                      (CASE WHEN M."voice" != '' THEN 'голосовое сообщение' END ),
                                                      (CASE WHEN M."images" != '{}' THEN CONCAT(cardinality(M."images"), ' фото') END )
                                                )),
         'createdAt', M."createdAt", 'authorId', M."authorId", 'isRead', M."isRead") as "lastMessage"
         FROM "Dialogs" D 
         LEFT JOIN (SELECT distinct on("dialogId") * FROM "Messages" order by "dialogId", "createdAt" DESC) as M ON M."dialogId" = D."id"
         LEFT JOIN "Messages" CM ON CM."dialogId" = D."id" AND CM."isRead" = false AND CM."authorId" != $1
         JOIN "Users" U ON  
                        CASE 
                           WHEN D."ownerId" = $1
                           THEN D."companionId" = U."id"	
                           WHEN D."companionId" = $1
                           THEN D."ownerId" = U."id"
                        END
         WHERE D."id" = $2
         GROUP BY  D."id", M."id", M."text",  M."createdAt",  M."authorId", M."isRead", M."images", M."voice", U."id"`,
         [userId, id]
      )
      return query.rows[0]
   }

   async getCompanion(dialogId: number, userId: number) {
      const query: QueryResult<UserModel> = await db.query(
         `SELECT U.*
         FROM "Dialogs" D
         JOIN "Users" U ON CASE 
                              WHEN D."ownerId" = $1
                              THEN D."companionId" = U."id"	
                              WHEN D."companionId" = $1 
                              THEN D."ownerId" = U."id"
				               END
         WHERE D."id" = $2 AND (D."companionId" = $1 OR D."ownerId" = $1)`,
         [userId, dialogId]
      )
      return query.rows[0]
   }

   async openDialog(ownerId: number, companionId: number) {
      const query: QueryResult<{ id: number }> = await db.query(
         `
         SELECT id FROM "Dialogs" WHERE ("ownerId" = $1 AND "companionId" = $2) OR ("ownerId" = $2 AND "companionId" = $1)`,
         [ownerId, companionId]
      )

      if (query.rows[0]) {
         return query.rows[0].id
      } else {
         return null
      }
   }

   async create(id: number, ownerId: number, companionId: number) {
      const query: QueryResult<DialogModel> = await db.query(
         `INSERT INTO "Dialogs" (id, "ownerId",  "companionId") values ($1, $2, $3) RETURNING *`,
         [id, ownerId, companionId]
      )
      return query.rows[0]
   }

   async delete(id: number, userId: number) {
      const query: QueryResult<DialogModel> = await db.query(
         `DELETE FROM "Dialogs" WHERE id = $1 AND "userId" = $2 RETURNING *`,
         [id, userId]
      )
      //сделать каскадное удаление сообщений при удаления диалога
      return query.rows[0]
   }
}

export default new DialogsService()

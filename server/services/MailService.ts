import nodemailer from 'nodemailer'
import ApiError from '../exeptions/ApiError'
require('dotenv').config()

class MailService {
   transporter: any
   constructor() {
      this.transporter = nodemailer.createTransport({
         host: process.env.SMTP_HOST,
         auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASSWORD,
         },
      })
   }

   async sendActivationMail(to: string, link: string) {
      try {
         await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to,
            subject: 'Активация аккаунта TwitterClone',
            html: `<div>Для активации аккаунта перейдите <a href=${process.env.API_URL}/user/activate?hash=${link}>по этой ссылке</a><div>`,
         })
      } catch (error) {
         console.log(error);
         throw ApiError.BadRequest('Неудачная отправка сообщения активации на почту')
      }
   }

   async sendLinkResetPassword(toEmail: string, userName: string, link: string) {
      try {
         await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: toEmail,
            subject: 'Запрос на сброс пароля TwitterClone',
            html: `<div>Если вы запросили сброс пароля для @${userName}, перейдите по <a href=${process.env.API_URL}/user/password_reset?link=${link}>ссылке</a>, чтобы завершить процесс.
             Если вы не отправляли этот запрос, проигнорируйте это письмо.<div>`,
         })
      } catch (error) {
         console.log(error);  
         throw ApiError.BadRequest('Неудачная отправка сообщения сброса пароля на почту')
      }
   }
}

export default new MailService()



CREATE TABLE "Users" (
  id SERIAL PRIMARY KEY,
  "create" timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  avatar VARCHAR(200),
  "avatarBackground" VARCHAR(200),
  email VARCHAR(40) NOT NULL UNIQUE,
  "fullName" VARCHAR(40) NOT NULL,
  "userName" VARCHAR(40) NOT NULL UNIQUE,
  password VARCHAR(255) NOT NULL,
  about VARCHAR(255),
  website VARCHAR(155),
  location VARCHAR(155),
  confirmed BOOLEAN DEFAULT FALSE,
  "confirmHash" VARCHAR(255) UNIQUE,
  "lastSeen" VARCHAR(140)
);

CREATE TABLE "Tweets" (
  id SERIAL PRIMARY KEY,
  date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  text VARCHAR(350) NOT NULL,
  likes INTEGER,
  retweets INTEGER,
  replies INTEGER,
  images VARCHAR[],
  "userId" INTEGER,
  FOREIGN KEY ("userId") REFERENCES "Users" (Id)
);

CREATE TABLE "Dialogs" (
  id SERIAL PRIMARY KEY,
  "ownerId" INTEGER,
  "companionId" INTEGER,
  "dateCreate" timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY ("ownerId") REFERENCES "Users" (Id),
  FOREIGN KEY ("companionId") REFERENCES "Users" (Id)
);

CREATE TABLE "Messages" (
  id SERIAL PRIMARY KEY,
  text VARCHAR, 
  "isRead" BOOLEAN DEFAULT FALSE,
  "images" VARCHAR[],
  "voice" VARCHAR(255),
  "isChanged" BOOLEAN DEFAULT FALSE,
  "createdAt" timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  "dialogId" INTEGER,
  "authorId" INTEGER,
  FOREIGN KEY ("dialogId") REFERENCES "Dialogs" (Id),
  FOREIGN KEY ("authorId") REFERENCES "Users" (Id)
);


CREATE TABLE "Followers" (
  id SERIAL PRIMARY KEY,
  "followerId" INTEGER,
  "followingId" INTEGER,
  FOREIGN KEY ("followerId") REFERENCES "Users" (Id),
  FOREIGN KEY ("followingId") REFERENCES "Users" (Id)
);
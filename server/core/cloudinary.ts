import cloudinary from 'cloudinary'
import multer from 'multer'

require('dotenv').config()

cloudinary.v2.config({
   cloud_name: process.env.CLOUDINARY_NAME,
   api_key: process.env.CLOUDINARY_API_KEY,
   api_secret: process.env.CLOUDINARY_API_SECRET,
})

export const storage = multer.memoryStorage()

export const uploader = multer({ storage })

export default cloudinary

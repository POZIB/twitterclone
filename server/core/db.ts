import { Pool } from 'pg'
require('dotenv').config()

const DBConfing = {
   user: process.env.DB_USER,
   password: process.env.DB_PASSWORD,
   host: process.env.DB_HOST,
   port: Number(process.env.DB_PORT),
   database: process.env.DB_NAME,
}

const prodConfig = {
   connectionString: process.env.DATABASE_URL, //heroku
   ssl: {
      rejectUnauthorized: false,
   },
}

const pool = new Pool(process.env.NODE_ENV === 'production' ? prodConfig : DBConfing)

export default pool

import { Server, Socket } from 'socket.io'
import { io } from '../index'

import { DefaultEventsMap } from 'socket.io/dist/typed-events'
import DialogModel from '../models/DialogModel'
import DialogsService from '../services/DialogsService'
import MessagesService from '../services/MessagesService'

export default (
   io: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>,
   socket: Socket
) => {
   socket.on('DIALOG:JOIN', (dialogId: number) => {
      socket.rooms.forEach(
         value => value.startsWith('DIALOG:JOIN:') && socket.leave(value)
      )
      socket.join('DIALOG:JOIN:' + dialogId)
   })

   socket.on('DIALOGS:NEW', async ({ text, images, voice, newDialog }) => {
      try {
         if (newDialog && newDialog.companion.id) {
            const dialog = await DialogsService.create(
               newDialog.id,
               newDialog.ownerId,
               newDialog.companion.id
            )

            const message = await MessagesService.create(
               text,
               images,
               voice,
               dialog.id,
               dialog.ownerId
            )
            const companionDialog = await DialogsService.getOneByIdAndUserId(
               dialog.id,
               dialog.companionId
            )
            const ownerDialog = await DialogsService.getOneByIdAndUserId(
               dialog.id,
               dialog.ownerId
            )

            io.to('USER:ID:' + dialog.companionId).emit('DIALOGS:NEW', companionDialog)
            io.to('USER:ID:' + dialog.ownerId).emit('DIALOGS:NEW', ownerDialog)
            io.to('DIALOG:JOIN:' + dialog.id).emit('MESSAGES:SEND', message)
            socket.broadcast
               .in('USER:ID:' + dialog.companionId)
               .emit('MESSAGES:NOTIFICATION', {
                  text,
                  companion: companionDialog.companion,
               })
         }
      } catch (error) {
         console.log(error)
      }
   })
}

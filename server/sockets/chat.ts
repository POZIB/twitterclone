import { Server, Socket } from 'socket.io'

import MessagesService from '../services/MessagesService'
import UserDto from '../dtos/UserDto'
import { DefaultEventsMap } from 'socket.io/dist/typed-events'
import DialogsService from '../services/DialogsService'
import MessageModel from '../models/MessageModel'
import DialogModel from '../models/DialogModel'

export default (
   io: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>,
   socket: Socket
) => {
   socket.on('MESSAGES:SEND', async ({ text, images, voice, dialogId }) => {
      try {
         const userData: UserDto = socket.data.userData

         if (userData && dialogId) {
            const newMessage = await MessagesService.create(
               text,
               images,
               voice,
               dialogId,
               userData.id
            )

            const myDialog = await DialogsService.getOneByIdAndUserId(
               newMessage.dialogId,
               userData.id
            )
            const companionDialog = await DialogsService.getOneByIdAndUserId(
               newMessage.dialogId,
               myDialog.companion.id
            )

            let _text = ''
            if (newMessage.text) {
               _text = newMessage.text
            } else {
               if (newMessage.images?.length) {
                  _text = newMessage.images?.length + ' фото'
               }
               if (newMessage.voice) {
                  _text = 'Голосовое сообщение'
               }
            }

            socket.broadcast
               .in('USER:ID:' + myDialog.companion.id)
               .emit('MESSAGES:NOTIFICATION', {
                  dialogId: newMessage.dialogId,
                  text: _text,
                  companion: userData,
               })

            io.to('USER:ID:' + userData.id).emit('DIALOGS:UPDATE', myDialog)
            io.to('USER:ID:' + myDialog.companion.id).emit(
               'DIALOGS:UPDATE',
               companionDialog
            )

            io.to('DIALOG:JOIN:' + dialogId).emit('MESSAGES:SEND', newMessage)
         }
      } catch (error) {
         console.log(error)
      }
   })

   socket.on('MESSAGES:UPDATE', async ({ id, text, images }) => {
      try {
         const userData: UserDto = socket.data.userData
         if (userData?.id) {
            const message = await MessagesService.update(id, text, images)

            const myDialog = await DialogsService.getOneByIdAndUserId(
               message.dialogId,
               userData.id
            )
            const companionDialog = await DialogsService.getOneByIdAndUserId(
               message.dialogId,
               myDialog.companion.id
            )

            io.to('USER:ID:' + userData.id).emit('DIALOGS:UPDATE', myDialog)
            io.to('USER:ID:' + myDialog.companion.id).emit(
               'DIALOGS:UPDATE',
               companionDialog
            )

            io.to('DIALOG:JOIN:' + message.dialogId).emit('MESSAGES:UPDATE', message)
         }
      } catch (error) {
         console.log(error)
      }
   })

   socket.on('MESSAGES:DELETE', async (data: MessageModel) => {
      try {
         const userData: UserDto = socket.data.userData
         if (userData && userData.id === data.authorId) {
            const delMessage = await MessagesService.delete(data.id, userData.id)

            const myDialog = await DialogsService.getOneByIdAndUserId(
               delMessage.dialogId,
               userData.id
            )
            const companionDialog = await DialogsService.getOneByIdAndUserId(
               delMessage.dialogId,
               myDialog.companion.id
            )

            io.to('USER:ID:' + userData.id).emit('DIALOGS:UPDATE', myDialog)
            io.to('USER:ID:' + myDialog.companion.id).emit(
               'DIALOGS:UPDATE',
               companionDialog
            )

            io.to('DIALOG:JOIN:' + data.dialogId).emit('MESSAGES:DELETE', delMessage)
         }
      } catch (error) {
         console.log(error)
      }
   })

   socket.on('MESSAGES:READING', async (dialogId: number) => {
      try {
         const userData: UserDto = socket.data.userData
         if (userData && userData.id && dialogId) {
            const data = await MessagesService.reading(dialogId, userData.id)

            io.to('DIALOG:JOIN:' + dialogId).emit('MESSAGES:READING', data)

            const myDialog = await DialogsService.getOneByIdAndUserId(
               dialogId,
               userData.id
            )
            const companionDialog = await DialogsService.getOneByIdAndUserId(
               dialogId,
               myDialog.companion.id
            )

            io.to('USER:ID:' + userData.id).emit('DIALOGS:UPDATE', myDialog)
            io.to('USER:ID:' + myDialog.companion.id).emit(
               'DIALOGS:UPDATE',
               companionDialog
            )
         }
      } catch (error) {
         console.log(error)
      }
   })

   socket.on('DIALOGS:RECORDING_VOICE', async (dialog: DialogModel, flag: boolean) => {
      try {
         socket
            .to('USER:ID:' + dialog.companion.id)
            .emit('DIALOGS:RECORDING_VOICE', { dialogId: dialog.id, flag })
      } catch (error) {
         console.log(error)
      }
   })

   socket.on('DIALOGS:TYPING', async (dialog: DialogModel, flag: boolean) => {
      try {
         socket
            .to('USER:ID:' + dialog.companion.id)
            .emit('DIALOGS:TYPING', { dialogId: dialog.id, flag })
      } catch (error) {
         console.log(error)
      }
   })
}

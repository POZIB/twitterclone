import { Server, Socket } from 'socket.io'
import { DefaultEventsMap } from 'socket.io/dist/typed-events'
import UserDto from '../dtos/UserDto'
import { io } from '../index'
import UserService from '../services/UserService'

export default async (
   io: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>,
   socket: Socket
) => {
   try {
      const userData: UserDto = socket.data.userData
      if (userData) {
         socket.join('USER:ID:' + userData.id)
         await UserService.updateLastSeen(userData.id, 'online')
         io.in('USER:ID:' + userData.id).emit('USER:IS_ONLINE', true)
      }

      socket.on('disconnecting', async reason => {
         if (userData) {
            await UserService.updateLastSeen(userData.id, new Date().toDateString())
            io.in('USER:ID:' + userData.id).emit('USER:IS_ONLINE', false)
         }
      })
   } catch (error) {
      console.log(error)
   }
}

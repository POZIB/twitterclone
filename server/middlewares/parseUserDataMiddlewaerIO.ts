import { Socket } from 'socket.io'
import { ExtendedError } from 'socket.io/dist/namespace'
import TokenService from '../services/TokenService'

export default async function (
   socket: Socket,
   next: (err?: ExtendedError | undefined) => void
) {
   try {
      const token = socket.handshake.auth.token?.split(' ')[1]
      if (!token) {
         next()
      }

      const userData = TokenService.Validate(token)
      socket.data.userData = userData
      next()
   } catch (error) {
      next()
   }
}

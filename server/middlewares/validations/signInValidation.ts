import { body } from 'express-validator'

export const signInValidation = [
   body('email', 'Введите Email')
      .isEmail()
      .withMessage('Неверный Email')
      .isLength({
         min: 4,
         max: 40,
      })
      .withMessage('Допустимое кол-во символов в почте от 4 до 40'),

   body('password', 'Введите пароль')
      .isString()
      .isLength({
         min: 6,
      })
      .withMessage('Минимальная длинна пароля 6 символов'),
]

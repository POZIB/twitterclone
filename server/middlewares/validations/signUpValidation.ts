import { body } from 'express-validator'

export const sighUpValidation = [
   body('email', 'Введите Email')
      .isEmail()
      .withMessage('Неверный Email')
      .isLength({
         min: 4,
         max: 40,
      })
      .withMessage('Допустимое кол-во символов в почте от 4 до 40'),

   body('fullName', 'Введите имя')
      .isString()
      .isLength({
         min: 2,
         max: 40,
      })
      .withMessage('Допустимое кол-во символов в имени от 10 до 40'),

   body('userName', 'Укажите логин')
      .isString()
      .isLength({
         min: 2,
         max: 40,
      })
      .withMessage('Допустимое кол-во символов в логине от 10 до 40'),

   body('password', 'Введите пароль')
      .isString()
      .isLength({
         min: 4,
      })
      .withMessage('Минимальная длинна пароля 6 символов'),
   // .custom((value, { req }) => {
   //    if (value !== req.body.password2) {
   //       throw new Error('Пароли не совпадают')
   //    } else {
   //       return value
   //    }
   // }),
]

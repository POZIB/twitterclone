import { body } from 'express-validator'

export const tweetsValidation = [
   body('text', 'Введите текст твита')
      .isString()
      .isLength({
         max: 280,
      })
      .withMessage('Макс 280 симоволов '),
]

import express from 'express'
import ApiError from '../exeptions/ApiError'
// const ValidationFields = require('../exeptions/validationFields')

export default function (
   err: Error,
   req: express.Request,
   res: express.Response,
   next: express.NextFunction
) {
   if (err instanceof ApiError) {
      return res
         .status(err.status || 400)
         .json({ message: err.message, errors: err.errors })
   }

   return res.status(500).json({ message: 'Непредвиденная ошибка', errors: err })
}

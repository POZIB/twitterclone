import express from 'express'
import ApiError from '../exeptions/ApiError'
import TokenService from '../services/TokenService'

export default function (
   req: express.Request,
   res: express.Response,
   next: express.NextFunction
) {
   try {
      const authHeader = req.headers.authorization

      if (!authHeader) {
         next(ApiError.Unauthorized())
         return
      }

      const user = TokenService.Validate(authHeader.split(' ')[1])

      if (!user) {
         next(ApiError.Unauthorized())
         return
      }

      req.body.user = user
      next()
   } catch (error) {
      return next(ApiError.Unauthorized())
   }
}

import express from 'express'
import TokenService from '../services/TokenService'
import UserDto from '../dtos/UserDto'

export default function (
   req: express.Request,
   res: express.Response,
   next: express.NextFunction
) {
   try {
      const authHeader = req.headers.authorization

      const userDto = TokenService.Validate(authHeader?.split(' ')[1])

      req.body.user = userDto ? userDto : ({ id: 0 } as UserDto)
      next()
   } catch (error) {
      return next()
   }
}

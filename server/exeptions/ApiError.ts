import { Result, ValidationError } from 'express-validator'

export default class ApiError extends Error {
   status?: number
   errors?: string | string[]

   constructor(status?: number, message?: string, errors?: string | string[]) {
      super(message)
      this.status = status
      this.errors = errors
   }

   static Unauthorized() {
      return new ApiError(401, 'Пользователь не авторизован')
   }

   static BadRequest(message: string, errors: string | string[] = []) {
      return new ApiError(400, message, errors)
   }

   static FailedValidation(errors: Result<ValidationError>) {
      const arr: any = errors.array().map(err => {
         err.msg
      })
      console.log(errors.array())

      return new ApiError(400, 'Ошибки валидации', arr)
   }

   static NoAccess() {
      return new ApiError(403, 'Нет доступа')
   }
}

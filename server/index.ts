import express from 'express'
import http from 'http'
import { Server, Socket } from 'socket.io'
import cors from 'cors'
import dontev from 'dotenv'

import router from './router/index'
import errorMiddleware from './middlewares/errorMiddleware'

import parseUserDataMiddlewaerIO from './middlewares/parseUserDataMiddlewaerIO'
import chat from './sockets/chat'
import registerUser from './sockets/registerUser'
import dialogs from './sockets/dialogs'

require('dotenv').config()

const PORT = process.env.PORT || 5000
const path = require('path')
const app = express()

app.use(express.json())
if (process.env.NODE_ENV === 'production') {
   app.use(express.static(path.join(__dirname + '/public')))
}

app.use(
   cors({
      credentials: true,
      origin: process.env.CLIENT_URL,
   })
)

app.use('/api', router)
if (process.env.NODE_ENV === 'production') {
   app.get('/*', (req, res) => {
      res.sendFile(path.resolve(__dirname + '/public/index.html'))
   })
}
app.use(errorMiddleware)

const server = http.createServer(app)

export const io = new Server(server, {
   cors: {
      credentials: true,
      origin: process.env.CLIENT_URL,
   },
})

const onConnection = (socket: Socket) => {
   registerUser(io, socket)
   dialogs(io, socket)
   chat(io, socket)
}

io.use(parseUserDataMiddlewaerIO).on('connection', onConnection)

const start = async () => {
   try {
      server.listen(PORT, () => {
         console.log(`SERVER START ${PORT}`)
      })
   } catch (error) {
      console.log(error)
   }
}

start()

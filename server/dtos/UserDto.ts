import UserModel from '../models/UserModel'

export default class UserDto {
   id: number
   avatar?: string
   avatarBackground?: string
   create: string
   email: string
   fullName: string
   userName: string
   confirmed: boolean
   location?: string
   about?: string
   website?: string

   constructor(model: UserModel) {
      this.id = model.id
      this.avatar = model.avatar
      this.avatarBackground = model.avatarBackground
      this.create = model.create
      this.email = model.email
      this.fullName = model.fullName
      this.userName = model.userName
      this.confirmed = model.confirmed
      this.location = model.location
      this.about = model.location
      this.website = model.website
   }
}

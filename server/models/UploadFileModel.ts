export default interface UploadFileModel {
   fileName: string
   size: number
   ext: string
   url: string
   messageId: number
   userId: number
}

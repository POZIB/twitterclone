export default interface UserModel {
   id: number
   avatar: string
   avatarBackground?: string
   create: string
   email: string
   fullName: string
   userName: string
   password: string
   confirmed: boolean
   confirmHash: string
   location?: string
   about?: string
   website?: string
   lastSeen?: string
   following?: number
   followers?: number
}

import UserModel from './UserModel'

export default interface MessageModel {
   id: number
   text: string
   isRead: boolean
   images?: string[]
   voice?: string
   isChanged: boolean
   createdAt: string
   updatedAt: string
   authorId: number
   dialogId: number
   author: UserModel
}

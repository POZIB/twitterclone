import UserModel from './UserModel'

export interface UsersFollowModel extends UserModel {
   followId?: number
   isFollowing?: boolean
}

export default interface FollowersModel {
   selectUser: UserModel
   users: UsersFollowModel[]
}

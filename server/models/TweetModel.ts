import IUser from './UserModel'

export default interface TweetModel {
   id?: number
   date?: string
   text: string
   likes?: number
   retweets?: number
   replies?: number
   iamges?: string[]
   user: IUser
}

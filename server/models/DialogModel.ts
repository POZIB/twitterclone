import IUser from './UserModel'

export default interface DialogModel {
   id: number
   ownerId: number
   companionId: number
   companion: IUser
   dateCreate: string
   countUnreadMessages: number
   lastMessage: {
      id: number
      text: string
      createdAt: string
      authorId: number
   }
}

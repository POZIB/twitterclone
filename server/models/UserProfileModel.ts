import UserModel from './UserModel'

export default interface UserProfileModel extends UserModel {
   followers?: number
   followings?: number
   isFollowing?: boolean
}

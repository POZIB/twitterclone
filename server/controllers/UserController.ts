import express from 'express'
import { validationResult } from 'express-validator'
import bcrypt from 'bcrypt'

import ApiError from '../exeptions/ApiError'
import UserModel from '../models/UserModel'
import MailService from '../services/MailService'
import TokenService from '../services/TokenService'
import UserService from '../services/UserService'
import UserDto from '../dtos/UserDto'

class UserController {
   async checkAuth(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const token = req.headers.authorization?.split(' ')[1]

         if (!token) {
            throw ApiError.BadRequest('Ошибка токена')
         }

         const userData = await UserService.checkAuth(token)

         res.cookie('token', userData.token, {
            maxAge: 30 * 24 * 60 * 60 * 1000,
            httpOnly: true,
         })

         res.json(userData)
      } catch (error) {
         res.json()
      }
   }

   async signIn(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ): Promise<void> {
      try {
         const { email, password } = req.body

         const userData = await UserService.signIn(password, email)

         res.cookie('token', userData.token, {
            maxAge: 30 * 24 * 60 * 60 * 1000,
            httpOnly: true,
         })

         res.json(userData)
      } catch (error) {
         next(error)
      }
   }

   async signUp(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ): Promise<void> {
      try {
         const errors = validationResult(req)

         if (!errors.isEmpty()) {
            throw ApiError.FailedValidation(errors)
         }

         const { email, fullName, userName, password } = req.body

         const userData = await UserService.signUp(email, fullName, userName, password)

         res.cookie('token', userData.token, {
            maxAge: 30 * 24 * 60 * 60 * 1000,
            httpOnly: true,
         })

         res.json(userData)
      } catch (error) {
         next(error)
      }
   }

   async activateMail(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const activationLink = req.query.hash

         await UserService.activateMail(String(activationLink))

         return res.redirect(String(process.env.CLIENT_URL))
      } catch (error) {
         next(error)
      }
   }

   async getUserProfile(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const Iam: UserModel = req.body.user
         const { userName } = req.query

         if (!userName) {
            next()
         }

         const user = await UserService.getUserProfile(Iam.id, String(userName))
         res.json(user)
      } catch (error) {
         next(error)
      }
   }

   async findUserByEmail(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const { email } = req.query

         if (!email) {
            next()
         }

         const users = await UserService.findUserByEmail(String(email))
         res.json(users)
      } catch (error) {
         next(error)
      }
   }

   async findUsers(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const { value } = req.query

         if (!value) {
            next()
         }

         const users = await UserService.findUsers(String(value))

         res.json(users)
      } catch (error) {
         next(error)
      }
   }

   async findUsersWithDialog(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const user: UserModel = req.body.user
         const { value } = req.query

         if (!user) {
            next()
         }

         const users = await UserService.findUsersWithDialog(user.id, String(value))
         res.json(users)
      } catch (error) {
         next(error)
      }
   }

   async updateProfile(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const { updateUser } = req.body
         if (!updateUser) {
            next()
         }

         const user = await UserService.updateProfile(updateUser)
         res.json(user)
      } catch (error) {
         next(error)
      }
   }

   async sendLinkResetPassword(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const { email } = req.body
         if (!email) {
            next()
         }

         const user = await UserService.findUserByEmail(String(email))

         if (user) {
            const link = TokenService.Generate(
               {
                  email: user.email,
                  userName: user.userName,
               } as UserDto,
               '5m'
            )
            await MailService.sendLinkResetPassword(user.email, user.userName, link)
         }

         res.json(!!user)
      } catch (error) {
         next(error)
      }
   }

   async redirectLinkResetPassword(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const link = req.query.link
         if (!link) {
            next()
         }

         const result = TokenService.Validate(String(link))
         res.redirect(
            `${process.env.CLIENT_URL}/i/flow/password_reset/confirmed?email=${
               result?.email || ''
            }&userName=${result?.userName || ''}&hash=${result ? link : 'expired'}`
         )
      } catch (error) {
         next(error)
      }
   }

   async resetPassword(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const { hash, password, password2 } = req.body
         if (!hash || password !== password2) {
            next()
         }

         const user = TokenService.Validate(hash)

         if (!user) {
            next(ApiError.BadRequest('Время изменения пароля истекло. Повторите попытку'))
         } else {
            const newPassword = await bcrypt.hash(password, 3)
            const result = await UserService.resetPassword(user.email, newPassword)
            res.json(!!result)
         }
      } catch (error) {
         next(error)
      }
   }
}

export default new UserController()

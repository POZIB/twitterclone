import express from 'express'

import ApiError from '../exeptions/ApiError'
import TopicalService from '../services/TopicalService'

class TopicalController {
   async getAll(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const items = await TopicalService.getAll()

         res.json(items)
      } catch (error) {
         next(error)
      }
   }
}

export default new TopicalController()

import express, { json } from 'express'
import cloudinary from 'cloudinary'

import cloudinaryConfig from '../core/cloudinary'
import ApiError from '../exeptions/ApiError'

import UploadFileModel from '../models/UploadFileModel'
import UserModel from '../models/UserModel'

class UploadFileController {
   async create(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const file = req.file

         // for (const file of Array.from(files as Express.Multer.File[])) {
         setTimeout(() => {
            cloudinaryConfig.v2.uploader
               .upload_stream(
                  { resource_type: 'auto' },
                  async (
                     error: cloudinary.UploadApiErrorResponse | undefined,
                     result: cloudinary.UploadApiResponse | undefined
                  ) => {
                     if (error || !result) {
                        console.log(error || result)
                        next(ApiError.BadRequest('Проблема с загрузкой вложений'))
                     }
                     if (result) {
                        res.json({
                           url: result.url,
                           fileName: Buffer.from(
                              String(file?.originalname),
                              'latin1'
                           ).toString('utf8'),
                           // width: result.width,
                           // height: result.height,
                        })
                     }
                  }
               )
               .end(file?.buffer)
         }, 100)
         // }
      } catch (error) {
         next(error)
      }
   }

   async delete(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const user: UserModel = req.body.user
         // const file = req.file
      } catch (error) {
         next(error)
      }
   }
}

export default new UploadFileController()

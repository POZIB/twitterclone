import express from 'express'

import ApiError from '../exeptions/ApiError'
import DialogsService from '../services/DialogsService'
import UserModel from '../models/UserModel'

class DialogsController {
   async get(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const owner: UserModel = req.body.user

         if (!owner.id) {
            next(ApiError.BadRequest('Ошибка диалогов'))
         }

         const dialogs = await DialogsService.get(owner.id)
         res.json(dialogs)
      } catch (error) {
         next(error)
      }
   }

   async openDialog(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const owner = req.body.user as UserModel
         const { companionId } = req.body

         if (!owner.id || !companionId) {
            next(ApiError.BadRequest('Ошибка создания диалога'))
         }

         const dialogId = await DialogsService.openDialog(owner.id, Number(companionId))
         res.json(dialogId)
      } catch (error) {
         next(error)
      }
   }

   async delete(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const user = req.body.user as UserModel
         const { id } = req.query

         if (!id) {
            next(ApiError.BadRequest('Ошибка удаления диалога'))
         }

         const dialog = await DialogsService.delete(Number(id), user.id)
         res.json(dialog)
      } catch (error) {
         next(error)
      }
   }
}

export default new DialogsController()

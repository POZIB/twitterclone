import express from 'express'
import { validationResult } from 'express-validator'
import ApiError from '../exeptions/ApiError'
import UserModel from '../models/UserModel'
import TweetService from '../services/TweetService'

class TweetController {
   async getOne(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const { id } = req.params

         if (!id) {
            res.json()
         }

         const tweet = await TweetService.getOne(Number(id))

         res.json(tweet)
      } catch (error) {
         next(error)
      }
   }

   async getAll(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const Iam: UserModel = req.body.user
         const { userId } = req.query

         if (userId) {
            const tweets = await TweetService.getAllByUser(Number(userId))
            res.json(tweets)
         } else {
            const tweets = await TweetService.getAll(Iam.id)
            res.json(tweets)
         }
      } catch (error) {
         next(error)
      }
   }

   //исправить вывод ошибки
   async create(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         // const file = req.files
         const errors = validationResult(req)

         if (!errors.isEmpty()) {
            next(ApiError.BadRequest('Проблемы с созданием твита'))
            res.json({ staus: 400, message: errors.array() })
         }
         const user: UserModel = req.body.user
         const { text, images } = req.body

         const newTweet = await TweetService.create(text, images, user.id)

         res.json(newTweet)
      } catch (error) {
         next(error)
      }
   }

   async delete(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const user: UserModel = req.body.user
         const { id } = req.query

         if (!id) {
            next(ApiError.BadRequest('Ошибка удаления'))
         }

         const deletedTweet = await TweetService.delete(Number(id), user.id)

         res.json(deletedTweet)
      } catch (error) {
         next(error)
      }
   }
}

export default new TweetController()

import express from 'express'
import { io } from '../index'

import ApiError from '../exeptions/ApiError'
import UserModel from '../models/UserModel'
import FollowersService from '../services/FollowService'

class FollowController {
   async getFollowers(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const Iam: UserModel = req.body.user
         const { userName } = req.query

         if (!userName) {
            next()
         }

         const result = await FollowersService.getFollowers(Iam.id, String(userName))
         res.json(result)
      } catch (error) {
         next(error)
      }
   }

   async getFollowings(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const Iam: UserModel = req.body.user
         const { userName } = req.query

         if (!userName) {
            next()
         }

         const result = await FollowersService.getFollowings(Iam.id, String(userName))
         res.json(result)
      } catch (error) {
         next(error)
      }
   }

   async follow(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const Iam: UserModel = req.body.user
         const { userId } = req.body

         if (!userId) {
            next()
         }

         const result = await FollowersService.follow(Iam.id, userId)
         res.json(result)
      } catch (error) {
         next(error)
      }
   }

   async unFollow(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const Iam: UserModel = req.body.user
         const { userId } = req.body

         if (!userId) {
            next()
         }

         const result = await FollowersService.unFollow(Iam.id, userId)
         res.json(result)
      } catch (error) {
         next(error)
      }
   }
}

export default new FollowController()

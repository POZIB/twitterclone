import express from 'express'
import UserModel from '../models/UserModel'
import WhomToReadService from '../services/WhomToReadService'

class WhomToReadController {
   async get(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const Iam: UserModel = req.body.user

         const items = await WhomToReadService.get(Iam.id)

         res.json(items)
      } catch (error) {
         next(error)
      }
   }
}

export default new WhomToReadController()

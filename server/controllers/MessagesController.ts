import express from 'express'

import ApiError from '../exeptions/ApiError'
import UserModel from '../models/UserModel'
import MessagesService from '../services/MessagesService'

class MessagesController {
   async getAll(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const { user } = req.body
         const { dialogId } = req.query

         if (!user || !dialogId) {
            throw ApiError.BadRequest('Ошибка сообщений')
         }

         const messages = await MessagesService.getAll(Number(dialogId))
         res.json(messages)
      } catch (error) {
         next(error)
      }
   }

   async create(req: express.Request, res: express.Response, next: express.NextFunction) {
      try {
         const user: UserModel = req.body.user
         const { text, dialogId, attachments } = req.body

         if (!dialogId || !user.id) {
            next(ApiError.BadRequest('Ошибка cоздания сообщения'))
         }

         // const message = await MessagesService.create(
         //    text,
         //    attachments,
         //    dialogId,
         //    user.id
         // )

         // res.json(message)
      } catch (error) {
         next(error)
      }
   }

   async findByText(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const { value } = req.params

         if (!value) {
            next()
         }

         const messages = await MessagesService.findByText(value)

         res.json(messages)
      } catch (error) {
         next(error)
      }
   }

   async findWithDialog(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const user: UserModel = req.body.user
         const { value } = req.query

         if (!value || !user) {
            next()
         }

         const messages = await MessagesService.findWithDialog(user.id, String(value))

         res.json(messages)
      } catch (error) {
         next(error)
      }
   }

   async countUnreadLastMessages(
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
   ) {
      try {
         const user: UserModel = req.body.user
         if (!user) {
            next()
         }

         let count: number | undefined = await MessagesService.countUnreadLastMessages(
            user.id
         )
         count = count == 0 ? undefined : count

         res.json(count)
      } catch (error) {
         next(error)
      }
   }
}

export default new MessagesController()

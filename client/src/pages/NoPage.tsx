import React from 'react'
import { styled, Typography } from '@mui/material'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   alignItems: 'center',
   justifyContent: 'center',
   flex: '1',
}))

const NoPage: React.FC = () => {
   return (
      <Wrapper>
         <Typography variant='h2'>Упс...</Typography>
         <Typography variant='h6' fontWeight='300'>
            Такой страницы не существует
         </Typography>
      </Wrapper>
   )
}

export default NoPage

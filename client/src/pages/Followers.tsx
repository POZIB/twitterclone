import React from 'react'
import { useParams, useNavigate, Link } from 'react-router-dom'
import { styled, Typography, Skeleton, Tabs, Tab, Box, Avatar } from '@mui/material'

import { useTypedSelector } from '../hooks/useTypedSelector'
import FollowApi from '../api/FollowApi'
import IFollow, { IUsersFollow } from '../models/IFollow'
import GoBackButton from '../components/UI/GoBackButton'
import Loader from '../components/Loader'
import ButtonUnFollow from '../components/UI/ButtonUnFollow'
import ButtonFollow from '../components/UI/ButtonFollow'

const Wrapper = styled('div')(({ theme }) => ({}))

const Header = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   position: 'sticky',
   top: 0,
   zIndex: 1,
   backgroundColor: 'rgba(255, 255, 255, 0.85)',
   backdropFilter: 'blur(12px)',
}))

const TabsWrapper = styled('div')(() => ({
   '& .MuiTabPanel-root': {
      padding: '0',
   },
}))

const WrapperItem = styled('div')(() => ({
   transition: 'background-color 0.2s',
   cursor: 'pointer',
   '&:hover': {
      backgroundColor: 'rgba(0,0,0, 0.03)',
   },
}))

const ContainerItem = styled('div')(() => ({
   display: 'flex',
   padding: '12px 16px',
}))

const WrapperAvatarItem = styled('div')(() => ({
   marginRight: '12px',
}))

const BodyItem = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: 1,
   minWidth: '0',
}))

const BlockNamesAndBtnItem = styled('div')(() => ({
   display: 'flex',
   justifyContent: 'space-between',
   alignItems: 'center',
}))

const BlockNamesItem = styled('div')(() => ({}))

const BlockBtnItem = styled('div')(() => ({}))

const ContentBtnUnfollowing = styled('div')(() => ({
   minHeight: '32px',
   minWidth: '32px',
   display: 'flex',
   alignItems: 'center',
   backgroundColor: 'transparent',
   padding: '0 16px',
   transition: 'background-color 0.2s, border-color 0.2s, color 0.2s',
   border: '1px solid rgb(207, 217, 222)',
   borderRadius: '9999px',
   color: 'rgb(15, 20, 25)',
   fontSize: '14px',
   fontWeight: '700',
   lineHeight: '16px',
   '&:hover': {
      backgroundColor: 'rgb(243, 33, 46, 0.1)',
      borderColor: 'rgb(253, 201, 206)',
      color: 'rgb(244, 33, 46)',
   },
}))

const BlockAboutItem = styled('div')(() => ({
   paddingTop: '4px',
   color: 'rgb(15 20 25)',
   minWidth: '0',
   '& span': {
      fontSize: '15px',
      lineHeight: '20px',
      fontWeight: 400,
      textOverflow: 'ellipsis',
      overflow: 'hidden',
      wordWrap: 'break-word',
   },
}))

const WrapperNoFollowers = styled('div')(() => ({
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   margin: '30px',
}))

const Followers: React.FC = () => {
   const navigate = useNavigate()
   const params = useParams()
   const { user: myProfile } = useTypedSelector(state => state.auth)
   const [loading, setLoading] = React.useState(true)
   const [value, setValue] = React.useState<IFollow | null>(null)

   React.useEffect(() => {
      const userName = params.userName
      if (userName) {
         setLoading(true)
         fetchFollowers(userName)
      }
   }, [params.userName])

   const fetchFollowers = async (userName: string) => {
      try {
         const res = await FollowApi.fetchFollowers(userName)
         setValue(res.data)
      } catch (error) {
      } finally {
         setLoading(false)
      }
   }

   const handleUnFollow = async (userId?: number) => {
      if (!userId || !value) return
      try {
         const res = await FollowApi.unFollow(userId)
         if (res.data) {
            setValue({
               ...value,
               users: value?.users.map(item =>
                  item.id === userId ? { ...item, isFollowing: false } : item
               ),
            })
         }
      } catch (error) {}
   }

   const handleFollow = async (userId?: number) => {
      if (!userId || !value) return
      try {
         const res = await FollowApi.follow(userId)
         if (res.data) {
            setValue({
               ...value,
               users: value?.users.map(item =>
                  item.id === userId ? { ...item, isFollowing: true } : item
               ),
            })
         }
      } catch (error) {}
   }

   const FollowingItem = (item: IUsersFollow) => (
      <WrapperItem key={item.id}>
         <ContainerItem>
            <WrapperAvatarItem>
               <Link to={'/user/' + item.userName}>
                  <Avatar src={item.avatar} sx={{ width: 48, height: 48 }} />
               </Link>
            </WrapperAvatarItem>
            <BodyItem>
               <BlockNamesAndBtnItem>
                  <BlockNamesItem>
                     <Link to={'/user/' + item.userName}>
                        <Typography
                           color='black'
                           fontSize='15px'
                           fontWeight='700'
                           lineHeight='20px'
                        >
                           {item.fullName}
                        </Typography>
                     </Link>
                     <Box
                        sx={{
                           '& a:hover': { textDecoration: 'none' },
                        }}
                     >
                        <Link to={'/user/' + item.userName}>
                           <Typography variant='caption'>@{item.userName}</Typography>
                        </Link>
                     </Box>
                  </BlockNamesItem>
                  <BlockBtnItem>
                     {item.id !== myProfile?.id && (
                        <>
                           {item.isFollowing ? (
                              <div onClick={() => handleUnFollow(item.id)}>
                                 <ButtonUnFollow />
                              </div>
                           ) : (
                              <div onClick={() => handleFollow(item.id)}>
                                 <ButtonFollow />
                              </div>
                           )}
                        </>
                     )}
                  </BlockBtnItem>
               </BlockNamesAndBtnItem>
               <BlockAboutItem>
                  <span>{item.about}</span>
               </BlockAboutItem>
            </BodyItem>
         </ContainerItem>
      </WrapperItem>
   )

   const NoFollowers = () => (
      <WrapperNoFollowers>
         <div>
            {value?.selectUser.id === myProfile?.id ? (
               <>
                  <Typography variant='h2' fontSize='28px' fontWeight='300' mb='12px'>
                     У вас пока нет читателей
                  </Typography>
                  <Typography variant='caption' fontSize='16px'>
                     У вас пока нет читателей
                  </Typography>
               </>
            ) : (
               <Typography variant='h2' fontSize='28px' fontWeight='300'>
                  У <b>{value?.selectUser.fullName}</b> пока нет читателей
               </Typography>
            )}
         </div>
      </WrapperNoFollowers>
   )

   if (!value?.selectUser && !loading)
      return (
         <Wrapper>
            <Header>
               <GoBackButton margin='0 20px 0 15px' />
               <div>
                  <Typography variant='h6'>
                     <Skeleton width={150} animation={false} />
                  </Typography>
                  <Typography
                     variant='caption'
                     display='block'
                     fontSize='13px'
                     gutterBottom
                  >
                     @{params.userName}
                  </Typography>
               </div>
            </Header>
            <TabsWrapper>
               <Tabs value={'followers'}>
                  <Tab
                     value={'followers'}
                     label='Читатели'
                     sx={{ flex: '1' }}
                     disabled={true}
                  />
                  <Tab
                     value={'following'}
                     label='Читает'
                     sx={{ flex: '1' }}
                     disabled={true}
                  />
               </Tabs>
            </TabsWrapper>
            <Box
               display='flex'
               flexDirection='column'
               alignItems='center'
               justifyContent='center'
               margin='30px'
            >
               <div>
                  <Typography variant='h2' fontSize='30px' fontWeight='400' mb='12px'>
                     Пользователя @{params.userName} не существует
                  </Typography>
                  <Typography variant='caption'>Попробуйте поискать другого.</Typography>
               </div>
            </Box>
         </Wrapper>
      )

   return (
      <Wrapper>
         <Header>
            <GoBackButton margin='0 20px 0 15px' />
            <div>
               <Typography variant='h6'>
                  {value?.selectUser ? (
                     value?.selectUser.fullName
                  ) : (
                     <Skeleton width={150} />
                  )}
               </Typography>
               <Typography variant='caption' display='block' fontSize='13px' gutterBottom>
                  @{params.userName}
               </Typography>
            </div>
         </Header>
         <TabsWrapper>
            <Tabs value={'followers'}>
               <Tab
                  disabled={loading}
                  value={'followers'}
                  label={
                     myProfile?.id === value?.selectUser.id ? 'Мои читатели' : 'Читатели'
                  }
                  onClick={() =>
                     navigate(`/user/${value?.selectUser.userName}/followers`)
                  }
                  sx={{ flex: '1' }}
               />
               <Tab
                  disabled={loading}
                  value={'following'}
                  label={myProfile?.id === value?.selectUser.id ? 'Я читаю' : 'Читатели'}
                  onClick={() =>
                     navigate(`/user/${value?.selectUser.userName}/following`)
                  }
                  sx={{ flex: '1' }}
               />
            </Tabs>
         </TabsWrapper>
         <div>
            {loading ? (
               <Loader size={42} padding={'15px 0'} />
            ) : (
               <>
                  {!value?.users
                     ? NoFollowers()
                     : value?.users.map(item => FollowingItem(item))}
               </>
            )}
         </div>
      </Wrapper>
   )
}

export default Followers

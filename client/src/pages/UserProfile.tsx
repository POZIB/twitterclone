import React from 'react'
import { useNavigate, useParams, Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import {
   Typography,
   styled,
   Avatar,
   Tab,
   Skeleton,
   Button,
   Grid,
   IconButton,
   Box,
} from '@mui/material'
import {
   LocationOnOutlined,
   LinkOutlined,
   CalendarMonthOutlined,
   MarkunreadOutlined,
} from '@mui/icons-material'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'

import FormatDateString from '../utils/FormatDateString'

import { useTypedSelector } from '../hooks/useTypedSelector'
import { newDialog } from '../store/dialogs/actionCreators'
import UserApi from '../api/UserApi'
import TweetsApi from '../api/TweetsApi'
import DialogsApi from '../api/DialogsApi'
import IUserProfile from '../models/IUserProfile'
import ITweet from '../models/ITweet'
import Tweet from '../components/Tweet/index'
import Loader from '../components/Loader'
import GoBackButton from '../components/UI/GoBackButton'
import NoTweets from '../components/UserProfile/NoTweets'
import NoTweetsAndReplies from '../components/UserProfile/NoTweetsAndReplies'
import NoMedia from '../components/UserProfile/NoMedia'
import ModalEditUserProfile from '../components/UserProfile/ModalEditUserProfile'
import ButtonUnFollow from '../components/UI/ButtonUnFollow'
import ButtonFollow from '../components/UI/ButtonFollow'
import FollowApi from '../api/FollowApi'

const Wrapper = styled('div')(({ theme }) => ({}))

const Header = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   position: 'sticky',
   top: 0,
   zIndex: 1,
   backgroundColor: 'rgba(255, 255, 255, 0.85)',
   backdropFilter: 'blur(12px)',
}))

const WrapperAvatarUser = styled('div')(({ theme }) => ({
   width: '9rem',
   height: '9rem',
}))

const AvatarUser = styled(Avatar)(({ theme }) => ({
   width: '100%',
   height: '100%',
   border: '4px solid #fff',
}))

const ContainerUserInfo = styled('div')(({ theme }) => ({
   padding: '0 15px 10px 15px',
}))

const BackgroundAvatar = styled('div')(({ theme }) => ({
   width: '100%',
   height: '200px',
   backgroundColor: '#c4cfd6',
   backgroundRepeat: 'no-repeat',
   backgroundSize: 'cover',
   backgroundPosition: 'center',
}))

const BoxIcon = styled('div')(() => ({
   marginRight: '12px',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   fontSize: '15px',
   lineHeight: '12px',
   color: '#536471',
   '& svg': {
      marginRight: '4px',
   },
}))

const TabsWrapper = styled('div')(() => ({
   '& .MuiTabPanel-root': {
      padding: '0',
   },
}))

const TabContainer = styled('div')(() => ({
   borderBottom: '1px solid rgb(239, 243, 244)',

   '& .MuiTabs-flexContainer': {
      justifyContent: 'center',
   },
}))

const BoxButton = styled('div')(() => ({
   marginLeft: '8px',
   display: 'flex',
   '& .MuiButtonBase-root': {
      border: '1px solid rgb(207, 217, 222)',
      color: 'rgb(15, 20, 25)',
      padding: '5px',
      transition: 'background-color 0.2s',
      '&:hover': {
         backgroundColor: 'rgba(15, 20, 25, 0.1)',
         color: 'rgb(15, 20, 25)',
      },
   },
}))

const CustomLink = styled('a')(({ theme }) => ({
   color: theme.palette.primary.main,
   textDecoration: 'none',
   '&:hover': {
      textDecoration: 'underline',
   },
}))

const ContainerNotAccount = styled('div')(({ theme }) => ({
   maxWidth: '400px',
   margin: '32px auto',
   padding: '40px 20px',
}))

const User: React.FC = () => {
   const dispatch = useDispatch()
   const navigate = useNavigate()
   const params = useParams()
   const { user: myProfile } = useTypedSelector(state => state.auth)
   const [tabs, setTabs] = React.useState('Tweets')
   const [user, setUser] = React.useState<IUserProfile | undefined>(undefined)
   const [tweets, setTweets] = React.useState<ITweet[] | undefined>(undefined)
   const [loadingUser, setLoadingUser] = React.useState(true)
   const [loadingTweets, setLoadingTweets] = React.useState(true)
   const [isVisibleModalEditProfile, setIsVisibleModalEditProfile] = React.useState(false)

   React.useEffect(() => {
      const userName = params.userName || myProfile?.userName
      setLoadingTweets(true)
      setLoadingUser(true)

      if (userName) {
         fetchUser(userName)
      }

      return () => {
         setUser(undefined)
      }
   }, [params.userName, myProfile])

   React.useEffect(() => {
      if (!user) return
      switch (tabs) {
         case 'Tweets': {
            fetchTweets()
            break
         }
         default: {
            setLoadingTweets(false)
         }
      }
   }, [user, tabs])

   const fetchUser = async (userName: string) => {
      try {
         setLoadingUser(true)
         const res = await UserApi.getUserProfile(userName)
         setUser(res.data)
      } catch (error) {
      } finally {
         setLoadingUser(false)
      }
   }

   const fetchTweets = async () => {
      if (user) {
         const res = await TweetsApi.fetchTweets(user.id)
         setTweets(res.data)
         setLoadingTweets(false)
      }
   }

   const handleClickSendMessage = async () => {
      if (user && user.id) {
         const res = await DialogsApi.openDialog(user.id)
         if (res.data) {
            navigate('/messages/' + res.data)
         } else {
            if (user && user.id && myProfile && myProfile.id) {
               const id = dispatch(newDialog(myProfile.id, user)).payload?.id
               navigate('/messages/' + id)
            }
         }
      }
   }

   const handleUnFollow = async () => {
      if (!user || !user.id) return
      try {
         const res = await FollowApi.unFollow(user.id)
         if (res.data) {
            setUser({
               ...user,
               followers: user.followers ? --user.followers : 0,
               isFollowing: false,
            })
         }
      } catch (error) {}
   }

   const handleFollow = async () => {
      if (!user || !user.id) return
      try {
         const res = await FollowApi.follow(user.id)
         if (res.data) {
            setUser({
               ...user,
               followers: user.followers ? ++user.followers : 1,
               isFollowing: true,
            })
         }
      } catch (error) {}
   }

   const handleChangeTab = (event: React.SyntheticEvent, newValue: string) => {
      setLoadingTweets(true)
      setTabs(newValue)
   }

   const handleOpenModalEditProfile = () => {
      setIsVisibleModalEditProfile(true)
   }

   if (!user && !loadingUser)
      return (
         <Wrapper>
            <Header>
               <GoBackButton margin='0 20px 0 15px' />
               <div>
                  <Typography variant='h6'>Профиль</Typography>
               </div>
            </Header>

            <Grid
               container
               direction='column'
               justifyContent='center'
               alignItems='stretch'
            >
               <Grid item>
                  <BackgroundAvatar />
               </Grid>
               <Grid item>
                  <ContainerUserInfo>
                     <Grid
                        container
                        direction='column'
                        justifyContent='center'
                        alignItems='stretch'
                        spacing={1}
                     >
                        <Grid
                           item
                           display='flex'
                           justifyContent='space-between'
                           alignItems='flex-end'
                           mt='-70px'
                        >
                           <WrapperAvatarUser>
                              <AvatarUser />
                           </WrapperAvatarUser>
                        </Grid>
                        <Grid item>
                           <Typography variant='h6' fontWeight={700} lineHeight='24px'>
                              @{params.id}
                           </Typography>
                        </Grid>
                        <ContainerNotAccount>
                           <Typography
                              variant='h2'
                              fontSize='32px'
                              fontWeight='400'
                              lineHeight='36px'
                              color='rgb(15, 20, 25)'
                              mb='8px'
                           >
                              Этот аккаунт не существует
                           </Typography>
                           <Typography
                              variant='h6'
                              fontSize='15px'
                              fontWeight='400'
                              lineHeight='20px'
                              color='rgb(83, 100, 113)'
                           >
                              Попробуйте поискать другой.
                           </Typography>
                        </ContainerNotAccount>

                        <Grid item display='flex'></Grid>
                     </Grid>
                  </ContainerUserInfo>
               </Grid>
            </Grid>
         </Wrapper>
      )

   return (
      <Wrapper>
         <Header>
            <GoBackButton margin='0 20px 0 15px' />
            <div>
               <Typography variant='h6'>
                  {user ? user.fullName : <Skeleton width={150} />}
               </Typography>
               <Typography variant='caption' display='block' fontSize='13px' gutterBottom>
                  {user ? (
                     tabs === 'Tweets' && `${tweets?.length || 'нет'} твитов`
                  ) : (
                     <Skeleton width={150} />
                  )}
               </Typography>
            </div>
         </Header>

         <Grid container direction='column' justifyContent='center' alignItems='stretch'>
            <Grid item>
               <BackgroundAvatar
                  sx={{
                     backgroundImage: user?.avatarBackground
                        ? `url(${user?.avatarBackground})`
                        : '',
                  }}
               />
            </Grid>
            <Grid item>
               <ContainerUserInfo>
                  <Grid
                     container
                     direction='column'
                     justifyContent='center'
                     alignItems='stretch'
                     spacing={1}
                  >
                     <Grid
                        item
                        display='flex'
                        justifyContent='space-between'
                        alignItems='flex-end'
                        mt='-70px'
                     >
                        <WrapperAvatarUser>
                           <AvatarUser src={user?.avatar} />
                        </WrapperAvatarUser>
                        {user ? (
                           user?.id === myProfile?.id ? (
                              <Button
                                 variant='outlined'
                                 color='primary'
                                 onClick={handleOpenModalEditProfile}
                              >
                                 Изменить профиль
                              </Button>
                           ) : (
                              <Box display='flex'>
                                 <BoxButton>
                                    <IconButton
                                       size='small'
                                       onClick={handleClickSendMessage}
                                    >
                                       <MarkunreadOutlined />
                                    </IconButton>
                                 </BoxButton>

                                 <BoxButton>
                                    {user.isFollowing ? (
                                       <ButtonUnFollow
                                          fontSize='16px'
                                          onClick={handleUnFollow}
                                       />
                                    ) : (
                                       <ButtonFollow
                                          fontSize='16px'
                                          onClick={handleFollow}
                                       />
                                    )}
                                 </BoxButton>
                              </Box>
                           )
                        ) : (
                           <Skeleton width={150} height={50} />
                        )}
                     </Grid>
                     <Grid item>
                        <Typography variant='h6' fontWeight={700} lineHeight='24px'>
                           {user ? user.fullName : <Skeleton width={150} />}
                        </Typography>
                        <Typography variant='caption'>
                           {user ? `@${user?.userName}` : <Skeleton width={100} />}
                        </Typography>
                     </Grid>
                     <Grid item whiteSpace='pre-wrap' sx={{ wordBreak: 'break-word' }}>
                        <Typography variant='body1'>{user?.about}</Typography>
                     </Grid>
                     <Grid item display='flex'>
                        {user?.location && (
                           <BoxIcon>
                              <LocationOnOutlined color='inherit' fontSize='medium' />
                              {user?.location}
                           </BoxIcon>
                        )}
                        {user?.website && (
                           <BoxIcon>
                              <LinkOutlined color='inherit' fontSize='medium' />
                              <CustomLink
                                 target='_blank'
                                 rel='noopener noreferrer'
                                 href={user?.website}
                              >
                                 {user?.website}
                              </CustomLink>
                           </BoxIcon>
                        )}
                     </Grid>
                     <Grid item display='flex'>
                        <BoxIcon>
                           <CalendarMonthOutlined color='inherit' fontSize='medium' />
                           Регистрация: {FormatDateString.register(user?.create)}
                        </BoxIcon>
                     </Grid>
                     <Grid item display='flex'>
                        <Link to={`/user/${user?.userName}/following`}>
                           <Box mr='20px'>
                              <Typography
                                 display='inline'
                                 variant='body2'
                                 color='black'
                                 fontSize='14px'
                                 fontWeight='700'
                                 pr='2px'
                              >
                                 {user?.followings}
                              </Typography>
                              <Typography variant='captionLight'>Читает</Typography>
                           </Box>
                        </Link>
                        <Link to={`/user/${user?.userName}/followers`}>
                           <Box>
                              <Typography
                                 display='inline'
                                 variant='body2'
                                 color='black'
                                 fontSize='14px'
                                 fontWeight='700'
                                 pr='2px'
                              >
                                 {user?.followers}
                              </Typography>
                              <Typography variant='captionLight'>Читателей</Typography>
                           </Box>
                        </Link>
                     </Grid>
                  </Grid>
               </ContainerUserInfo>
            </Grid>
         </Grid>
         <TabsWrapper>
            <TabContext value={tabs}>
               <TabContainer>
                  <TabList onChange={handleChangeTab}>
                     <Tab label='Твиты' value='Tweets' />
                     <Tab
                        label={
                           user?.id === myProfile?.id
                              ? 'Твиты и мои ответы'
                              : 'Твиты и ответы'
                        }
                        value='TweetsAndReplies'
                     />
                     <Tab label='Медиа' value='Media' />
                     <Tab label='Нравится' value='Likes' />
                  </TabList>
               </TabContainer>
               {loadingTweets ? (
                  <Loader size={42} padding={'15px 0'} />
               ) : (
                  <>
                     <TabPanel value='Tweets'>
                        {tweets?.length ? (
                           tweets?.map(item => <Tweet key={item.id} item={item} />)
                        ) : (
                           <NoTweets user={user} />
                        )}
                     </TabPanel>
                     <TabPanel value='TweetsAndReplies'>
                        {<NoTweetsAndReplies user={user} />}
                     </TabPanel>
                     <TabPanel value='Media'>{<NoMedia user={user} />}</TabPanel>
                     <TabPanel value='Likes'>{<NoMedia user={user} />}</TabPanel>
                  </>
               )}
            </TabContext>
         </TabsWrapper>

         {myProfile && user && (
            <ModalEditUserProfile
               user={user}
               fullWidth
               maxWidth='sm'
               open={isVisibleModalEditProfile}
               onClose={() => setIsVisibleModalEditProfile(false)}
            />
         )}
      </Wrapper>
   )
}

export default User

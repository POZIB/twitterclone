import React from 'react'
import { useDispatch } from 'react-redux'
import { styled } from '@mui/material'
import socket from '../socket'

import {
   deleteMessage,
   fetchCountUnreadSMS,
   redingMessages,
   sendMessage,
   updateMessage,
} from '../store/messages/actionCreators'
import {
   addDialog,
   upadteDialog,
   setIsRecording,
   setIsTyping,
   newDialog,
   resetNewDialog,
   setSelectDialog,
} from '../store/dialogs/actionCreators'

import Dialogs from '../components/Messages/Dialogs'
import ChatDialog from '../components/Messages/ChatDialog'
import IMessage from '../models/IMessage'
import IDialog from '../models/IDialog'

const Wrapper = styled('div')(({ theme }) => ({
   height: '100%',
   display: 'flex',
   alignItems: 'stretch',
}))

const Messages: React.FC = () => {
   const dispatch = useDispatch()
   let timeoutRecordingId: NodeJS.Timeout
   let timeoutTypingId: NodeJS.Timeout

   React.useEffect(() => {
      socket.on('MESSAGES:SEND', (data: IMessage) => {
         dispatch(sendMessage(data))
      })

      socket.on('MESSAGES:DELETE', (data: IMessage) => {
         dispatch(deleteMessage(data))
      })

      socket.on('MESSAGES:UPDATE', (data: IMessage) => {
         dispatch(updateMessage(data))
      })

      socket.on('MESSAGES:READING', (data: IMessage[]) => {
         dispatch(redingMessages(data))
         dispatch(fetchCountUnreadSMS())
      })

      socket.on('DIALOGS:UPDATE', (data: IDialog) => {
         dispatch(upadteDialog(data))
         dispatch(fetchCountUnreadSMS())
      })

      socket.on('DIALOGS:NEW', (data: IDialog) => {
         dispatch(addDialog(data))
         dispatch(resetNewDialog())
         dispatch(setSelectDialog(data.id))
      })

      socket.on('DIALOGS:RECORDING_VOICE', ({ dialogId, flag }) => {
         dispatch(setIsRecording({ dialogId, flag }))
         clearTimeout(timeoutRecordingId)
         if (flag) {
            timeoutRecordingId = setTimeout(() => {
               dispatch(setIsRecording({ dialogId, flag: false }))
            }, 60000)
         }
      })
      socket.on('DIALOGS:TYPING', ({ dialogId, flag }) => {
         dispatch(setIsTyping({ dialogId, flag }))
         clearTimeout(timeoutTypingId)
         if (flag) {
            timeoutTypingId = setTimeout(() => {
               dispatch(setIsTyping({ dialogId, flag: false }))
            }, 10000)
         }
      })

      return () => {
         socket.removeListener('MESSAGES:DELETE')
         socket.removeListener('MESSAGES:SEND')
         socket.removeListener('MESSAGES:READING')
         socket.removeListener('DIALOGS:NEW')
         socket.removeListener('DIALOGS:UPDATE')
         socket.removeListener('DIALOGS:NEW')
         socket.removeListener('MESSAGES:RECORDING_VOICE')
         socket.removeListener('MESSAGES:TYPING')
      }
   }, [socket])
   return (
      <Wrapper>
         <Dialogs />
         <ChatDialog />
      </Wrapper>
   )
}

export default Messages

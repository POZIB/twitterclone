import React, { useState } from 'react'
import { Box, Button, Grid, styled, Typography } from '@mui/material'
import { Twitter } from '@mui/icons-material'

import { Outlet, useNavigate, useLocation } from 'react-router-dom'
import { useTypedSelector } from '../hooks/useTypedSelector'
import NotificationError from '../components/NotificationError'
import { useSnackbar } from '../hooks/useSnackbar'

const Wrapper = styled('div')(({ theme }) => ({
   overflow: 'hidden',
   display: 'flex',
   height: '100vh',
   '@media (max-width: 1024px)': {
      flexDirection: 'column-reverse',
      height: 'auto',
   },
}))

const LoginSide = styled('section')(({ theme }) => ({
   minWidth: '45vw',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   padding: '16px',
}))

const LoginSideBody = styled('div')(({ theme }) => ({
   padding: '20px',
   maxWidth: '760px',
   '@media (max-width: 1024px)': {
      maxWidth: '620px',
   },
}))

const ImgSide = styled('section')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   backgroundColor: theme.palette.primary.main,
   '@media (max-width: 1024px)': {
      hidth: '45vh',
   },
}))

const WrapperImgBlock = styled('div')(({ theme }) => ({
   height: '100%',
   width: '100%',
   backgroundPosition: 'center center',
   backgroundSize: 'cover',
   backgroundRepeat: 'no-repeat',
   backgroundImage: 'url(https://abs.twimg.com/sticky/illustrations/lohp_1302x955.png)',
}))

const ImgBlock = styled('div')(({ theme }) => ({
   height: '100%',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   '& svg': {
      padding: '32px',
      color: '#fff',
      // maxHeight: '380px',
      height: '100%',
      width: '100%',
   },
}))

const SignIn: React.FC = (): React.ReactElement => {
   const navigate = useNavigate()
   const location = useLocation()
   const { authError } = useTypedSelector(state => state.auth)
   const { isOpen, message, openSnackBar } = useSnackbar()

   React.useEffect(() => {
      openSnackBar(authError)
   }, [authError])

   return (
      <Wrapper>
         <NotificationError open={isOpen} message={message} />
         <ImgSide>
            <WrapperImgBlock>
               <ImgBlock>
                  <Twitter />
               </ImgBlock>
            </WrapperImgBlock>
         </ImgSide>
         <LoginSide>
            <LoginSideBody>
               <Twitter color='primary' sx={{ height: '57px', width: '45px' }} />
               <Box mt={'45px'} mb={'45px'}>
                  <Typography
                     fontSize='64px'
                     lineHeight='84px'
                     fontWeight={700}
                     variant='h1'
                     textOverflow='break-word'
                  >
                     В курсе происходящего
                  </Typography>
               </Box>
               <Box mb={'32px'}>
                  <Typography
                     fontSize='31px'
                     lineHeight='36px'
                     fontWeight={700}
                     textOverflow='break-word'
                  >
                     Присоединяйтесь к Твиттеру прямо сейчас!
                  </Typography>
               </Box>
               <Box width='300px' maxWidth='380px'>
                  <Button
                     variant='contained'
                     color='primary'
                     fullWidth
                     onClick={() => navigate('/i/flow/sign_up', {state: location.state})}
                  >
                     Зарегистрироваться
                  </Button>
                  <Typography
                     fontSize='17px'
                     fontWeight={700}
                     lineHeight='20px'
                     mt='30px'
                     mb='20px'
                  >
                     Уже зарегистрированы?
                  </Typography>
                  <Button
                     variant='outlined'
                     color='primary'
                     fullWidth
                     onClick={() => navigate('/i/flow/sign_in', {state: location.state})}
                  >
                     Войти
                  </Button>
               </Box>
            </LoginSideBody>
         </LoginSide>

         <Outlet />
      </Wrapper>
   )
}

export default SignIn

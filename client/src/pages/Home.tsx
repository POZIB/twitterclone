import React from 'react'
import { useDispatch } from 'react-redux'
import { Route, Routes } from 'react-router-dom'

import { styled, Typography, Box } from '@mui/material'

import { useTypedSelector } from '../hooks/useTypedSelector'
import { fetchTweets } from '../store/tweets/actionCreators'

import ITweet from '../models/ITweet'
import GoBackButton from '../components/UI/GoBackButton'
import Tweet from '../components/Tweet/index'
import FormAddTweet from '../components/Tweet/FormAddTweet'
import SelectedTweet from '../components/Tweet/SelectedTweet'
import SkeletonTweet from '../components/Tweet/SkeletonTweet'

const Wrapper = styled('section')(({ theme }) => ({
   flex: 1,
}))

const HeaderTweets = styled('div')(() => ({
   height: '53px',
   display: 'flex',
   alignItems: 'center',
   padding: '5px 10px',
   position: 'sticky',
   top: 0,
   zIndex: 1,
   backgroundColor: 'rgba(255, 255, 255, 0.85)',
   backdropFilter: 'blur(12px)',
}))

const WrapperFormAddTweet = styled('div')(() => ({
   userSelect: 'none',
   borderBottom: '1px solid #e4e4e4',
}))

const ContainerTweets = styled('div')(() => ({
   listStyle: 'none',
   padding: 0,
}))

const Home: React.FC = (): React.ReactElement => {
   const dispatch = useDispatch()
   const { tweets } = useTypedSelector(state => state.tweets)
   const [selectedTweet, setSelectedTweet] = React.useState<ITweet | null>(null)

   React.useEffect(() => {
      dispatch(fetchTweets())
   }, [dispatch])

   return (
      <Box sx={{ display: 'flex' }}>
         <Wrapper>
            <HeaderTweets>
               <Routes>
                  <Route path='/tweet/*' element={<GoBackButton margin='0 15px 0 0' />} />
               </Routes>

               <Routes>
                  <Route
                     path='/tweet/:id'
                     element={
                        <Typography variant='h6' fontWeight={700} lineHeight='24px'>
                           Ветка
                        </Typography>
                     }
                  />
                  <Route
                     path='/'
                     element={
                        <Typography variant='h6' fontWeight={800}>
                           Главная
                        </Typography>
                     }
                  ></Route>
               </Routes>
            </HeaderTweets>
            <Routes>
               <Route
                  path='/'
                  element={
                     <>
                        <WrapperFormAddTweet>
                           <FormAddTweet />
                        </WrapperFormAddTweet>
                        <ContainerTweets>
                           {tweets.length
                              ? tweets.map(item => <Tweet key={item.id} item={item} />)
                              : [...Array(4)].map((item, index) => (
                                   <SkeletonTweet key={index} />
                                ))}
                        </ContainerTweets>
                     </>
                  }
               />
               <Route
                  path='/tweet/:id'
                  element={<SelectedTweet id={selectedTweet?.id} />}
               />
            </Routes>
         </Wrapper>
      </Box>
   )
}

export default Home

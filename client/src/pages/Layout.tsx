import React from 'react'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom'

import { Grid, styled } from '@mui/material'
import { Container } from '@mui/system'

import { fetchTopicals } from '../store/topical/actionCreators'

import SideRight from '../components/Layout/SideRight/index'
import SideMenu from '../components/Layout/SideMenu/index'

import Notifications from '../components/Notifications'
import { useTypedSelector } from '../hooks/useTypedSelector'
import NotificationError from '../components/NotificationError'
import { useSnackbar } from '../hooks/useSnackbar'

const Wrapper = styled('main')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   borderLeft: '1px solid #e4e4e4',
   borderRight: '1px solid #e4e4e4',
}))

const ContainerChildren = styled('main')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   borderLeft: '1px solid #e4e4e4',
   borderRight: '1px solid #e4e4e4',
}))

interface IProps {
   children: React.ReactNode
}

const Layout: React.FC<IProps> = ({ children }): React.ReactElement => {
   const dispatch = useDispatch()
   const location = useLocation()
   const { errorTweets } = useTypedSelector(state => state.tweets)
   const { isOpen, message, openSnackBar } = useSnackbar()

   React.useEffect(() => {
      dispatch(fetchTopicals())
   }, [dispatch])

   React.useEffect(() => {
      openSnackBar(errorTweets)
   }, [errorTweets])

   return (
      <Wrapper>
         <NotificationError open={isOpen} message={message} />
         <Container maxWidth='xl' sx={{ display: 'flex', flex: '1' }}>
            <Grid container spacing={3} display='flex' flex='1'>
               <Grid item xs={1.5} md={1} xl={2.5}>
                  <SideMenu />
               </Grid>
               <Grid
                  item
                  xs={10.5}
                  md={location.pathname.includes('messages') ? 11 : 7.5}
                  xl={location.pathname.includes('messages') ? 9.5 : 6}
                  display='flex'
                  flexDirection='column'
                  flexGrow='1'
               >
                  <ContainerChildren>{children}</ContainerChildren>
               </Grid>
               {location.pathname.includes('messages') ? (
                  <></>
               ) : (
                  <Grid item xs={0} md={3.5} xl={3.5}>
                     <SideRight />
                  </Grid>
               )}
            </Grid>
         </Container>
         <Notifications />
      </Wrapper>
   )
}

export default Layout

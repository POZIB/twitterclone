import { createTheme } from '@mui/material'

declare module '@mui/material/styles' {
   interface TypographyVariants {
      menu: React.CSSProperties
      captionLight: React.CSSProperties
   }
   interface Palette {
      black: string
   }

   interface PaletteOptions {
      black: string
   }
   // allow configuration using `createTheme`
   interface TypographyVariantsOptions {
      menu?: React.CSSProperties
      captionLight?: React.CSSProperties
   }
}

// Update the Typography's variant prop options
declare module '@mui/material/Typography' {
   interface TypographyPropsVariantOverrides {
      menu: true
      captionLight: true
   }
}

declare module '@mui/material/Link' {
   interface LinkPropsColorOverrides {
      black: true
   }
}

export const theme = createTheme({
   breakpoints: {
      values: {
         xs: 0,
         sm: 600,
         md: 900,
         lg: 1070,
         xl: 1320,
      },
   },
   palette: {
      black: 'rgb(15, 20, 25)',

      primary: {
         main: 'rgb(29, 155, 240)',
         light: 'rgba(77, 182, 255, 0.9)',
         dark: 'rgb(26, 140, 216)',
      },
      background: {
         default: '#fff',
      },
   },
   typography: {
      menu: {
         fontWeight: 400,
         lineHeight: '24px',
         fontSize: '20px',
         color: '#0f1419',
      },
      caption: {
         color: '#536471',
         fontSize: '15px',
         lineHeight: '20px',
         fontWeight: '400',
      },
      captionLight: {
         color: '#536471',
         fontSize: '14px',
         lineHeight: '16px',
         fontWeight: '400',
      },
      body1: {
         color: '#0f1419',
         fontSize: '15px',
         lineHeight: '20px',
         fontWeight: '400',
      },
      h6: {
         fontWeight: 700,
      },
      fontFamily: [
         'TwitterChirp',
         'system-ui',
         '-apple-system',
         'BlinkMacSystemFont',
         'Segoe UI',
         'Roboto',
         'Ubuntu',
         'Halverica Heue',
         'sans-serif',
      ].join(','),
   },

   components: {
      MuiSnackbarContent: {
         styleOverrides: {
            root: {
               backgroundColor: 'rgb(0 126 210 / 65%)',
               backdropFilter: 'blur(16px)',
            },
            message: {
               padding: '4px 0',
            },
         },
      },
      MuiMenu: {
         styleOverrides: {
            root: {
               '& .MuiPaper-root': {
                  boxShadow:
                     'rgb(101 119 134 / 20%) 0px 0px 15px, rgb(101 119 134 / 15%) 0px 0px 3px 1px',
               },
            },
         },
      },

      MuiIconButton: {
         styleOverrides: {
            colorInherit: {
               ':hover': {
                  backgroundColor: 'rgb(15 20 25 / 10%)',
                  color: 'inherit',
               },
            },
            root: {
               '&: hover': {
                  color: 'rgb(29, 155, 240)',
                  backgroundColor: 'rgba(29, 155, 240, 0.1)',
               },
            },
         },
      },
      MuiAvatar: {
         styleOverrides: {
            root: {
               '&: hover': {
                  textDecoration: 'underline',
               },
            },
         },
      },

      MuiLink: {
         styleOverrides: {
            root: {
               textDecoration: 'none',
               '&: hover': {
                  textDecoration: 'underline',
               },
            },
         },
      },
      MuiTab: {
         styleOverrides: {
            root: {
               fontWeight: 500,
               color: '#536471',
               lineHeight: '20px',
               size: '15px',
               padding: '12px 25px',
               '&: hover': {
                  backgroundColor: 'rgba(15, 20, 25, 0.1)',
               },
            },
         },
      },
      MuiDialogTitle: {
         styleOverrides: {
            root: {
               marginBottom: '0',
               padding: '5px',
               minHeight: '20px',
               // borderBottom: '1px solid #e4e4e4',
            },
         },
      },
      MuiDialogContent: {
         styleOverrides: {
            root: {
               padding: '5px 15px',
               paddingTop: '10px',
            },
         },
      },
      MuiDialog: {
         styleOverrides: {
            paper: {
               borderRadius: '16px',
            },
         },
      },
      MuiButton: {
         styleOverrides: {
            root: {
               borderRadius: '30px',
            },
            contained: {
               color: '#fff',
               fontWeight: 700,
               boxShadow: 'none',
               ':hover': {
                  backgroundColor: 'rgb(26, 140, 216)',
                  boxShadow: 'none',
               },
            },
            outlined: {
               color: 'rgba(29, 155, 240, 0.8)',
               fontWeight: 600,
               borderColor: 'rgba(29, 155, 240, 0.8)',
            },
         },
      },

      MuiTypography: {
         defaultProps: {
            variantMapping: {
               h1: 'h2',
               h2: 'h2',
               h3: 'h2',
               h4: 'h2',
               h5: 'h2',
               h6: 'h2',
               subtitle1: 'h2',
               subtitle2: 'h2',
               body1: 'span',
               body2: 'span',
            },
         },
      },
   },
})

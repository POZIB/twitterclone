import { IUsersFollow } from './../models/IFollow'
import { AxiosResponse } from 'axios'
import $api from '../http'

export default class WhomToReadApi {
   static async fetchWhomToRead(): Promise<AxiosResponse<IUsersFollow[]>> {
      return await $api.get<IUsersFollow[]>('/whoomtoread')
   }
}

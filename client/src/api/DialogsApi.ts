import { AxiosResponse } from 'axios'
import $api from '../http'

import IDialog from '../models/IDialog'

export interface IDialogResponse {}

export default class DialogsApi {
   static async fetchDialogs(): Promise<AxiosResponse<IDialog>> {
      return await $api.get<IDialog>('/dialogs')
   }

   static async openDialog(companionId: number): Promise<AxiosResponse<number>> {
      return await $api.post<number>('/dialogs', { companionId })
   }

   static async deleteDialog(id: number): Promise<AxiosResponse<IDialog>> {
      return await $api.delete<IDialog>('/dialogs', { params: { id } })
   }
}

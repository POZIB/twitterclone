import { AxiosResponse } from 'axios'
import $api from '../http'

import IMessage from '../models/IMessage'
import { ISendMessageMessageData } from '../store/messages/types'

export interface IMessageResponse {}

export default class MessagesApi {
   static async fetchMessages(dialogId: number): Promise<AxiosResponse<IMessage[]>> {
      return await $api.get<IMessage[]>('/messages', { params: { dialogId } })
   }

   static async sendMessage(
      payload: ISendMessageMessageData
   ): Promise<AxiosResponse<IMessage>> {
      return await $api.post<IMessage>('/messages', payload)
   }

   static async findByText(value: string): Promise<AxiosResponse<IMessage[]>> {
      return await $api.get<IMessage[]>('/messages/find', { params: value })
   }

   static async findWithDialog(value: string): Promise<AxiosResponse<IMessage[]>> {
      return await $api.get<IMessage[]>('/messages/with_dialog', {
         params: { value: value },
      })
   }

   static async fetchCountUnread(): Promise<AxiosResponse<number>> {
      return await $api.get<number>('/messages/count_unread')
   }
}

import { AxiosResponse } from 'axios'
import $api from '../http'
import ITopical from '../models/ITopical'

export default class TopicalsApi {
   static async fetchTopicals(): Promise<AxiosResponse<ITopical>> {
      return await $api.get('/topicals')
   }
}

import { AxiosResponse } from 'axios'
import $api from '../http'

interface responseUpload {
   url: string
   fileName: string
}

export default class UploadFileApi {
   static async upload(file: File): Promise<AxiosResponse<responseUpload>> {
      const formData = new FormData()
      formData.append('file', file)
      return await $api.post<responseUpload>('/upload', formData, {
         headers: {
            'Content-Type': 'multipart/form-data',
         },
      })
   }
}

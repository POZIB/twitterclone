import { AxiosResponse } from 'axios'
import $api from '../http'
import IFollow from '../models/IFollow'

export default class FollowApi {
   static async fetchFollowers(userName: string): Promise<AxiosResponse<IFollow>> {
      return await $api.get<IFollow>('/follow/followers', { params: { userName } })
   }

   static async fetchFollowings(userName: string): Promise<AxiosResponse<IFollow>> {
      return await $api.get<IFollow>('/follow/followings', { params: { userName } })
   }

   static async follow(userId: number): Promise<AxiosResponse<any>> {
      return await $api.post<any>('/follow', { userId })
   }

   static async unFollow(userId: number): Promise<AxiosResponse<any>> {
      return await $api.put<any>('/follow', { userId })
   }
}

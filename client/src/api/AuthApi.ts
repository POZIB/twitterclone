import { AxiosResponse } from 'axios'
import $api from '../http'
import IUser from '../models/IUser'
import { ISignIn, ISignUp } from '../store/auth/types'

export interface IAuthResponse {
   user: IUser
   token: string
}

export default class AuthApi {
   static async checkAuth(): Promise<AxiosResponse<IAuthResponse>> {
      return await $api.get<IAuthResponse>('/user/check_auth')
   }

   static async signIn(data: ISignIn): Promise<AxiosResponse<IAuthResponse>> {
      return await $api.post<IAuthResponse>('/user/signIn', data)
   }

   static async signUp(data: ISignUp): Promise<AxiosResponse<IAuthResponse>> {
      return await $api.post<IAuthResponse>('/user/signUp', data)
   }
}

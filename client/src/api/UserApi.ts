import { AxiosResponse } from 'axios'
import $api from '../http'
import IUser from '../models/IUser'
import IUserProfile from '../models/IUserProfile'

export default class UserApi {
   static async getUserProfile(userName: string): Promise<AxiosResponse<IUserProfile>> {
      return await $api.get<IUserProfile>('/user', { params: { userName } })
   }

   static async findUsers(value: string): Promise<AxiosResponse<IUser[]>> {
      return await $api.get<IUser[]>('/user/users', { params: { value } })
   }

   static async findUserByEmail(email: string): Promise<AxiosResponse<IUser>> {
      return await $api.get<IUser>('/user/find', { params: { email } })
   }

   static async findUsersWithDialog(value: string): Promise<AxiosResponse<IUser[]>> {
      return await $api.get<IUser[]>('/user/with_dialog', { params: { value } })
   }

   static async updateProfile(user: IUser): Promise<AxiosResponse<IUser>> {
      return await $api.put<IUser>('/user', { updateUser: user })
   }

   static async sendLinkResetPassword(email: string): Promise<AxiosResponse<boolean>> {
      return await $api.post<boolean>('/user/password_reset', { email })
   }

   static async resetPassword(
      hash: string,
      password: string,
      password2: string
   ): Promise<AxiosResponse<boolean>> {
      return await $api.put<boolean>('/user/password_reset', {
         hash,
         password,
         password2,
      })
   }
}

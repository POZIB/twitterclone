import { IPayloadAddTweet } from './../store/tweets/types'
import { AxiosResponse } from 'axios'
import $api from '../http'
import ITweet from '../models/ITweet'

export default class TweetsApi {
   static async fetchOneTweet(id: number): Promise<AxiosResponse<ITweet>> {
      return await $api.get<ITweet>('/tweets/' + id)
   }

   static async fetchTweets(userId?: number): Promise<AxiosResponse<ITweet[]>> {
      return await $api.get<ITweet[]>('/tweets', { params: { userId } })
   }

   static async addTweet(payload: IPayloadAddTweet): Promise<AxiosResponse<ITweet>> {
      return await $api.post<ITweet>('/tweets', payload)
   }

   static async deleteTweet(id: number): Promise<AxiosResponse<ITweet>> {
      return await $api.delete<ITweet>('/tweets', { params: { id } })
   }
}

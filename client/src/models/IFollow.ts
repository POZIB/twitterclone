import IUser from './IUser'

export interface IUsersFollow extends IUser {
   followId?: number
   isFollowing?: boolean
}

export default interface IFollow {
   selectUser: IUser
   users: IUsersFollow[]
}

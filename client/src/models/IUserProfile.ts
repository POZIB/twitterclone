import IUser from './IUser'

export default interface IUserProfile extends IUser {
   followers?: number
   followings?: number
   isFollowing?: boolean
}

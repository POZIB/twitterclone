import IUser from './IUser'

interface ILastMessage {
   id: number
   text: string
   createdAt: string
   authorId: number
   dialogId: number
   isRead: boolean
}

export default interface IDialog {
   id: number
   ownerId: number
   companion: IUser
   dateCreate: string
   countUnreadMessages: number
   lastMessage: ILastMessage | null
   isRecording?: boolean
   isTyping?: boolean
}

export default interface IUser {
   id?: number
   avatar: string
   avatarBackground?: string
   create: string
   email: string
   fullName: string
   userName: string
   confirmed: boolean
   location?: string
   about?: string
   website?: string
   lastSeen?: string
}

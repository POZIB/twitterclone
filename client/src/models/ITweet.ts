import IUser from './IUser'

export default interface ITweet {
   id?: number
   date: string
   text: string
   likes?: number
   replies?: number
   retweets?: number
   images?: string[]
   userId: number
   user?: IUser
}

import IUser from './IUser'

export default interface IMessage {
   id: number
   text: string
   isRead: boolean
   images?: string[]
   voice?: string
   isChanged: boolean
   createdAt: string
   updatedAt: string
   dialogId: number
   authorId: number
   author?: IUser
}

export default interface ITopical {
   id: string
   headline: string
   count: number
}

export default interface IFile {
   file?: File
   url: string
   status?: 'LOADING' | 'LOADED' | 'ERROR'
}

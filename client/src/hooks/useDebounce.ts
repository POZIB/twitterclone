import React from 'react'

export default function useDebounce<T>(callback: T, delay: number = 350) {
   const [debouncedCallback, setDebouncedCallback] = React.useState<T>(callback)

   React.useEffect(() => {
      const timer = setTimeout(() => setDebouncedCallback(callback), delay)

      return () => {
         clearTimeout(timer)
      }
   }, [callback, delay])

   return debouncedCallback
}

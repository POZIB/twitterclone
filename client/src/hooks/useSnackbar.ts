import React from 'react'

export function useSnackbar() {
   const [isOpen, setIsOpen] = React.useState(false)
   const [message, setMessage] = React.useState<string>('')

   React.useEffect(() => {
      if (isOpen) {
         setTimeout(() => {
            setIsOpen(false)
         }, 6000)
      }
   }, [isOpen])

   const openSnackBar = (msg?: string) => {
      if (!msg) return
      setMessage(msg)
      setIsOpen(true)
   }

   return { isOpen, message, openSnackBar }
}

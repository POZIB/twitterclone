import React from 'react'

export default function useDelaySend<T>(callback: T, delay: number = 350) {
   const [debouncedCallback, setDebouncedCallback] = React.useState<T>(callback)
   const [nextDelay, setNextDelay] = React.useState(0)

   React.useEffect(() => {
      if (!callback) {
         setNextDelay(0)
         return
      }

      const timer = setTimeout(() => {
         setDebouncedCallback(callback)
      }, nextDelay)

      const timerClear = setTimeout(() => {
         setNextDelay(0)
      }, 3500)

      setNextDelay(delay)

      return () => {
         clearTimeout(timer)
         clearTimeout(timerClear)
         setNextDelay(0)
      }
   }, [callback, delay])

   return debouncedCallback
}

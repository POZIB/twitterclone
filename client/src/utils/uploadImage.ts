import $api from '../http'

export const uploadImage = async (image: File) => {
   const formData = new FormData()
   formData.append('image', image)

   const response = await $api.post('/upload', formData, {
      headers: {
         'Content-Type': 'multipart/form-data',
      },
   })

   return response.data
}

import differenceInMinutes from 'date-fns/differenceInMinutes'
import differenceInSeconds from 'date-fns/differenceInSeconds'

import format from 'date-fns/format'
import formatDistance from 'date-fns/formatDistance'
import formatDistanceStrict from 'date-fns/formatDistanceStrict'
import isToday from 'date-fns/isToday'

import ruLung from 'date-fns/locale/ru'

export default class FormatDateString {
   static dateDistance(date?: string): string {
      return date
         ? formatDistance(new Date(date), new Date(), {
              locale: ruLung,
           })
         : ''
   }

   static time(time?: string): string {
      return time ? format(new Date(String(time)), 'H:mm', { locale: ruLung }) : ''
   }

   static date(date?: string): string {
      return date ? format(new Date(date), 'dd MMM yyyy г.', { locale: ruLung }) : ''
   }

   static register(date?: string): string {
      return date ? format(new Date(date), 'MMMM yyyy г.', { locale: ruLung }) : ''
   }

   static dateReceivedSMS(date?: string): string {
      if (date) {
         const _date = new Date(date)
         if (isToday(_date)) {
            const dist = formatDistanceStrict(_date, new Date())
            if (dist.includes('seconds') || dist.includes('second')) {
               return differenceInSeconds(new Date(), _date).toString() + 'c'
            }
            if (dist.includes('minutes') || dist.includes('minute')) {
               return differenceInMinutes(new Date(), _date).toString() + 'м'
            }

            return format(_date, 'HH:mm', { locale: ruLung })
         } else {
            return format(_date, 'MMM dd, HH:mm', { locale: ruLung })
         }
      }
      return ''
   }
}

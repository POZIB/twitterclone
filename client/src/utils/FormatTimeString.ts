import format from 'date-fns/format'
import formatDuration from 'date-fns/formatDuration'

class FormatTimeString {
   min(time?: string | number): string {
      return time ? format(new Date(+time * 1000), 'm:ss') : '0:00'
   }
}

export default new FormatTimeString()

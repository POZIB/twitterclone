import io from 'socket.io-client'

export const API_URL =
   process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:5000'

const socket = io(API_URL, {
   withCredentials: true,
   auth: {
      token: `Bearer ${localStorage.getItem('token')}`,
   },
})

export default socket

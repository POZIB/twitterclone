import Home from '../pages/Home'
import Auth from '../pages/Auth'
import User from '../pages/UserProfile'
import Messages from '../pages/Messages'
import NoPage from '../pages/NoPage'
import Followers from '../pages/Followers'
import Following from '../pages/Following'

export interface IRoute {
   path: string
   element: React.ComponentType
}

export enum RouteNames {
   REDIRECT = '*',
   START = '/',
   HOME = '/home/*',
   USER = '/user/:userName',
   FOLLOWERS = '/user/:userName/followers',
   FOLLOWING = '/user/:userName/following',
   ALL_MESSAGES = '/messages',
   CUR_MESSAGES = '/messages/:id',
   AUTH = '/i/flow',
}

export const publicRoutes: IRoute[] = [
   { path: RouteNames.REDIRECT, element: NoPage },
   { path: RouteNames.START, element: Home },
   { path: RouteNames.HOME, element: Home },
   { path: RouteNames.USER, element: User },
   { path: RouteNames.FOLLOWERS, element: Followers },
   { path: RouteNames.FOLLOWING, element: Following },
   { path: RouteNames.ALL_MESSAGES, element: Messages },
   { path: RouteNames.CUR_MESSAGES, element: Messages },
]

export const privateRoutes: IRoute[] = [{ path: RouteNames.AUTH, element: Auth }]

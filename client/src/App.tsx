import React from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { checkAuth } from './store/auth/actionCreators'
import { useTypedSelector } from './hooks/useTypedSelector'

import { styled } from '@mui/material'
import './App.css'

import LogoLoading from './components/LogoLoading'

import { fetchCountUnreadSMS } from './store/messages/actionCreators'
import AppRouter from './components/AppRouter'

const Main = styled('main')(({ theme }) => ({}))

function App(): React.ReactElement {
   const navigate = useNavigate()
   const location = useLocation()
   const dispatch = useDispatch()
   const { user, authLoadingStatus } = useTypedSelector(state => state.auth)
   const isReady = authLoadingStatus !== 'NEVER' && authLoadingStatus !== 'LOADING'

   React.useEffect(() => {
      dispatch(checkAuth())
   }, [])

   React.useEffect(() => {
      if (user) {
         dispatch(fetchCountUnreadSMS())
      }
   }, [user])

   React.useEffect(() => {
      if (!user) {
         if (!location.pathname.includes('/i/flow')) {
            location.state = {from: location.pathname}
         }

         if (location.pathname.includes('/i/flow/password_reset')) {
            if (location.pathname.includes('/i/flow/password_reset/confirmed')) {
               navigate(location.pathname + location.search)
            } else {
               navigate(location.pathname)
            }
         }
      } else if (location.pathname.includes('/i/flow')) {
         const state = location.state as null | {from : string }
         state ? navigate(state.from) : navigate('/home')
      }
   }, [user, location.pathname])

   const render = (): React.ReactNode => {
      if (user && location.pathname.includes('/i/flow')) {
         return <LogoLoading />
      }
      if (!isReady && !user) {
         return <LogoLoading />
      }

      return <AppRouter />
   }

   return <div className='App'>{render()}</div>
}

export default App

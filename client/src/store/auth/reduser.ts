import produce, { Draft } from 'immer'
import { LoadingStatusType } from '../types/LoadingStatusType'

import { IAuthState, AuthAction, AuthActionsType } from './types'

const initialState: IAuthState = {
   user: undefined,
   authLoadingStatus: LoadingStatusType.NEVER,
   authError: '',
   signInLoadingStatus: LoadingStatusType.NEVER,
   signUpLoadingStatus: LoadingStatusType.NEVER,
}

export const authReducer = produce((draft: Draft<IAuthState>, action: AuthAction) => {
   switch (action.type) {
      case AuthActionsType.FETCH_CHECK_AUTH: {
         draft.authLoadingStatus = LoadingStatusType.LOADING
         break
      }
      case AuthActionsType.FETCH_SIGN_IN: {
         draft.signInLoadingStatus = LoadingStatusType.LOADING
         draft.authError = ''
         draft.user = undefined
         break
      }
      case AuthActionsType.FETCH_SIGN_UP: {
         draft.signUpLoadingStatus = LoadingStatusType.LOADING
         draft.authError = ''
         draft.user = undefined
         break
      }
      case AuthActionsType.SET_USER: {
         draft.user = action.payload
         draft.authLoadingStatus = LoadingStatusType.LOADED
         break
      }
      case AuthActionsType.SET_AUTH_LOADING: {
         draft.authLoadingStatus = action.payload
         break
      }
      case AuthActionsType.SET_SIGN_IN_LOADING_STATUS: {
         draft.signInLoadingStatus = action.payload
         break
      }
      case AuthActionsType.SET_SIGN_UP_LOADING_STATUS: {
         draft.signUpLoadingStatus = action.payload
         break
      }
      case AuthActionsType.SIGN_OUT: {
         draft.user = undefined
         draft.authLoadingStatus = LoadingStatusType.LOADED
         break
      }
      case AuthActionsType.SET_AUTH_ERROR: {
         draft.authError = action.payload
         break
      }

      default:
         return draft
   }
}, initialState)

import { Action } from 'redux'
import IUser from '../../models/IUser'
import { LoadingStatusType } from '../types/LoadingStatusType'

export interface IAuthState {
   user: IUser | undefined
   authLoadingStatus: LoadingStatusType
   signInLoadingStatus: LoadingStatusType
   signUpLoadingStatus: LoadingStatusType
   authError: string
}

export interface ISignIn {
   email: string
   password: string
}

export interface ISignUp {
   email: string
   fullName: string
   userName: string
   password: string
}

export enum AuthActionsType {
   FETCH_CHECK_AUTH = 'auth/FETCH_CHECK_AUTH',
   FETCH_SIGN_IN = 'auth/FETCH_SIGN_IN',
   FETCH_SIGN_UP = 'auth/FETCH_SIGN_UP',
   SET_USER = 'auth/SET_USER',
   SET_AUTH_LOADING = 'auth/SET_AUTH_LOADING',
   SIGN_OUT = 'auth/SIGN_OUT',
   SET_AUTH_ERROR = 'auth/SET_AUTH_ERROR',
   SET_SIGN_IN_LOADING_STATUS = 'auth/SET_SIGN_IN_LOADING_STATUS',
   SET_SIGN_UP_LOADING_STATUS = 'auth/SET_SIGN_UP_LOADING_STATUS',
}

export interface IFetchCheckAuthAction extends Action<AuthActionsType> {
   type: AuthActionsType.FETCH_CHECK_AUTH
}

export interface IFetchSignInAction extends Action<AuthActionsType> {
   type: AuthActionsType.FETCH_SIGN_IN
   payload: ISignIn
}

export interface IFetchSignUpAction extends Action<AuthActionsType> {
   type: AuthActionsType.FETCH_SIGN_UP
   payload: ISignUp
}

export interface ISetUserAction extends Action<AuthActionsType> {
   type: AuthActionsType.SET_USER
   payload: IUser | undefined
}

export interface ISetAuthLoadingAction extends Action<AuthActionsType> {
   type: AuthActionsType.SET_AUTH_LOADING
   payload: LoadingStatusType
}

export interface ISetSignInLoadingStatusAction extends Action<AuthActionsType> {
   type: AuthActionsType.SET_SIGN_IN_LOADING_STATUS
   payload: LoadingStatusType
}

export interface ISetSignUpLoadinStatusgAction extends Action<AuthActionsType> {
   type: AuthActionsType.SET_SIGN_UP_LOADING_STATUS
   payload: LoadingStatusType
}

export interface ISignOutAction extends Action<AuthActionsType> {
   type: AuthActionsType.SIGN_OUT
}

export interface ISetAuthErrorAction extends Action<AuthActionsType> {
   type: AuthActionsType.SET_AUTH_ERROR
   payload: string
}

export type AuthAction =
   | IFetchCheckAuthAction
   | IFetchSignInAction
   | IFetchSignUpAction
   | ISetUserAction
   | ISetAuthLoadingAction
   | ISetSignInLoadingStatusAction
   | ISetSignUpLoadinStatusgAction
   | ISignOutAction
   | ISetAuthErrorAction

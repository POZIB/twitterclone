import { call, put, takeLatest } from 'redux-saga/effects'
import { AxiosError, AxiosResponse } from 'axios'

import AuthApi, { IAuthResponse } from '../../api/AuthApi'
import {
   setAuthError,
   setAuthLoadingStatus,
   setSignInLoadingStatus,
   setSignUpLoadingStatus,
   setUser,
} from './actionCreators'
import { AuthActionsType, IFetchSignInAction, IFetchSignUpAction } from './types'
import { LoadingStatusType } from '../types/LoadingStatusType'

export function* fetchChechAuthRequest() {
   try {
      if (window.localStorage.getItem('token')) {
         const { data }: AxiosResponse<IAuthResponse> = yield call(AuthApi.checkAuth)
         window.localStorage.setItem('token', data.token)
         yield put(setUser(data.user))
      } else {
         yield put(setAuthLoadingStatus(LoadingStatusType.LOADED))
      }
   } catch (error) {
      const err = error as AxiosError<{ message: string }>
      if (err.response) {
         yield put(setAuthError(err.response?.data.message))
      }
      window.localStorage.removeItem('token')
      yield put(setUser(undefined))
      yield put(setAuthLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export function* fetchSignInRequest({ payload }: IFetchSignInAction) {
   try {
      const { data }: AxiosResponse<IAuthResponse> = yield call(AuthApi.signIn, payload)
      window.localStorage.setItem('token', data.token)
      yield put(setSignInLoadingStatus(LoadingStatusType.LOADED))
      yield put(setUser(data.user))
   } catch (error) {
      const err = error as AxiosError<{ message: string }>
      if (err.response) {
         yield put(setAuthError(err.response?.data.message))
      }
      yield put(setSignInLoadingStatus(LoadingStatusType.ERROR))
      yield put(setAuthLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export function* fetchSingUpRequest({ payload }: IFetchSignUpAction) {
   try {
      const { data }: AxiosResponse<IAuthResponse> = yield call(AuthApi.signUp, payload)
      window.localStorage.setItem('token', data.token)
      yield put(setSignUpLoadingStatus(LoadingStatusType.LOADED))
      yield put(setUser(data.user))
   } catch (error) {
      const err = error as AxiosError<{ message: string }>
      if (err.response) {
         yield put(setAuthError(err.response?.data.message))
      }
      yield put(setSignUpLoadingStatus(LoadingStatusType.ERROR))
      yield put(setAuthLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export function* logout() {
   try {
      window.localStorage.removeItem('token')
   } catch (error) {
      yield put(setAuthLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export default function* authSaga() {
   yield takeLatest(AuthActionsType.FETCH_CHECK_AUTH, fetchChechAuthRequest)
   yield takeLatest(AuthActionsType.FETCH_SIGN_IN, fetchSignInRequest)
   yield takeLatest(AuthActionsType.FETCH_SIGN_UP, fetchSingUpRequest)
   yield takeLatest(AuthActionsType.SIGN_OUT, logout)
}

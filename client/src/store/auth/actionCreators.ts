import IUser from '../../models/IUser'
import { LoadingStatusType } from '../types/LoadingStatusType'

import {
   IFetchSignInAction,
   IFetchSignUpAction,
   ISetAuthLoadingAction,
   ISetSignInLoadingStatusAction,
   ISetSignUpLoadinStatusgAction,
   ISetUserAction,
   AuthActionsType,
   ISetAuthErrorAction,
   ISignIn,
   ISignUp,
   ISignOutAction,
} from './types'

export const checkAuth = () => ({
   type: AuthActionsType.FETCH_CHECK_AUTH,
})

export const fetchSignIn = (payload: ISignIn): IFetchSignInAction => ({
   type: AuthActionsType.FETCH_SIGN_IN,
   payload,
})

export const fetchSignUp = (payload: ISignUp): IFetchSignUpAction => ({
   type: AuthActionsType.FETCH_SIGN_UP,
   payload,
})

export const setUser = (payload: IUser | undefined): ISetUserAction => ({
   type: AuthActionsType.SET_USER,
   payload,
})

export const setAuthLoadingStatus = (
   payload: LoadingStatusType
): ISetAuthLoadingAction => ({
   type: AuthActionsType.SET_AUTH_LOADING,
   payload,
})

export const setSignInLoadingStatus = (
   payload: LoadingStatusType
): ISetSignInLoadingStatusAction => ({
   type: AuthActionsType.SET_SIGN_IN_LOADING_STATUS,
   payload,
})

export const setSignUpLoadingStatus = (
   payload: LoadingStatusType
): ISetSignUpLoadinStatusgAction => ({
   type: AuthActionsType.SET_SIGN_UP_LOADING_STATUS,
   payload,
})

export const logout = (): ISignOutAction => ({
   type: AuthActionsType.SIGN_OUT,
})

export const setAuthError = (payload: string): ISetAuthErrorAction => ({
   type: AuthActionsType.SET_AUTH_ERROR,
   payload,
})

import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import allReducers from './allReducers'
import rootSaga from './sagas'

const composeEnhancers =
   //@ts-ignore
   (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
   compose

const sagaMidleware = createSagaMiddleware()

const appReducer = combineReducers(allReducers)

const rootReducer = (state: any, action: any) => {
   if (action.type === 'auth/SIGN_OUT') {
      return appReducer(undefined, action)
   }

   return appReducer(state, action)
}

export const store = createStore(
   rootReducer,
   composeEnhancers(applyMiddleware(sagaMidleware))
)

sagaMidleware.run(rootSaga)

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch

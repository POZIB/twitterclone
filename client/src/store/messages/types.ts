import { Action } from 'redux'
import IMessage from '../../models/IMessage'
import { LoadingStatusType } from '../types/LoadingStatusType'

export interface ISendMessageMessageData {
   text: string
   dialogId: number
}

export interface IMessagesState {
   messages: IMessage[]
   loadingStatusMessages: LoadingStatusType
   sendMessageLoadingStatus: LoadingStatusType
   isSubmitSendMessage: boolean
   countUnreadLastSMS: number | undefined
   mutableMessage: IMessage | undefined
}

export enum MessagesActionsType {
   FETCH_MESSAGES = 'messages/FETCH_MESSAGES',
   SET_MESSAGES = 'messages/SET_MESSAGES',
   SET_MESSAGES_LOADING_STATUS = 'messages/SET_MESSAGES_LOADING_STATUS',
   SET_SEND_MESSAGE_LOADING_STATUS = 'messages/SET_SEND_MESSAGE_LOADING_STATUS',
   SET_IS_SUBMIT_SEND_MESSAGE = 'messages/SET_IS_SUBMIT_SEND_MESSAGE',
   FETCH_SEND_MESSAGE = 'messages/FETCH_SEND_MESSAGE',
   SEND_MESSAGE = 'messages/SEND_MESSAGE',
   DELETE_MESSAGE = 'messages/DELETE_MESSAGE',
   FETCH_COUNT_UNREAD_SMS = 'messages/FETCH_COUNT_UNREAD_SMS',
   SET_COUNT_UNREAD_SMS = 'messages/SET_COUNT_UNREAD_SMS',
   READING_MESSAGES = 'messages/READING_MESSAGES',
   MUTABLE_MESSAGE = 'messages/MUTABLE_MESSAGE',
   UPDATE_MESSAGE = 'messages/UPDATE_MESSAGE',
}

export interface ISetMessagesLoadingStatusAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.SET_MESSAGES_LOADING_STATUS
   payload: LoadingStatusType
}

export interface IFetchMessagesAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.FETCH_MESSAGES
   payload: number
}

export interface ISetMessagesAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.SET_MESSAGES
   payload: IMessage[]
}

export interface ISetSendMessageLoadingStatusAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.SET_SEND_MESSAGE_LOADING_STATUS
   payload: LoadingStatusType
}

export interface ISetIsSubmitSendMessageAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.SET_IS_SUBMIT_SEND_MESSAGE
   payload: boolean
}

export interface IFetchSendMessageAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.FETCH_SEND_MESSAGE
   payload: ISendMessageMessageData
}

export interface ISendMessageAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.SEND_MESSAGE
   payload: IMessage
}

export interface IDeleteMessageAction extends Action<MessagesActionsType> {
   type: MessagesActionsType.DELETE_MESSAGE
   payload: IMessage
}

export interface IFetchCountUnreadSMS extends Action<MessagesActionsType> {
   type: MessagesActionsType.FETCH_COUNT_UNREAD_SMS
}

export interface ISetCountUnreadSMS extends Action<MessagesActionsType> {
   type: MessagesActionsType.SET_COUNT_UNREAD_SMS
   payload: number | undefined
}

export interface ISetIsReadMessages extends Action<MessagesActionsType> {
   type: MessagesActionsType.READING_MESSAGES
   payload: IMessage[]
}

export interface ISetMutableMessage extends Action<MessagesActionsType> {
   type: MessagesActionsType.MUTABLE_MESSAGE
   payload: IMessage | undefined
}

export interface IUpdateMessage extends Action<MessagesActionsType> {
   type: MessagesActionsType.UPDATE_MESSAGE
   payload: IMessage
}

export type MessagesActions =
   | IFetchMessagesAction
   | ISetMessagesAction
   | ISetMessagesLoadingStatusAction
   | ISetIsSubmitSendMessageAction
   | IFetchSendMessageAction
   | ISetSendMessageLoadingStatusAction
   | ISendMessageAction
   | IDeleteMessageAction
   | ISetCountUnreadSMS
   | IFetchCountUnreadSMS
   | ISetIsReadMessages
   | ISetMutableMessage
   | IUpdateMessage

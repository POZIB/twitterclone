import { call, put, takeLatest } from 'redux-saga/effects'
import { AxiosResponse } from 'axios'

import IMessage from './../../models/IMessage'

import {
   sendMessage,
   setCountUnreadSMS,
   setMessages,
   setMessagesLoadingStatus,
   setSendMessageLoadingStatus,
} from './actionCreators'
import {
   IFetchMessagesAction,
   IFetchSendMessageAction,
   MessagesActionsType,
} from './types'
import MessagesApi from '../../api/MessagesApi'
import { LoadingStatusType } from '../types/LoadingStatusType'

export function* fetchMessageRequest({ payload }: IFetchMessagesAction) {
   try {
      const { data }: AxiosResponse<IMessage[]> = yield call(
         MessagesApi.fetchMessages,
         payload
      )
      yield put(setMessages(data))
   } catch (error) {
      yield put(setMessagesLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export function* fetchSendMessageRequest({ payload }: IFetchSendMessageAction) {
   try {
      const { data }: AxiosResponse<IMessage> = yield call(
         MessagesApi.sendMessage,
         payload
      )
      yield put(sendMessage(data))
   } catch (error) {
      yield put(setSendMessageLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export function* fetchCountUnreadLastMessage() {
   try {
      const { data }: AxiosResponse<number> = yield call(MessagesApi.fetchCountUnread)
      yield put(setCountUnreadSMS(data))
   } catch (error) {}
}

export default function* messagesSaga() {
   yield takeLatest(MessagesActionsType.FETCH_MESSAGES, fetchMessageRequest)
   yield takeLatest(MessagesActionsType.FETCH_SEND_MESSAGE, fetchSendMessageRequest)
   yield takeLatest(
      MessagesActionsType.FETCH_COUNT_UNREAD_SMS,
      fetchCountUnreadLastMessage
   )
}

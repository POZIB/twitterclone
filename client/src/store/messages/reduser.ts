import produce, { Draft } from 'immer'
import { LoadingStatusType } from '../types/LoadingStatusType'

import { MessagesActions, MessagesActionsType, IMessagesState } from './types'

const initialState: IMessagesState = {
   messages: [],
   loadingStatusMessages: LoadingStatusType.NEVER,
   isSubmitSendMessage: false,
   sendMessageLoadingStatus: LoadingStatusType.NEVER,
   countUnreadLastSMS: undefined,
   mutableMessage: undefined,
}

export const messagesReducer = produce(
   (draft: Draft<IMessagesState>, action: MessagesActions) => {
      switch (action.type) {
         case MessagesActionsType.FETCH_MESSAGES: {
            draft.loadingStatusMessages = LoadingStatusType.LOADING
            break
         }
         case MessagesActionsType.SET_MESSAGES: {
            draft.messages = action.payload
            draft.loadingStatusMessages = LoadingStatusType.LOADED
            break
         }
         case MessagesActionsType.SET_MESSAGES_LOADING_STATUS: {
            draft.loadingStatusMessages = action.payload
            break
         }

         case MessagesActionsType.SET_SEND_MESSAGE_LOADING_STATUS: {
            draft.sendMessageLoadingStatus = action.payload
            break
         }

         case MessagesActionsType.SET_IS_SUBMIT_SEND_MESSAGE: {
            draft.isSubmitSendMessage = action.payload
            break
         }

         case MessagesActionsType.FETCH_SEND_MESSAGE: {
            draft.isSubmitSendMessage = false
            draft.sendMessageLoadingStatus = LoadingStatusType.LOADING
            break
         }
         case MessagesActionsType.SEND_MESSAGE: {
            draft.messages.push(action.payload)
            draft.sendMessageLoadingStatus = LoadingStatusType.LOADED
            draft.isSubmitSendMessage = true
            break
         }

         case MessagesActionsType.DELETE_MESSAGE: {
            draft.messages = draft.messages.filter(item => item.id != action.payload.id)
            break
         }

         case MessagesActionsType.SET_COUNT_UNREAD_SMS: {
            draft.countUnreadLastSMS = action.payload
            break
         }

         case MessagesActionsType.READING_MESSAGES: {
            if (!action.payload.length) {
               break
            }
            draft.messages = draft.messages.map(item => {
               for (let i = 0; i < action.payload.length; i++) {
                  if (action.payload[i].id === item.id) {
                     return action.payload[i]
                  }
               }
               return item
            })
            break
         }

         case MessagesActionsType.MUTABLE_MESSAGE: {
            draft.mutableMessage = action.payload
            break
         }

         case MessagesActionsType.UPDATE_MESSAGE: {
            draft.messages = draft.messages.map(item =>
               item.id === action.payload.id ? action.payload : item
            )
            draft.isSubmitSendMessage = true
            break
         }

         default:
            return draft
      }
   },
   initialState
)

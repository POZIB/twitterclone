import IMessage from '../../models/IMessage'
import { LoadingStatusType } from '../types/LoadingStatusType'
import {
   IDeleteMessageAction,
   IFetchMessagesAction,
   IFetchSendMessageAction,
   ISendMessageAction,
   ISendMessageMessageData,
   ISetMessagesAction,
   ISetMessagesLoadingStatusAction,
   ISetSendMessageLoadingStatusAction,
   MessagesActionsType,
   ISetCountUnreadSMS,
   IFetchCountUnreadSMS,
   ISetIsReadMessages,
   ISetIsSubmitSendMessageAction,
   ISetMutableMessage,
   IUpdateMessage,
} from './types'

export const fetchMessages = (dialogId: number): IFetchMessagesAction => ({
   type: MessagesActionsType.FETCH_MESSAGES,
   payload: dialogId,
})

export const setMessages = (payload: IMessage[]): ISetMessagesAction => ({
   type: MessagesActionsType.SET_MESSAGES,
   payload,
})

export const setMessagesLoadingStatus = (
   payload: LoadingStatusType
): ISetMessagesLoadingStatusAction => ({
   type: MessagesActionsType.SET_MESSAGES_LOADING_STATUS,
   payload,
})

export const setSendMessageLoadingStatus = (
   payload: LoadingStatusType
): ISetSendMessageLoadingStatusAction => ({
   type: MessagesActionsType.SET_SEND_MESSAGE_LOADING_STATUS,
   payload,
})

export const setIsSubmitSendMessage = (
   payload: boolean
): ISetIsSubmitSendMessageAction => ({
   type: MessagesActionsType.SET_IS_SUBMIT_SEND_MESSAGE,
   payload,
})

export const fetchSendMessage = (
   payload: ISendMessageMessageData
): IFetchSendMessageAction => ({
   type: MessagesActionsType.FETCH_SEND_MESSAGE,
   payload,
})

export const sendMessage = (payload: IMessage): ISendMessageAction => ({
   type: MessagesActionsType.SEND_MESSAGE,
   payload,
})

export const deleteMessage = (payload: IMessage): IDeleteMessageAction => ({
   type: MessagesActionsType.DELETE_MESSAGE,
   payload,
})

export const fetchCountUnreadSMS = (): IFetchCountUnreadSMS => ({
   type: MessagesActionsType.FETCH_COUNT_UNREAD_SMS,
})

export const setCountUnreadSMS = (payload: number | undefined): ISetCountUnreadSMS => ({
   type: MessagesActionsType.SET_COUNT_UNREAD_SMS,
   payload,
})

export const redingMessages = (payload: IMessage[]): ISetIsReadMessages => ({
   type: MessagesActionsType.READING_MESSAGES,
   payload,
})

export const setMutableMessage = (payload: IMessage | undefined): ISetMutableMessage => ({
   type: MessagesActionsType.MUTABLE_MESSAGE,
   payload,
})

export const updateMessage = (payload: IMessage): IUpdateMessage => ({
   type: MessagesActionsType.UPDATE_MESSAGE,
   payload,
})

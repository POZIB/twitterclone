import { messagesReducer } from './messages/reduser'
import { authReducer } from './auth/reduser'
import { dialogsReducer } from './dialogs/reduser'
import { topicalsReducer } from './topical/reduser'
import { tweetsReducer } from './tweets/reduser'

export default {
   auth: authReducer,
   tweets: tweetsReducer,
   topicals: topicalsReducer,
   dialogs: dialogsReducer,
   messages: messagesReducer,
}

import { all } from 'redux-saga/effects'
import authSaga from './auth/saga'
import tweetsSaga from './tweets/saga'
import topicalsSaga from './topical/saga'
import dialogsSaga from './dialogs/saga'
import messagesSaga from './messages/saga'

export default function* rootSaga() {
   yield all([authSaga(), tweetsSaga(), topicalsSaga(), dialogsSaga(), messagesSaga()])
}

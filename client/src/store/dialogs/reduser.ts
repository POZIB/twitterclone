import produce, { current, Draft } from 'immer'
import { LoadingStatusType } from '../types/LoadingStatusType'

import { DialogsActionsType, DialogsActions, IDialogsState } from './types'

const initialState: IDialogsState = {
   dialogs: [],
   selectDialog: undefined,
   loadingStatusDialogs: LoadingStatusType.NEVER,
   newDialog: undefined,
}

export const dialogsReducer = produce(
   (draft: Draft<IDialogsState>, action: DialogsActions) => {
      switch (action.type) {
         case DialogsActionsType.FETCH_DIALOGS: {
            draft.dialogs = []
            draft.loadingStatusDialogs = LoadingStatusType.LOADING
            break
         }
         case DialogsActionsType.SET_DIALOGS: {
            draft.dialogs = action.payload
            draft.loadingStatusDialogs = LoadingStatusType.LOADED
            break
         }
         case DialogsActionsType.SET_DIALOGS_LOADING_STATUS: {
            draft.loadingStatusDialogs = action.payload
            break
         }

         case DialogsActionsType.SET_SElECT_DIALOG: {
            draft.selectDialog =
               draft.dialogs.find(item => item.id === action.payload) || draft.newDialog
            break
         }

         case DialogsActionsType.ADD_DIALOG: {
            draft.dialogs.unshift(action.payload)
            if (draft.newDialog && action.payload.id === draft.newDialog.id) {
               draft.newDialog = undefined
            }
            break
         }
         case DialogsActionsType.UPDATE_DIALOG: {
            draft.dialogs = draft.dialogs
               .map(item =>
                  item.id === action.payload.id
                     ? {
                          ...action.payload,
                          isRecording: item.isRecording,
                          isTyping: item.isTyping,
                       }
                     : item
               )
               .sort((a, b) => {
                  return (
                     new Date(b.lastMessage?.createdAt || b.dateCreate).getTime() -
                     new Date(a.lastMessage?.createdAt || a.dateCreate).getTime()
                  )
               })
            break
         }

         case DialogsActionsType.NEW_DIALOG: {
            draft.newDialog = action.payload
            break
         }

         case DialogsActionsType.SET_IS_RECODING_DIALOG: {
            draft.dialogs = draft.dialogs.map(item =>
               item.id === action.payload.dialogId
                  ? { ...item, isRecording: action.payload.flag }
                  : item
            )
            break
         }

         case DialogsActionsType.SET_IS_TYPING_DIALOG: {
            draft.dialogs = draft.dialogs.map(item =>
               item.id === action.payload.dialogId
                  ? { ...item, isTyping: action.payload.flag }
                  : item
            )
            break
         }

         default:
            return draft
      }
   },
   initialState
)

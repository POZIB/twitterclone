import { Action } from 'redux'

import IDialog from '../../models/IDialog'
import { LoadingStatusType } from '../types/LoadingStatusType'

export interface IDialogsState {
   dialogs: IDialog[]
   selectDialog: IDialog | undefined
   loadingStatusDialogs: LoadingStatusType
   newDialog: IDialog | undefined
}

export interface IPayloadRecodingAndTyping {
   dialogId: number
   flag: boolean
}

export enum DialogsActionsType {
   FETCH_DIALOGS = 'dialogs/FETCH_DIALOGS',
   SET_DIALOGS = 'dialogs/SET_DIALOGS',
   SET_DIALOGS_LOADING_STATUS = 'dialogs/SET_DIALOGS_LOADING_STATUS',
   SElECT_DIALOG = 'dialogs/SElECT_DIALOG',
   SET_SElECT_DIALOG = 'dialogs/SET_SElECT_DIALOG',
   ADD_DIALOG = 'dialogs/ADD_DIALOG',
   UPDATE_DIALOG = 'dialogs/UPDATE_DIALOG',
   NEW_DIALOG = 'dialogs/NEW_DIALOG',
   SET_IS_RECODING_DIALOG = 'dialogs/SET_IS_RECODING_DIALOG',
   SET_IS_TYPING_DIALOG = 'dialogs/SET_IS_TYPING_DIALOG',
}

export interface IFetchDialogsAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.FETCH_DIALOGS
}

export interface ISetDialogsAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.SET_DIALOGS
   payload: IDialog[]
}

export interface ISetDialogsLoadingStatusAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.SET_DIALOGS_LOADING_STATUS
   payload: LoadingStatusType
}

export interface ISelectDialogAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.SElECT_DIALOG
}

export interface ISetSelectDialogAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.SET_SElECT_DIALOG
   payload: number | undefined
}

export interface IAddDialogAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.ADD_DIALOG
   payload: IDialog
}

export interface IUpdateDialogAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.UPDATE_DIALOG
   payload: IDialog
}

export interface INewDialogAction extends Action<DialogsActionsType> {
   type: DialogsActionsType.NEW_DIALOG
   payload: IDialog | undefined
}

export interface ISetIsRecordingDialog extends Action<DialogsActionsType> {
   type: DialogsActionsType.SET_IS_RECODING_DIALOG
   payload: IPayloadRecodingAndTyping
}

export interface ISetIsTypingDialog extends Action<DialogsActionsType> {
   type: DialogsActionsType.SET_IS_TYPING_DIALOG
   payload: IPayloadRecodingAndTyping
}

export type DialogsActions =
   | IFetchDialogsAction
   | ISetDialogsAction
   | ISetDialogsLoadingStatusAction
   | ISelectDialogAction
   | ISetSelectDialogAction
   | IAddDialogAction
   | IUpdateDialogAction
   | INewDialogAction
   | ISetIsRecordingDialog
   | ISetIsTypingDialog

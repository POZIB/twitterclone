import IDialog from '../../models/IDialog'
import IUser from '../../models/IUser'
import { LoadingStatusType } from '../types/LoadingStatusType'
import {
   IFetchDialogsAction,
   ISetDialogsAction,
   ISetDialogsLoadingStatusAction,
   ISetSelectDialogAction,
   DialogsActionsType,
   IAddDialogAction,
   IUpdateDialogAction,
   INewDialogAction,
   ISetIsRecordingDialog,
   ISetIsTypingDialog,
   IPayloadRecodingAndTyping,
} from './types'

export const fetchDialogs = (): IFetchDialogsAction => ({
   type: DialogsActionsType.FETCH_DIALOGS,
})

export const setDialogs = (payload: IDialog[]): ISetDialogsAction => ({
   type: DialogsActionsType.SET_DIALOGS,
   payload,
})

export const setDialogsLoadingStatus = (
   payload: LoadingStatusType
): ISetDialogsLoadingStatusAction => ({
   type: DialogsActionsType.SET_DIALOGS_LOADING_STATUS,
   payload,
})

export const setSelectDialog = (payload: number | undefined): ISetSelectDialogAction => ({
   type: DialogsActionsType.SET_SElECT_DIALOG,
   payload,
})

export const addDialog = (payload: IDialog): IAddDialogAction => ({
   type: DialogsActionsType.ADD_DIALOG,
   payload,
})

export const upadteDialog = (payload: IDialog): IUpdateDialogAction => ({
   type: DialogsActionsType.UPDATE_DIALOG,
   payload,
})

export const newDialog = (ownerId: number, companion: IUser): INewDialogAction => {
   const id = Math.floor(5000 + Math.random() * (999999999 + 1 - 5000))
   const payload: IDialog = {
      id,
      ownerId,
      companion,
      countUnreadMessages: 0,
      lastMessage: null,
      dateCreate: '',
   }
   return { type: DialogsActionsType.NEW_DIALOG, payload }
}

export const resetNewDialog = (): INewDialogAction => ({
   type: DialogsActionsType.NEW_DIALOG,
   payload: undefined,
})

export const setIsRecording = (
   payload: IPayloadRecodingAndTyping
): ISetIsRecordingDialog => ({
   type: DialogsActionsType.SET_IS_RECODING_DIALOG,
   payload,
})

export const setIsTyping = (payload: IPayloadRecodingAndTyping): ISetIsTypingDialog => ({
   type: DialogsActionsType.SET_IS_TYPING_DIALOG,
   payload,
})

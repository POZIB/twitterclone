import { call, put, takeLatest } from 'redux-saga/effects'
import { AxiosResponse } from 'axios'

import IDialog from './../../models/IDialog'

import { setDialogs, setDialogsLoadingStatus } from './actionCreators'
import { DialogsActionsType } from './types'
import DialogsApi from '../../api/DialogsApi'
import { LoadingStatusType } from '../types/LoadingStatusType'

export function* fetchDialogsRequest() {
   try {
      const { data }: AxiosResponse<IDialog[]> = yield call(DialogsApi.fetchDialogs)
      yield put(setDialogs(data))
   } catch (error) {
      yield put(setDialogsLoadingStatus(LoadingStatusType.ERROR))
   }
}

export default function* dialogsSaga() {
   yield takeLatest(DialogsActionsType.FETCH_DIALOGS, fetchDialogsRequest)
}

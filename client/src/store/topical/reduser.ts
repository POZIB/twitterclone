import produce, { Draft } from 'immer'
import { LoadingStatusType } from '../types/LoadingStatusType'

import { TopicalsActions, TopicalsActionsType, ITopicalsState } from './types'

const initialState: ITopicalsState = {
   topicals: [],
   loadingStatusTopicals: LoadingStatusType.NEVER,
}

export const topicalsReducer = produce(
   (draft: Draft<ITopicalsState>, action: TopicalsActions) => {
      switch (action.type) {
         case TopicalsActionsType.FETCH_TOPICALS: {
            draft.topicals = []
            draft.loadingStatusTopicals = LoadingStatusType.LOADING
            break
         }
         case TopicalsActionsType.SET_TOPICALS: {
            draft.topicals = action.payload
            draft.loadingStatusTopicals = LoadingStatusType.LOADED
            break
         }
         case TopicalsActionsType.SET_TOPICALS_LOADING_STATUS: {
            draft.loadingStatusTopicals = action.payload
            break
         }

         default:
            return draft
      }
   },
   initialState
)

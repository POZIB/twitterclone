import ITopical from '../../models/ITopical'
import { LoadingStatusType } from '../types/LoadingStatusType'
import {
   IFetchTopicalsAction,
   ISetTopicalsAction,
   ISetTopicalsLoadingStatusAction,
   TopicalsActionsType,
} from './types'

export const fetchTopicals = (): IFetchTopicalsAction => ({
   type: TopicalsActionsType.FETCH_TOPICALS,
})

export const setTopicals = (payload: ITopical[]): ISetTopicalsAction => ({
   type: TopicalsActionsType.SET_TOPICALS,
   payload,
})

export const setTopicalsLoadingStatus = (
   payload: LoadingStatusType
): ISetTopicalsLoadingStatusAction => ({
   type: TopicalsActionsType.SET_TOPICALS_LOADING_STATUS,
   payload,
})

export type TopicalsActions = ISetTopicalsAction

import { call, put, takeLatest } from 'redux-saga/effects'
import { AxiosResponse } from 'axios'

import ITopical from './../../models/ITopical'

import { setTopicals, setTopicalsLoadingStatus } from './actionCreators'
import { TopicalsActionsType } from './types'
import TopicalsApi from '../../api/TopicalsApi'
import { LoadingStatusType } from '../types/LoadingStatusType'

export function* fetchTopicalsRequest() {
   try {
      const { data }: AxiosResponse<ITopical[]> = yield call(TopicalsApi.fetchTopicals)
      yield put(setTopicals(data))
   } catch (error) {
      yield put(setTopicalsLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export default function* topicalsSaga() {
   yield takeLatest(TopicalsActionsType.FETCH_TOPICALS, fetchTopicalsRequest)
}

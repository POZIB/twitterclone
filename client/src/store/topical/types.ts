import { Action } from 'redux'
import ITopical from '../../models/ITopical'
import { LoadingStatusType } from '../types/LoadingStatusType'

export interface ITopicalsState {
   topicals: ITopical[]
   loadingStatusTopicals: LoadingStatusType
}

export enum TopicalsActionsType {
   FETCH_TOPICALS = 'topical/FETCH_TOPICALS',
   SET_TOPICALS = 'topical/SET_TOPICALS',
   SET_TOPICALS_LOADING_STATUS = 'topical/SET_TOPICALS_LOADING_STATUS',
}

export interface IFetchTopicalsAction extends Action<TopicalsActionsType> {
   type: TopicalsActionsType.FETCH_TOPICALS
}

export interface ISetTopicalsAction extends Action<TopicalsActionsType> {
   type: TopicalsActionsType.SET_TOPICALS
   payload: ITopical[]
}

export interface ISetTopicalsLoadingStatusAction extends Action<TopicalsActionsType> {
   type: TopicalsActionsType.SET_TOPICALS_LOADING_STATUS
   payload: LoadingStatusType
}

export type TopicalsActions =
   | IFetchTopicalsAction
   | ISetTopicalsAction
   | ISetTopicalsLoadingStatusAction

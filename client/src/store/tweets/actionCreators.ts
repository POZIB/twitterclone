import ITweet from '../../models/ITweet'
import { LoadingStatusType } from '../types/LoadingStatusType'
import {
   IFetchTweetsAction,
   ISetTweetsAction,
   TweetsActionsType,
   ISetTweetsLoadingStatusAction,
   IAddTweetAction,
   IFetchAddTweetAction,
   IDeleteTweetAction,
   IPayloadAddTweet,
   IFetchDeleteTweetAction,
   ISetIsSubmitDeleteAction,
   ITweetAddingLoadingStatusAction,
   ISetIsSubmitAddAction,
   ITweetDeleteLoadingStatusAction,
   ISetErrorTweetsAction,
} from './types'

export const fetchTweets = (): IFetchTweetsAction => ({
   type: TweetsActionsType.FETCH_TWEETS,
})

export const setTweets = (payload: ITweet[]): ISetTweetsAction => ({
   type: TweetsActionsType.SET_TWEETS,
   payload,
})

export const setTweetsLoadingStatus = (
   payload: LoadingStatusType
): ISetTweetsLoadingStatusAction => ({
   type: TweetsActionsType.SET_TWEETS_LOADING_STATUS,
   payload,
})

export const setIsLoadingAddTweet = (
   payload: LoadingStatusType
): ITweetAddingLoadingStatusAction => ({
   type: TweetsActionsType.SET_TWEET_ADDING_LOADING_STATUS,
   payload,
})

export const setIsSumbitAddTweet = (payload: boolean): ISetIsSubmitAddAction => ({
   type: TweetsActionsType.SET_IS_SUBMIT_ADD,
   payload,
})

export const fetchAddTweet = (payload: IPayloadAddTweet): IFetchAddTweetAction => ({
   type: TweetsActionsType.FETCH_ADD_TWEET,
   payload,
})

export const addTweet = (payload: ITweet): IAddTweetAction => ({
   type: TweetsActionsType.ADD_TWEET,
   payload,
})

export const setIsLoadingDeleteTweet = (
   payload: LoadingStatusType
): ITweetDeleteLoadingStatusAction => ({
   type: TweetsActionsType.SET_TWEET_DELETE_LOADING_STATUS,
   payload,
})

export const setIsSumbitDeleteTweet = (payload: boolean): ISetIsSubmitDeleteAction => ({
   type: TweetsActionsType.SET_IS_SUBMIT_DELETE,
   payload,
})

export const fetchDeleteTweet = (payload: number): IFetchDeleteTweetAction => ({
   type: TweetsActionsType.FETCH_DELETE_TWEET,
   payload,
})

export const deleteTweet = (payload: number): IDeleteTweetAction => ({
   type: TweetsActionsType.DELETE_TWEET,
   payload,
})

export const setErrorTweet = (payload: string): ISetErrorTweetsAction => ({
   type: TweetsActionsType.SET_ERROR_TWEETS,
   payload,
})

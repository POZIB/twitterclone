import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import { AxiosError, AxiosResponse } from 'axios'

import ITweet from './../../models/ITweet'
import TweetsApi from '../../api/TweetsApi'

import {
   addTweet,
   deleteTweet,
   setTweets,
   setTweetsLoadingStatus,
   setIsLoadingAddTweet,
   setIsLoadingDeleteTweet,
   setErrorTweet,
} from './actionCreators'
import { IFetchAddTweetAction, IFetchDeleteTweetAction, TweetsActionsType } from './types'
import { LoadingStatusType } from '../types/LoadingStatusType'

export function* fetchTweetsRequest() {
   try {
      const { data }: AxiosResponse<ITweet[]> = yield call(TweetsApi.fetchTweets)
      yield put(setTweets(data))
   } catch (error) {
      yield put(setTweetsLoadingStatus(LoadingStatusType.ERROR)) //dispatch
   }
}

export function* fetchAddTweetRequest({ payload }: IFetchAddTweetAction) {
   try {
      const { data }: AxiosResponse<ITweet> = yield call(TweetsApi.addTweet, payload)
      yield put(addTweet(data))
   } catch (error) {
      const err = error as AxiosError<{ message: string }>
      if (err.response) {
         yield put(setErrorTweet(err.response?.data.message))
      }
      yield put(setIsLoadingAddTweet(LoadingStatusType.ERROR)) //dispatch
   }
}

export function* fetchDeleteTweetRequest({ payload }: IFetchDeleteTweetAction) {
   try {
      const { data }: AxiosResponse<ITweet> = yield call(TweetsApi.deleteTweet, payload)
      if (data.id) {
         yield put(deleteTweet(data.id))
      }
   } catch (error) {
      const err = error as AxiosError<{ message: string }>
      if (err.response) {
         yield put(setErrorTweet(err.response?.data.message))
      }
      yield put(setIsLoadingDeleteTweet(LoadingStatusType.ERROR))
   }
}

export default function* tweetsSaga() {
   yield takeLatest(TweetsActionsType.FETCH_TWEETS, fetchTweetsRequest)
   yield takeLatest(TweetsActionsType.FETCH_ADD_TWEET, fetchAddTweetRequest)
   yield takeLatest(TweetsActionsType.FETCH_DELETE_TWEET, fetchDeleteTweetRequest)
}

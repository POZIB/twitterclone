import produce, { Draft } from 'immer'
import { LoadingStatusType } from '../types/LoadingStatusType'

import { TweetsActionsType, ITweetsState, TweetsActions } from './types'

const initialState: ITweetsState = {
   tweets: [],
   loadingStatusTweets: LoadingStatusType.NEVER,
   loadingStatusAddTweet: LoadingStatusType.NEVER,
   loadingStatusDeleteTweet: LoadingStatusType.NEVER,
   isSubmitDeleteTweet: false,
   isSubmitAddTweet: false,
   errorTweets: '',
}

export const tweetsReducer = produce(
   (draft: Draft<ITweetsState>, action: TweetsActions) => {
      switch (action.type) {
         case TweetsActionsType.FETCH_TWEETS: {
            draft.tweets = []
            draft.loadingStatusTweets = LoadingStatusType.LOADING
            break
         }
         case TweetsActionsType.SET_TWEETS: {
            draft.tweets = action.payload
            draft.loadingStatusTweets = LoadingStatusType.LOADED
            break
         }
         case TweetsActionsType.SET_TWEETS_LOADING_STATUS: {
            draft.loadingStatusTweets = action.payload
            break
         }

         case TweetsActionsType.SET_IS_SUBMIT_ADD: {
            draft.isSubmitAddTweet = action.payload
            break
         }

         case TweetsActionsType.FETCH_ADD_TWEET: {
            draft.errorTweets = ''
            draft.isSubmitAddTweet = false
            draft.loadingStatusAddTweet = LoadingStatusType.LOADING
            break
         }
         case TweetsActionsType.ADD_TWEET: {
            draft.isSubmitAddTweet = true
            draft.tweets.unshift(action.payload)
            draft.loadingStatusAddTweet = LoadingStatusType.LOADED
            break
         }
         case TweetsActionsType.SET_TWEET_DELETE_LOADING_STATUS: {
            draft.loadingStatusDeleteTweet = action.payload
            break
         }

         case TweetsActionsType.SET_IS_SUBMIT_DELETE: {
            draft.isSubmitDeleteTweet = action.payload
            break
         }

         case TweetsActionsType.FETCH_DELETE_TWEET: {
            draft.errorTweets = ''
            draft.isSubmitDeleteTweet = false
            draft.loadingStatusDeleteTweet = LoadingStatusType.LOADING
            break
         }

         case TweetsActionsType.DELETE_TWEET: {
            draft.isSubmitDeleteTweet = true
            draft.tweets = draft.tweets.filter(item => item.id !== action.payload)
            draft.loadingStatusDeleteTweet = LoadingStatusType.LOADED
            break
         }
         case TweetsActionsType.SET_ERROR_TWEETS: {
            draft.errorTweets = action.payload
            break
         }

         default:
            return draft
      }
   },
   initialState
)

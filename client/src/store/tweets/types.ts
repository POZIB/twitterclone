import { Action } from 'redux'
import ITweet from '../../models/ITweet'
import { LoadingStatusType } from '../types/LoadingStatusType'

export interface IPayloadAddTweet {
   text: string
   images: string[]
}

export interface ITweetsState {
   tweets: ITweet[]
   loadingStatusTweets: LoadingStatusType
   loadingStatusAddTweet: LoadingStatusType
   loadingStatusDeleteTweet: LoadingStatusType
   isSubmitAddTweet: boolean
   isSubmitDeleteTweet: boolean
   errorTweets: string
}

export enum TweetsActionsType {
   FETCH_TWEETS = 'tweets/FETCH_TWEETS',
   SET_TWEETS = 'tweets/SET_TWEETS',
   SET_TWEETS_LOADING_STATUS = 'tweets/SET_TWEETS_LOADING_STATUS',
   FETCH_ADD_TWEET = 'tweets/FETCH_ADD_TWEET',
   ADD_TWEET = 'tweets/ADD_TWEET',
   SET_TWEET_ADDING_LOADING_STATUS = 'tweets/SET_TWEET_ADDING_LOADING_STATUS',
   FETCH_DELETE_TWEET = 'tweets/FETCH_DELETE_TWEET',
   DELETE_TWEET = 'tweets/DELETE_TWEET',
   SET_TWEET_DELETE_LOADING_STATUS = 'tweets/SET_TWEET_DELETE_LOADING_STATUS',
   SET_IS_SUBMIT_DELETE = 'tweets/SET_IS_SUBMIT_DELETE',
   SET_IS_SUBMIT_ADD = 'tweets/SET_IS_SUBMIT_ADD',
   SET_ERROR_TWEETS = 'tweets/SET_ERROR_TWEETS',
}

export interface IFetchTweetsAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.FETCH_TWEETS
}

export interface ISetTweetsAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.SET_TWEETS
   payload: ITweet[]
}

export interface ISetTweetsLoadingStatusAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.SET_TWEETS_LOADING_STATUS
   payload: LoadingStatusType
}

export interface ITweetAddingLoadingStatusAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.SET_TWEET_ADDING_LOADING_STATUS
   payload: LoadingStatusType
}

export interface ISetIsSubmitAddAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.SET_IS_SUBMIT_ADD
   payload: boolean
}

export interface IFetchAddTweetAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.FETCH_ADD_TWEET
   payload: IPayloadAddTweet
}

export interface IAddTweetAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.ADD_TWEET
   payload: ITweet
}

export interface ITweetDeleteLoadingStatusAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.SET_TWEET_DELETE_LOADING_STATUS
   payload: LoadingStatusType
}

export interface ISetIsSubmitDeleteAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.SET_IS_SUBMIT_DELETE
   payload: boolean
}
export interface IFetchDeleteTweetAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.FETCH_DELETE_TWEET
   payload: number
}

export interface IDeleteTweetAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.DELETE_TWEET
   payload: number
}

export interface ISetErrorTweetsAction extends Action<TweetsActionsType> {
   type: TweetsActionsType.SET_ERROR_TWEETS
   payload: string
}

export type TweetsActions =
   | IFetchTweetsAction
   | ISetTweetsAction
   | ISetTweetsLoadingStatusAction
   | IFetchAddTweetAction
   | IAddTweetAction
   | ITweetAddingLoadingStatusAction
   | IDeleteTweetAction
   | IFetchDeleteTweetAction
   | ITweetDeleteLoadingStatusAction
   | ISetIsSubmitDeleteAction
   | ISetIsSubmitAddAction
   | ISetErrorTweetsAction

export enum LoadingStatusType {
   LOADED = 'LOADED',
   LOADING = 'LOADING',
   ERROR = 'ERROR',
   NEVER = 'NEVER',
}

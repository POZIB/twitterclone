import React from 'react'
import { IconButton, styled } from '@mui/material'
import { ImageOutlined } from '@mui/icons-material'
import IImage from '../../models/IImage'
import UploadFileApi from '../../api/UploadFileApi'

interface IProps {
   images: IImage[]
   max?: number
   multiple?: boolean
   icon?: 'hidden' | 'disabled'
   onChange: (value: React.SetStateAction<IImage[]>) => void
}

const UploadImages: React.FC<IProps> = ({
   images,
   max,
   multiple,
   icon,
   onChange,
}): React.ReactElement => {
   const [isImagesLoading, setIsImagesLoading] = React.useState(false)

   const MAX_IMAGES = max || 4
   const isMaxImages = images.length >= MAX_IMAGES

   React.useEffect(() => {
      setIsImagesLoading(!!images.find(item => item.status === 'LOADING'))
   }, [images])

   const fetchUpload = async (image: File) => {
      try {
         const res = await UploadFileApi.upload(image)
         onChange(prev =>
            prev.map(item =>
               item.file?.name === image.name && item.url.startsWith('blob')
                  ? { ...item, url: res.data.url, status: 'LOADED' }
                  : item
            )
         )
      } catch (error) {
         onChange(prev =>
            prev.map(item =>
               item.file?.name === image.name && item.url.startsWith('blob')
                  ? { ...item, status: 'ERROR' }
                  : item
            )
         )
      }
   }

   const handleClick = (event: React.MouseEvent<HTMLElement>) => {
      if (isMaxImages) {
         event.preventDefault()
      }
   }

   const handleChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
      if (!event.target.files) return

      if (!multiple) {
         if (event.target.files.length >= MAX_IMAGES) {
            return
         }
         const file = event.target.files[0]
         const urlObj = new Blob([file])
         onChange(prev => [
            ...prev,
            { file, url: URL.createObjectURL(urlObj), status: 'LOADING' },
         ])

         await fetchUpload(file)
      } else {
         const files = event.target.files

         Array.from(files).forEach((file, index) => {
            if (images.length + index >= MAX_IMAGES) {
               return
            }

            const urlObj = new Blob([file])
            onChange(prev => [
               ...prev,
               { file, url: URL.createObjectURL(urlObj), status: 'LOADING' },
            ])
         })

         for (const file of Array.from(files)) {
            await fetchUpload(file)
         }
      }
      event.target.value = ''
   }

   return (
      <div>
         <IconButton
            color='primary'
            component='label'
            onClick={handleClick}
            disabled={isMaxImages || isImagesLoading}
            sx={{ display: icon === 'hidden' && isMaxImages ? 'none' : 'block' }}
         >
            <input
               id='input-field'
               hidden
               type='file'
               accept='image/*'
               onChange={handleChange}
               max={max}
               multiple={multiple}
            />
            <ImageOutlined fontSize='medium' />
         </IconButton>
      </div>
   )
}

export default UploadImages

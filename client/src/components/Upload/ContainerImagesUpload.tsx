import React from 'react'
import { styled } from '@mui/material'
import { Cancel, Loop, Close } from '@mui/icons-material'

import Loader from '../Loader'
import IImage from '../../models/IImage'
import UploadFileApi from '../../api/UploadFileApi'

const List = styled('div')(() => ({
   display: 'flex',
   flexWrap: 'wrap',
}))

const ListItem = styled('div')(() => ({
   maxWidth: 'calc(50% - 8px)',
   borderRadius: '20px',
   marginRight: '10px',
   backgroundSize: 'cover',
   backgroundPosition: 'center',
   position: 'relative',
   overflow: 'hidden',
   margin: '4px',
   '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
   },
}))

const ContainerLoader = styled('div')(() => ({
   width: '100%',
   height: '100%',
   position: 'absolute',
   backgroundColor: 'rgb(35 35 35 / 60%)',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
}))

const ContainerRemove = styled('div')(() => ({
   height: '32px',
   width: '32px',
   position: 'absolute',
   top: '5px',
   left: '5px',
   color: '#0f1419bf',
   transitionProperty: 'background-color, box-shadow',
   transitionDuration: '0.22s',
   backdropFilter: 'blur(4px)',
   backgroundColor: 'rgba(15, 20, 25, 0.75)',
   border: '1px solid transparent',
   borderRadius: '50%',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   cursor: 'pointer',
   '&:hover': {
      backgroundColor: 'rgba(39,44, 48, 0.75)',
   },
}))

const ContainerRepeat = styled('div')(() => ({
   width: '100%',
   height: '100%',
   position: 'absolute',
   backgroundColor: 'rgb(35 35 35 / 82%)',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
}))

const RemoveIcon = styled(Close)(() => ({
   color: '#fff',
}))

const LoopIcon = styled(Loop)(() => ({
   height: '35%',
   width: '35%',
   cursor: 'pointer',
   color: '#e1e1e1',
}))

interface IProps {
   items: IImage[]
   onChange: (value: React.SetStateAction<IImage[]>) => void
}

const ContainerImagesUpload: React.FC<IProps> = ({ items, onChange }) => {
   const handleRemove = (_item: IImage) => {
      onChange(prev => prev.filter(item => item.url !== _item.url))
   }

   const handleRepeat = async (_item: IImage) => {
      if (!_item.file) return

      onChange(prev =>
         prev.map(item =>
            item.file?.name === _item.file?.name ? { ...item, status: 'LOADING' } : item
         )
      )
      try {
         const res = await UploadFileApi.upload(_item.file)
         onChange(prev =>
            prev.map(item =>
               item.file?.name === res.data.fileName && item.url.startsWith('blob')
                  ? { ...item, url: res.data.url, status: 'LOADED' }
                  : item
            )
         )
      } catch (error) {
         onChange(prev =>
            prev.map(item =>
               item.file?.name === _item.file?.name && item.url.startsWith('blob')
                  ? { ...item, status: 'ERROR' }
                  : item
            )
         )
      }
   }

   return (
      <List>
         {items.map((item, index) => (
            <ListItem key={index} sx={{ maxHeight: items.length > 2 ? '150px' : '100%' }}>
               <ContainerLoader
                  sx={{
                     display: item.status === 'LOADING' ? 'flex' : 'none',
                  }}
               >
                  <Loader size={38} />
               </ContainerLoader>
               <ContainerRepeat
                  sx={{
                     display: item.status === 'ERROR' ? 'flex' : 'none',
                  }}
               >
                  <LoopIcon onClick={() => handleRepeat(item)} />
               </ContainerRepeat>
               <img src={item.url} alt={item.file?.name} />
               <ContainerRemove>
                  <RemoveIcon onClick={() => handleRemove(item)} />
               </ContainerRemove>
            </ListItem>
         ))}
      </List>
   )
}

export default ContainerImagesUpload

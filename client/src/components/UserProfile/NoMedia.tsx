import React from 'react'
import { styled, Typography } from '@mui/material'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import IUser from '../../models/IUser'

const ContainerNoResults = styled('div')(() => ({
   maxWidth: '400px',
   display: 'flex',
   flexDirection: 'column',
   alignItems: 'flex-start',
   justifyContent: 'center',
   margin: '32px auto',
   padding: '0 20px',
   '& span': {
      marginBottom: '8px',
   },
}))

interface IProps {
   user: IUser | undefined
}

const NoMedia: React.FC<IProps> = ({ user }) => {
   return (
      <ContainerNoResults>
         <Typography fontSize='31px' fontWeight='700' component='span' lineHeight='30px'>
            Свет, камера, вложения!
         </Typography>
         <Typography variant='caption'>
            Когда вы отправляете твиты с фотографиями или видео, они будут отображаться
            здесь.
         </Typography>
         <Typography variant='caption'>(В разработке...)</Typography>
      </ContainerNoResults>
   )
}

export default NoMedia

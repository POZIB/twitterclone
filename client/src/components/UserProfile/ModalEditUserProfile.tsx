import React from 'react'
import {
   styled,
   Dialog,
   IconButton,
   Button,
   Typography,
   Box,
   Avatar,
   DialogProps,
   LinearProgress,
} from '@mui/material'
import { CameraEnhanceOutlined, Close, CloseOutlined } from '@mui/icons-material'
import { useDispatch } from 'react-redux'

import { setUser } from '../../store/auth/actionCreators'
import UploadFileApi from '../../api/UploadFileApi'
import TextInput from '../UI/TextInput'
import IImage from '../../models/IImage'
import UserApi from '../../api/UserApi'
import IUserProfile from '../../models/IUserProfile'

const Wrapper = styled('div')(() => ({
   minHeight: '400px',
   maxWidth: '80vw',
   maxHeight: '90vh',
   height: '650px',
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const Container = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   minHeight: '0',
}))

const Content = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   overflow: 'auto',
}))

const WrapperHeader = styled('div')(() => ({
   position: 'sticky',
   top: '0',
   zIndex: '2',
}))

const Header = styled('div')(() => ({
   height: '53px',
   display: 'flex',
   alignItems: 'center',
   padding: '0 8px',
   backgroundColor: 'rgba(255, 255, 255, 0.85)',
   backdropFilter: 'blur(12px)',
}))

const ContentHeader = styled('div')(() => ({
   display: 'flex',
   justifyContent: 'flex-start',
   minWidth: '56px',
}))

const WrapperTitle = styled('div')(() => ({
   flex: '1',
}))

const SaveButton = styled(Button)(() => ({
   backgroundColor: 'rgb(15, 20, 25)',
   color: '#fff',
   padding: ' 0 16px',
   minHeight: '32px',
   '&:hover': {
      backgroundColor: 'rgb(39, 44, 48)',
   },
}))

const Body = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   paddingBottom: '45px',
}))

const ContainerBackgroundAvatar = styled('div')(() => ({
   position: 'relative',
   display: 'flex',
   flex: '1',
   maxHeight: '250px',
   border: '2px solid #fff',
   overflow: 'hidden',
   backgroundColor: 'rgba(0, 0, 0, 0.3)',
}))

const BackgroundAvatarBackdrop = styled('div')(() => ({
   width: '100%',
   height: '100%',
   position: 'absolute',
   top: '0',
   left: '0',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
}))

const BackgroundAvatar = styled('div')(() => ({
   width: '100%',
   backgroundRepeat: 'no-repeat',
   backgroundSize: 'cover',
   backgroundPosition: 'center',
}))

const ContainerAvatar = styled('div')(() => ({
   maxWidth: '9rem',
   width: '25%',
   marginTop: '-3rem',
   marginLeft: '1rem',
   position: 'relative',
}))

const WrappperAvatar = styled('div')(() => ({
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   overflow: 'hidden',
}))

const AvatarBackdrop = styled('div')(() => ({
   width: '100%',
   height: '100%',
   position: 'absolute',
   top: '0',
   left: '0',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   backgroundColor: 'rgba(0, 0, 0, 0.3)',
   borderRadius: '50%',
   border: '4px solid #fff',
}))

const ContainerTextField = styled('div')(() => ({
   padding: '12px 16px',
}))

const WrapperIcon = styled('label')(() => ({
   minWidth: '44px',
   minHeight: '44px',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   backdropFilter: 'blur(4px)',
   backgroundColor: 'rgba(15, 20, 25, 0.75)',
   opacity: '0.75',
   borderRadius: '50%',
   overflow: 'hidden',
   transition: 'background-color 0.2s, box-shadow 0.2s',
   cursor: 'pointer',
   '&:hover': {
      backgroundColor: 'rgba(39, 44, 48, 0.75)',
   },
   '& svg': {
      color: '#fff',
   },
}))

const LineError = styled('div')(() => ({
   height: '4px',
   width: '100%',
   backgroundColor: '#ff2c2c',
}))

interface IProps {
   onClose: () => void
   user: IUserProfile
}

const ModalEditUserProfile: React.FC<IProps & DialogProps> = ({
   onClose,
   user,
   ...props
}) => {
   const dispatch = useDispatch()
   const [fullName, setFullName] = React.useState<string>(user.fullName)
   const [about, setAbout] = React.useState<string>(user.about || '')
   const [location, setLocation] = React.useState<string>(user.location || '')
   const [website, setWebsite] = React.useState<string>(user.website || '')
   const [avatar, setAvatar] = React.useState<IImage>({ url: user.avatar || '' })
   const [avatarBackground, setAvatarBackground] = React.useState<IImage>({
      url: user.avatarBackground || '',
   })
   const [loading, setLoading] = React.useState(false)
   const [error, setError] = React.useState(false)

   React.useEffect(() => {
      setFullName(user.fullName)
      setAbout(user.about || '')
      setLocation(user.location || '')
      setWebsite(user.website || '')
      setAvatar({ url: user.avatar || '' })
      setAvatarBackground({ url: user.avatarBackground || '' })
   }, [user, onClose])

   const handleSubmit = async () => {
      try {
         if (!user) return

         setLoading(true)
         const userUpdate = { ...user }
         userUpdate.fullName = fullName
         userUpdate.about = about
         userUpdate.location = location
         userUpdate.website = website
         userUpdate.avatarBackground = avatarBackground.url

         if (user.avatar !== avatar.url && avatar.file) {
            const res = await UploadFileApi.upload(avatar.file)
            userUpdate.avatar = res.data.url
         }
         if (user.avatarBackground !== avatarBackground.url && avatarBackground.file) {
            const res = await UploadFileApi.upload(avatarBackground.file)
            userUpdate.avatarBackground = res.data.url
         }

         if (
            user.fullName !== fullName ||
            user.about !== about ||
            user.location !== location ||
            user.website !== website ||
            user.avatar !== avatar.url ||
            user.avatarBackground !== avatarBackground.url
         ) {
            await UserApi.updateProfile(userUpdate)
            dispatch(setUser(userUpdate))
         }
         handleClose()
      } catch (error) {
         setError(true)
         console.log(error)
      } finally {
         setLoading(false)
      }
   }

   const handleSelectAvatar = (event: React.ChangeEvent<HTMLInputElement>) => {
      if (!event.target.files) return
      const file = event.target.files[0]
      setAvatar({ file, url: URL.createObjectURL(new Blob([file])) })
   }

   const handleSelectAvatarBackground = (event: React.ChangeEvent<HTMLInputElement>) => {
      if (!event.target.files) return
      const file = event.target.files[0]
      setAvatarBackground({ file, url: URL.createObjectURL(new Blob([file])) })
   }
   const handleDeleteAvatarBackground = () => {
      setAvatarBackground({ url: '' })
   }

   const handleClose = () => {
      onClose()
   }

   return (
      <Dialog onClose={handleClose} {...props}>
         <Wrapper>
            <Container>
               <Content>
                  <WrapperHeader>
                     <Header>
                        <ContentHeader>
                           <IconButton color='inherit' onClick={onClose}>
                              <Close />
                           </IconButton>
                        </ContentHeader>

                        <WrapperTitle>
                           <Typography
                              variant='h2'
                              fontSize='20px'
                              lineHeight='24px'
                              fontWeight='700'
                           >
                              Изменение профиля
                           </Typography>
                        </WrapperTitle>
                        <SaveButton onClick={handleSubmit}>Сохранить</SaveButton>
                     </Header>
                     {loading && <LinearProgress />}
                     {error && <LineError />}
                  </WrapperHeader>

                  <Body>
                     <ContainerBackgroundAvatar>
                        <Box
                           sx={{
                              paddingBottom: 'calc(100%/2)',
                           }}
                        ></Box>
                        <BackgroundAvatar
                           sx={{
                              backgroundImage: avatarBackground.url
                                 ? `url(${avatarBackground.url})`
                                 : '',
                           }}
                        />
                        <BackgroundAvatarBackdrop
                           sx={{
                              backgroundColor:
                                 avatarBackground.url && 'rgba(0, 0, 0, 0.3)',
                           }}
                        >
                           <WrapperIcon>
                              <input
                                 type='file'
                                 hidden
                                 onChange={handleSelectAvatarBackground}
                              />
                              <CameraEnhanceOutlined />
                           </WrapperIcon>
                           {avatarBackground.url && (
                              <WrapperIcon
                                 sx={{ ml: '20px' }}
                                 onClick={handleDeleteAvatarBackground}
                              >
                                 <CloseOutlined />
                              </WrapperIcon>
                           )}
                        </BackgroundAvatarBackdrop>
                     </ContainerBackgroundAvatar>

                     <ContainerAvatar>
                        <WrappperAvatar>
                           <Avatar
                              src={avatar.url}
                              sx={{
                                 border: '4px solid #fff',
                                 width: '100%',
                                 height: '140px',
                                 backgroundColor: 'rgba(0, 0, 0, 0.3)',
                              }}
                           />
                           <AvatarBackdrop>
                              <WrapperIcon>
                                 <input
                                    type='file'
                                    hidden
                                    onChange={handleSelectAvatar}
                                 />
                                 <CameraEnhanceOutlined />
                              </WrapperIcon>
                           </AvatarBackdrop>
                        </WrappperAvatar>
                     </ContainerAvatar>
                     <ContainerTextField>
                        <TextInput
                           label='ФИО'
                           maxLenght={50}
                           value={fullName}
                           onChange={setFullName}
                        />
                     </ContainerTextField>
                     <ContainerTextField>
                        <TextInput
                           label='О вас'
                           maxLenght={160}
                           value={about}
                           onChange={setAbout}
                           inputBaseProps={{ multiline: true }}
                        />
                     </ContainerTextField>
                     <ContainerTextField>
                        <TextInput
                           label='Местоположение'
                           maxLenght={70}
                           value={location}
                           onChange={setLocation}
                        />
                     </ContainerTextField>
                     <ContainerTextField>
                        <TextInput
                           label='Сайт'
                           maxLenght={100}
                           value={website}
                           onChange={setWebsite}
                        />
                     </ContainerTextField>
                  </Body>
               </Content>
            </Container>
         </Wrapper>
      </Dialog>
   )
}

export default ModalEditUserProfile

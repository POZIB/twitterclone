import React from 'react'
import { styled, Typography } from '@mui/material'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import IUser from '../../models/IUser'

const ContainerNoResults = styled('div')(() => ({
   maxWidth: '400px',
   display: 'flex',
   flexDirection: 'column',
   alignItems: 'flex-start',
   justifyContent: 'center',
   margin: '32px auto',
   padding: '0 20px',
   '& span': {
      marginBottom: '8px',
   },
}))

interface IProps {
   user: IUser | undefined
}

const NoTweetsAndReplies: React.FC<IProps> = ({ user }) => {
   const { user: myProfile } = useTypedSelector(state => state.auth)

   return (
      <ContainerNoResults>
         <Typography fontSize='31px' fontWeight='700' component='span' lineHeight='30px'>
            {myProfile?.id === user?.id
               ? 'У вас пока нет твитов и ответов на них'
               : 'У него пока нет твитов и ответов на них'}
         </Typography>
         <Typography variant='caption'>
            {myProfile?.id === user?.id
               ? 'Напишите твит, расскажите в нем что сегодня делали и ждите реакции на него. Когда вы это сделаете, они появится здесь.'
               : 'Напишите ему сообщение о том, как сделать твит, чтобы в дальнейшем на него могли оставлять ответы. Когда он это сделает, они появится здесь.'}
         </Typography>
         <Typography variant='caption'>(В разработке...)</Typography>
      </ContainerNoResults>
   )
}

export default NoTweetsAndReplies

import React from 'react'
import { styled, Typography } from '@mui/material'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import IUser from '../../models/IUser'

const ContainerNoResults = styled('div')(() => ({
   maxWidth: '400px',
   display: 'flex',
   flexDirection: 'column',
   alignItems: 'flex-start',
   justifyContent: 'center',
   margin: '32px auto',
   padding: '0 20px',
   '& span': {
      marginBottom: '8px',
   },
}))

interface IProps {
   user: IUser | undefined
}

const NoLikes: React.FC<IProps> = ({ user }) => {
   const { user: myProfile } = useTypedSelector(state => state.auth)

   return (
      <ContainerNoResults>
         <Typography fontSize='31px' fontWeight='700' component='span' lineHeight='30px'>
            {myProfile?.id === user?.id
               ? 'У вас пока нет лайков'
               : 'У него пока нет лайков'}
         </Typography>
         <Typography variant='caption'>
            {myProfile?.id === user?.id
               ? 'Коснитесь сердечка в любом твите, чтобы показать ему свою любовь. Когда вы это сделаете, он появится здесь'
               : 'Напишите ему сообщение о том, как поставить лайк на твит. Когда он это сделает, он появится здесь'}
         </Typography>
         <Typography variant='caption'>(В разработке...)</Typography>
      </ContainerNoResults>
   )
}

export default NoLikes

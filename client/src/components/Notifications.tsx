import React from 'react'
import { Avatar, Box, IconButton, Snackbar, Typography, styled } from '@mui/material'
import { CloseOutlined } from '@mui/icons-material'

import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import socket from '../socket'

import { fetchCountUnreadSMS } from '../store/messages/actionCreators'
import IUser from '../models/IUser'

const Wrapper = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
}))

const WrapperAvatar = styled('div')(({ theme }) => ({
   display: 'flex',
   cursor: 'pointer',
   marginRight: '12px',
}))

const Content = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   color: '#fff',
}))

const FullName = styled('span')(({ theme }) => ({
   fontSize: '1rem',
   fontWeight: '500',
   lineHeight: '1',
   cursor: 'pointer',
   transition: 'color 0.12s',
   '&:hover': {
      color: '#eee',
   },
}))

const ContainerText = styled('div')(({ theme }) => ({
   minWidth: '0',
   maxWidth: '200px',
   flex: '1',
}))

const Text = styled('span')(({ theme }) => ({
   textOverflow: 'ellipsis',
   overflow: 'hidden',
   wordWrap: 'break-word',
   '@supports (-webkit-line-clamp: 2)': {
      '-webkit-line-clamp': '3',
      display: '-webkit-box',
      '-webkit-box-orient': 'vertical',
   },
}))

interface INotificationSMS {
   dialogId: number
   text: string
   companion: IUser
}

interface INotificationSMS {
   dialogId: number
   text: string
   companion: IUser
}

const Notifications: React.FC = () => {
   const naviagete = useNavigate()
   const dispatch = useDispatch()
   const [openNotificationSMS, setOpenNotificationSMS] = React.useState(false)
   const [message, setMessage] = React.useState<INotificationSMS | null>(null)

   React.useEffect(() => {
      socket.on('MESSAGES:NOTIFICATION', (data: INotificationSMS) => {
         setMessage(data)
         setOpenNotificationSMS(true)
         dispatch(fetchCountUnreadSMS())
      })

      return () => {
         socket.removeListener('MESSAGES:NOTIFICATION')
      }
   }, [socket])

   const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
      if (reason === 'clickaway') {
         return
      }

      setOpenNotificationSMS(false)
      event?.stopPropagation()
   }

   const handlClickLinkOnUser = (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation()
      naviagete('/user/' + message?.companion.userName)
   }

   const handleClickSnackbar = (e: React.MouseEvent<HTMLDivElement>) => {
      e.stopPropagation()
      naviagete('/messages/' + message?.dialogId)
   }

   const actionSMS = (
      <Wrapper>
         <WrapperAvatar onClick={handlClickLinkOnUser}>
            <Avatar src={message?.companion.avatar} />
         </WrapperAvatar>
         <Content>
            <FullName onClick={handlClickLinkOnUser}>
               {message?.companion.fullName}
            </FullName>

            <ContainerText>
               <Text>{message?.text}</Text>
            </ContainerText>
         </Content>
      </Wrapper>
   )

   return (
      <>
         <Snackbar
            onClick={handleClickSnackbar}
            open={openNotificationSMS}
            autoHideDuration={6200}
            onClose={handleClose}
            message={actionSMS}
            action={
               <IconButton
                  size='small'
                  aria-label='close'
                  color='inherit'
                  onClick={handleClose}
               >
                  <CloseOutlined fontSize='small' />
               </IconButton>
            }
         />
      </>
   )
}

export default Notifications

import React from 'react'
import { useDispatch } from 'react-redux'
import { Link, Outlet, useNavigate } from 'react-router-dom'
import { useForm, SubmitHandler, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import LoadingButton from '@mui/lab/LoadingButton'
import { styled, Typography } from '@mui/material'

import Modal from '../Modal'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { fetchSignIn } from '../../store/auth/actionCreators'
import { ISignIn } from '../../store/auth/types'
import TextInput from '../UI/TextInput'

const Wrapper = styled('div')(() => ({
   height: '420px',
   maxWidth: '300px',
   margin: '0 auto',
}))

const WrapperInput = styled('div')(() => ({
   padding: '8px 0',
   '& .MuiInputBase-root-MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
      border: 'none',
   },
}))

const ContainerInputs = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'space-between',
}))

const ContainerBottomText = styled('div')(() => ({
   marginTop: '40px',
}))

const inputsValidation = yup.object().shape({
   email: yup.string().email('Неправильный формат Email').required('Введите Email'),
   password: yup
      .string()
      .min(4, 'Минимальная длина 6 символов')
      .required('Введите пароль'),
})

const SignInModal: React.FC = (): React.ReactElement => {
   const dispatch = useDispatch()
   const navigate = useNavigate()

   const { user, authError, signInLoadingStatus } = useTypedSelector(state => state.auth)

   const {
      handleSubmit,
      formState: { errors },
      control,
      reset,
      resetField,
   } = useForm<ISignIn>({
      resolver: yupResolver(inputsValidation),
   })

   React.useEffect(() => {
      if (signInLoadingStatus === 'ERROR') {
         resetField('password')
      }
   }, [signInLoadingStatus])

   const onSubmit: SubmitHandler<ISignIn> = data => {
      dispatch(fetchSignIn(data))
   }

   const handleClose = () => {
      reset()
      navigate('/i/flow')
   }

   return (
      <Modal maxWidth='sm' padding='0 32px 48px' isOpen={true} onClose={handleClose}>
         <Wrapper>
            <Typography
               variant='h2'
               fontSize='31px'
               lineHeight='36px'
               fontWeight='700'
               my='20px'
            >
               Вход в Твиттер
            </Typography>

            <form onSubmit={handleSubmit(onSubmit)}>
               <ContainerInputs>
                  <WrapperInput>
                     <Controller
                        name='email'
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                           <TextInput
                              label='Email'
                              helperText={errors.email?.message}
                              error={!!errors.email}
                              inputBaseProps={field}
                           />
                        )}
                     />
                  </WrapperInput>
                  <WrapperInput>
                     <Controller
                        name='password'
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                           <TextInput
                              label='Пароль'
                              helperText={errors.password?.message}
                              error={!!errors.password}
                              inputBaseProps={{ ...field, type: 'password' }}
                           />
                        )}
                     />
                  </WrapperInput>
               </ContainerInputs>
               <div>
                  <LoadingButton
                     loading={signInLoadingStatus === 'LOADING'}
                     variant='contained'
                     color='primary'
                     type='submit'
                     fullWidth
                     sx={{
                        height: '35px',
                        my: '12px',
                        color: '#fff',
                        backgroundColor: 'rgb(15,20,25)',
                        transition: 'background-color 0.2s',
                        '&:hover': {
                           backgroundColor: 'rgb(39,44,48)',
                        },
                     }}
                  >
                     Войти
                  </LoadingButton>
                  <LoadingButton
                     variant='contained'
                     color='primary'
                     fullWidth
                     onClick={() => navigate('/i/flow/password_reset')}
                     sx={{
                        height: '35px',
                        my: '12px',
                        color: 'rgb(15,20,25)',
                        backgroundColor: 'rgb(255,255,255)',
                        border: '1px solid rgb(207, 217, 222)',
                        transition: 'background-color 0.2s',
                        '&:hover': {
                           backgroundColor: 'rgba(15,20,25, 0.1)',
                        },
                     }}
                  >
                     Забыли пароль?
                  </LoadingButton>

                  <ContainerBottomText>
                     <Typography
                        variant='caption'
                        mt='40px'
                        sx={{
                           '& a': {
                              color: 'rgb(29, 155, 240)',
                           },
                        }}
                     >
                        Нет учетной записи?{' '}
                        <Link to='/i/flow/sign_up'>Зарегистрируйтесь</Link>
                     </Typography>
                  </ContainerBottomText>
               </div>
            </form>
         </Wrapper>
      </Modal>
   )
}

export default SignInModal

import React from 'react'
import { useNavigate } from 'react-router-dom'
import { styled, Typography, Button } from '@mui/material'
import completeImg from '../../../assets/complete.png'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const ContainerImg = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   padding: '30px 0',
   '& img': {
      maxHeight: '100%',
      minHeight: '100%',
      height: '0',
      objectFit: 'contain',
   },
}))

const ContainerButton = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
}))

const CompleteResetPassword = () => {
   const navigate = useNavigate()
   return (
      <Wrapper>
         <Typography
            variant='h2'
            fontSize='31px'
            lineHeight='36px'
            fontWeight='700'
            my='20px'
         >
            Пароль испешно изменен!
         </Typography>
         <Typography variant='caption'>
            Поздравляем с успешной сменой пароля. Осталось только войти в твиттер.
         </Typography>

         <ContainerImg>
            <img src={completeImg} alt='' />
         </ContainerImg>

         <ContainerButton>
            <Button
               onClick={() => navigate('/i/flow/sign_in')}
               sx={{
                  height: '50px',
                  my: '24px',
                  color: '#fff',
                  backgroundColor: 'rgb(15,20,25)',
                  transition: 'background-color 0.2s',
                  '&:hover': {
                     backgroundColor: 'rgb(39,44,48)',
                  },
               }}
            >
               Войти
            </Button>
         </ContainerButton>
      </Wrapper>
   )
}

export default CompleteResetPassword

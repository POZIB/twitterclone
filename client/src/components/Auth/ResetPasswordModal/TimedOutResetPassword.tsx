import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Button, styled, Typography } from '@mui/material'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const Container = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   justifyContent: 'flex-end',
}))

const TimedOutResetPassword = () => {
   const navigate = useNavigate()
   return (
      <Wrapper>
         <Typography
            variant='h2'
            fontSize='31px'
            lineHeight='36px'
            fontWeight='700'
            my='20px'
         >
            Ошибка сброса пароля
         </Typography>
         <Typography variant='caption'>
            Время сброса пароля истекло. Повторите попытку.
         </Typography>

         <Container>
            <Button
               onClick={() => navigate('/i/flow/password_reset')}
               sx={{
                  height: '50px',
                  my: '24px',
                  color: '#fff',
                  backgroundColor: 'rgb(15,20,25)',
                  transition: 'background-color 0.2s',
                  '&:hover': {
                     backgroundColor: 'rgb(39,44,48)',
                  },
               }}
            >
               Повторить попытку
            </Button>
            <Button
               variant='contained'
               color='primary'
               onClick={() => navigate('/i/flow')}
               sx={{
                  height: '50px',
                  my: '12px',
                  color: 'rgb(15,20,25)',
                  backgroundColor: 'rgb(255,255,255)',
                  border: '1px solid rgb(207, 217, 222)',
                  transition: 'background-color 0.2s',
                  '&:hover': {
                     backgroundColor: 'rgba(15,20,25, 0.1)',
                  },
               }}
            >
               Закрыть
            </Button>
         </Container>
      </Wrapper>
   )
}

export default TimedOutResetPassword

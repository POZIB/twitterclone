import React from 'react'
import { AxiosError } from 'axios'
import { useForm, SubmitHandler, Controller } from 'react-hook-form'
import { useSearchParams } from 'react-router-dom'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import LoadingButton from '@mui/lab/LoadingButton'
import { styled, Typography } from '@mui/material'

import NotificationError from '../../NotificationError'
import { useSnackbar } from '../../../hooks/useSnackbar'
import TextInput from '../../UI/TextInput'
import UserApi from '../../../api/UserApi'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const WrapperInput = styled('div')(() => ({
   padding: '8px 0',
   '& .MuiInputBase-root-MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
      border: 'none',
   },
}))

const Form = styled('form')(() => ({
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'space-between',
   flex: '1',
}))

const ContainerInputs = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'space-between',
}))

const inputsValidation = yup.object().shape({
   password: yup
      .string()
      .required('Введите новый пароль')
      .min(6, 'Минимальная длина 6 символа'),
   password2: yup.string().oneOf([yup.ref('password'), null], 'Пароли не совпадают'),
})

interface IProps {
   onComplete: () => void
}

const FormResetPassword: React.FC<IProps> = ({ onComplete }) => {
   const [searchParams] = useSearchParams()
   const [loading, setLoading] = React.useState(false)
   const { isOpen, message, openSnackBar } = useSnackbar()

   const {
      handleSubmit,
      formState: { errors },
      control,
      reset,
   } = useForm<{ password: string; password2: string }>({
      resolver: yupResolver(inputsValidation),
   })

   const onSubmit: SubmitHandler<{
      password: string
      password2: string
   }> = async data => {
      try {
         const hash = searchParams.get('hash')
         if (!hash) return

         if (hash) {
            setLoading(true)
            const res = await UserApi.resetPassword(hash, data.password, data.password2)
            if (res.data) {
               onComplete()
            }
         }
      } catch (error: Error | unknown) {
         const err = error as AxiosError<{ message: string }>
         openSnackBar(err.response?.data.message)
      } finally {
         setLoading(false)
         reset({ password: '', password2: '' })
      }
   }

   return (
      <Wrapper>
         <NotificationError open={isOpen} message={message} />
         <Typography
            variant='h2'
            fontSize='31px'
            lineHeight='36px'
            fontWeight='700'
            my='20px'
         >
            Сброс пароля
         </Typography>
         <Typography variant='caption'>
            Надежные пароли содержат цифры, буквы и знаки препинания.
         </Typography>

         <Form onSubmit={handleSubmit(onSubmit)}>
            <ContainerInputs>
               <WrapperInput>
                  <Controller
                     name='password'
                     control={control}
                     defaultValue=''
                     render={({ field }) => (
                        <TextInput
                           label='Введите новый пароль'
                           helperText={errors.password?.message}
                           error={!!errors.password}
                           inputBaseProps={{ type: 'password', ...field }}
                        />
                     )}
                  />
               </WrapperInput>
               <WrapperInput>
                  <Controller
                     name='password2'
                     control={control}
                     defaultValue=''
                     render={({ field }) => (
                        <TextInput
                           label='Введите пароль еще раз'
                           helperText={errors.password2?.message}
                           error={!!errors.password2}
                           inputBaseProps={{ type: 'password', ...field }}
                        />
                     )}
                  />
               </WrapperInput>
            </ContainerInputs>
            <LoadingButton
               fullWidth
               loading={loading}
               type='submit'
               variant='contained'
               color='primary'
               sx={{
                  height: '50px',
                  my: '24px',
                  color: '#fff',
                  backgroundColor: 'rgb(15,20,25)',
                  transition: 'background-color 0.2s',
                  '&:hover': {
                     backgroundColor: 'rgb(39,44,48)',
                  },
               }}
            >
               Сбросить пароль
            </LoadingButton>
         </Form>
      </Wrapper>
   )
}

export default FormResetPassword

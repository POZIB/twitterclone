import React from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'
import { styled } from '@mui/material'

import Modal from '../../Modal'
import FormResetPassword from './FormResetPassword'
import TimedOutResetPassword from './TimedOutResetPassword'
import CompleteResetPassword from './CompleteResetPassword'

const Wrapper = styled('div')(() => ({
   height: '450px',
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const ResetPasswordModal = () => {
   const [searchParams] = useSearchParams()
   const navigate = useNavigate()
   const [isComplete, setIsComplete] = React.useState(false)

   const handleClose = () => {
      navigate('/i/flow')
   }

   console.log()

   return (
      <Modal maxWidth='sm' padding='5px 80px' isOpen={true} onClose={handleClose}>
         <Wrapper>
            {isComplete ? (
               <CompleteResetPassword />
            ) : (
               <>
                  {searchParams.get('hash') !== 'expired' ? (
                     <FormResetPassword onComplete={() => setIsComplete(true)} />
                  ) : (
                     <TimedOutResetPassword />
                  )}
               </>
            )}
         </Wrapper>
      </Modal>
   )
}

export default ResetPasswordModal

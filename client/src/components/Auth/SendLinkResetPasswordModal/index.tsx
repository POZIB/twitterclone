import React from 'react'
import { useNavigate } from 'react-router-dom'
import { styled } from '@mui/material'

import Modal from '../../Modal'

import CompleteSendLink from './CompleteSendLink'
import FormSearchUserAndSendLink from './FormSearchUserAndSendLink'

const Wrapper = styled('div')(() => ({
   height: '450px',
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const SendLinkResetPasswordModal: React.FC = () => {
   const navigate = useNavigate()
   const [isFound, setIsFound] = React.useState(false)

   const handleClose = () => {
      navigate('/i/flow')
   }

   return (
      <Modal maxWidth='sm' padding='5px 80px' isOpen={true} onClose={handleClose}>
         <Wrapper>
            {isFound ? (
               <CompleteSendLink />
            ) : (
               <FormSearchUserAndSendLink onFound={() => setIsFound(true)} />
            )}
         </Wrapper>
      </Modal>
   )
}

export default SendLinkResetPasswordModal

import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Typography, styled } from '@mui/material'
import LoadingButton from '@mui/lab/LoadingButton'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const Container = styled('form')(() => ({
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'flex-end',
   flex: '1',
}))

const CompleteSendLink = () => {
   const naviage = useNavigate()
   return (
      <Wrapper>
         <Typography
            variant='h2'
            fontSize='31px'
            lineHeight='36px'
            fontWeight='700'
            my='20px'
         >
            Проверьте электронную почту
         </Typography>

         <Typography variant='caption'>
            Вы получите ссылку, перейдя по которой, вы сможете сбросить пароль.
         </Typography>

         <Container>
            <LoadingButton
               fullWidth
               type='submit'
               variant='contained'
               color='primary'
               onClick={() => naviage('/i/flow')}
               sx={{
                  height: '50px',
                  my: '24px',
                  color: '#fff',
                  backgroundColor: 'rgb(15,20,25)',
                  transition: 'background-color 0.2s',
                  '&:hover': {
                     backgroundColor: 'rgb(39,44,48)',
                  },
               }}
            >
               Закрыть
            </LoadingButton>
         </Container>
      </Wrapper>
   )
}

export default CompleteSendLink

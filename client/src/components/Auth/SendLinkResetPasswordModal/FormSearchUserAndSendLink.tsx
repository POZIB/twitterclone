import React from 'react'
import { useForm, SubmitHandler, Controller } from 'react-hook-form'
import { styled, Typography } from '@mui/material'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import LoadingButton from '@mui/lab/LoadingButton'

import TextInput from '../../UI/TextInput'
import NotificationError from '../../NotificationError'
import { useSnackbar } from '../../../hooks/useSnackbar'
import UserApi from '../../../api/UserApi'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const WrapperInput = styled('div')(() => ({
   padding: '8px 0',
   '& .MuiInputBase-root-MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
      border: 'none',
   },
}))

const ContainerInputs = styled('div')(() => ({
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'space-between',
}))

const Form = styled('form')(() => ({
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'space-between',
   flex: '1',
}))

interface IProps {
   onFound: () => void
}

const inputsValidation = yup.object().shape({
   email: yup.string().email('Неправильный формат Email').required('Введите Email'),
})

const FormSearchUserAndSendLink: React.FC<IProps> = ({ onFound }) => {
   const [loading, setLoading] = React.useState(false)
   const { isOpen, message, openSnackBar } = useSnackbar()
   const {
      handleSubmit,
      formState: { errors },
      control,
      reset,
   } = useForm<{ email: string }>({
      resolver: yupResolver(inputsValidation),
   })

   const onSubmit: SubmitHandler<{ email: string }> = async data => {
      try {
         setLoading(true)
         const res = await UserApi.sendLinkResetPassword(data.email)
         if (res.data) {
            onFound()
         } else {
            openSnackBar('Мы не смогли найти вашу учетную запись.')
         }
      } catch (error) {
         openSnackBar('Что то пошло не так. Повторите попытку.')
      } finally {
         setLoading(false)
         reset({ email: '' })
      }
   }

   return (
      <Wrapper>
         <NotificationError open={isOpen} message={message} />
         <Typography
            variant='h2'
            fontSize='31px'
            lineHeight='36px'
            fontWeight='700'
            my='20px'
         >
            Найдите свою учетную запись в Твиттере
         </Typography>

         <Form onSubmit={handleSubmit(onSubmit)}>
            <ContainerInputs>
               <WrapperInput>
                  <Controller
                     name='email'
                     control={control}
                     defaultValue=''
                     render={({ field }) => (
                        <TextInput
                           label='Email'
                           helperText={errors.email?.message}
                           error={!!errors.email}
                           inputBaseProps={field}
                        />
                     )}
                  />
               </WrapperInput>
            </ContainerInputs>
            <LoadingButton
               fullWidth
               loading={loading}
               type='submit'
               variant='contained'
               color='primary'
               sx={{
                  height: '50px',
                  my: '24px',
                  color: '#fff',
                  backgroundColor: 'rgb(15,20,25)',
                  transition: 'background-color 0.2s',
                  '&:hover': {
                     backgroundColor: 'rgb(39,44,48)',
                  },
               }}
            >
               Поиск
            </LoadingButton>
         </Form>
      </Wrapper>
   )
}

export default FormSearchUserAndSendLink

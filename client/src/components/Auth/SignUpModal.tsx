import React from 'react'
import { useDispatch } from 'react-redux'
import { useForm, SubmitHandler, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { Typography, styled } from '@mui/material'
import LoadingButton from '@mui/lab/LoadingButton'

import Modal from '../Modal'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { ISignUp } from '../../store/auth/types'
import { fetchSignUp } from '../../store/auth/actionCreators'
import TextInput from '../UI/TextInput'
import { useNavigate } from 'react-router-dom'

const WrapperInput = styled('div')(() => ({
   padding: '8px 0',
   '& .MuiInputBase-root-MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
      border: 'none',
   },
}))

const ContainerInputs = styled('div')(() => ({
   height: '380px',
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'space-between',
}))

const inputsValidation = yup.object().shape({
   email: yup.string().email('Неправильный формат Email').required('Введите Email'),
   fullName: yup.string().min(2, 'Минимальная длина 2 символа'),
   userName: yup.string().min(2, 'Минимальная длина 2 символа'),
   password: yup
      .string()
      .required('Введите пароль')
      .min(6, 'Минимальная длина 6 символа'),
})

const SignUpModal: React.FC = (): React.ReactElement => {
   const dispatch = useDispatch()
   const navigate = useNavigate()

   const { authLoadingStatus } = useTypedSelector(state => state.auth)
   const { user, authError, signUpLoadingStatus } = useTypedSelector(state => state.auth)
   const {
      handleSubmit,
      control,
      reset,
      resetField,
      formState: { errors },
   } = useForm<ISignUp>({
      resolver: yupResolver(inputsValidation),
   })

   React.useEffect(() => {
      if (signUpLoadingStatus === 'ERROR') {
         resetField('password')
      }
   }, [signUpLoadingStatus])

   const onSubmit: SubmitHandler<ISignUp> = data => {
      dispatch(fetchSignUp(data))
   }

   const handleClose = () => {
      navigate('/i/flow')
   }

   return (
      <Modal maxWidth='sm' padding='5px 80px' isOpen={true} onClose={handleClose}>
         <div>
            <Typography
               variant='h2'
               fontSize='31px'
               lineHeight='36px'
               fontWeight='700'
               my='20px'
            >
               Создайте учетную запись
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)}>
               <ContainerInputs>
                  <WrapperInput>
                     <Controller
                        name='fullName'
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                           <TextInput
                              label='Имя'
                              helperText={errors.fullName?.message}
                              error={!!errors.fullName}
                              inputBaseProps={field}
                           />
                        )}
                     />
                  </WrapperInput>

                  <WrapperInput>
                     <Controller
                        name='userName'
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                           <TextInput
                              label='Логин'
                              helperText={errors.userName?.message}
                              error={!!errors.userName}
                              inputBaseProps={field}
                           />
                        )}
                     />
                  </WrapperInput>
                  <WrapperInput>
                     <Controller
                        name='email'
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                           <TextInput
                              label='Email'
                              helperText={errors.email?.message}
                              error={!!errors.email}
                              inputBaseProps={field}
                           />
                        )}
                     />
                  </WrapperInput>
                  <WrapperInput>
                     <Controller
                        name='password'
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                           <TextInput
                              label='Пароль'
                              helperText={errors.password?.message}
                              error={!!errors.password}
                              inputBaseProps={{ ...field, type: 'password' }}
                           />
                        )}
                     />
                  </WrapperInput>
               </ContainerInputs>
               <LoadingButton
                  type='submit'
                  variant='contained'
                  color='primary'
                  fullWidth
                  loading={signUpLoadingStatus === 'LOADING'}
                  sx={{
                     height: '50px',
                     my: '24px',
                     color: '#fff',
                     backgroundColor: 'rgb(15,20,25)',
                     transition: 'background-color 0.2s',
                     '&:hover': {
                        backgroundColor: 'rgb(39,44,48)',
                     },
                  }}
               >
                  Далее
               </LoadingButton>
            </form>
         </div>
      </Modal>
   )
}

export default SignUpModal

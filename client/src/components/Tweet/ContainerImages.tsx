import React from 'react'
import { styled, Box } from '@mui/material'

import ITweet from '../../models/ITweet'

const ListImages = styled('div')(() => ({
   display: 'flex',
   justifyContent: 'center',
   borderRadius: '16px',
   border: '1px solid rgb(207, 217, 222)',
   overflow: 'hidden',
   boxSizing: 'border-box',
   margin: '-1px',
}))

const ItemImage = styled('div')(() => ({
   display: 'flex',
   flex: '1',
   minHeight: '0',
   '& img': {
      height: '100%',
      width: '100%',
      objectFit: 'cover',
   },
}))

interface IProps {
   tweet: ITweet
}

const ContainerImages: React.FC<IProps> = ({ tweet }) => {
   if (!tweet.images) {
      return <></>
   }

   return (
      <ListImages>
         {tweet.images.length === 1 && (
            <Box display='flex' flex='1'>
               <ItemImage>
                  <img src={tweet.images[0]} />
               </ItemImage>
            </Box>
         )}

         {tweet.images.length === 2 && (
            <Box display='flex' flex='1'>
               <ItemImage sx={{ mr: '2px' }}>
                  <img src={tweet.images[0]} />
               </ItemImage>
               <ItemImage>
                  <img src={tweet.images[1]} />
               </ItemImage>
            </Box>
         )}

         {tweet.images.length === 3 && (
            <Box display='flex' flex='1'>
               <Box display='flex' flex='1' minHeight={0}>
                  <ItemImage sx={{ mr: '2px' }}>
                     <img src={tweet.images[0]} />
                  </ItemImage>
               </Box>
               <Box display='flex' flexDirection='column' flex='1' minHeight={0}>
                  <ItemImage sx={{ mb: '2px' }}>
                     <img src={tweet.images[1]} />
                  </ItemImage>
                  <ItemImage>
                     <img src={tweet.images[2]} />
                  </ItemImage>
               </Box>
            </Box>
         )}

         {tweet.images.length === 4 && (
            <Box display='flex' flexDirection='column' flex='1'>
               <Box display='flex' flex='1' minHeight={0} sx={{ mb: '2px' }}>
                  <ItemImage sx={{ mr: '2px' }}>
                     <img src={tweet.images[0]} />
                  </ItemImage>
                  <ItemImage>
                     <img src={tweet.images[1]} />
                  </ItemImage>
               </Box>
               <Box display='flex' flex='1' minHeight={0}>
                  <ItemImage sx={{ mr: '2px' }}>
                     <img src={tweet.images[2]} />
                  </ItemImage>
                  <ItemImage>
                     <img src={tweet.images[3]} />
                  </ItemImage>
               </Box>
            </Box>
         )}
      </ListImages>
   )
}

export default ContainerImages

import React from 'react'
import { Avatar, Typography, IconButton, Box, Skeleton, styled } from '@mui/material'
import {
   ChatBubbleOutline,
   FavoriteBorderOutlined,
   MoreHorizOutlined,
   PresentToAllOutlined,
   RepeatOutlined,
} from '@mui/icons-material'

const Wrapper = styled('div')(({ theme }) => ({
   padding: '0 16px',
   borderBottom: '1px solid #e4e4e4',
}))

const AvatarWrapper = styled('div')(({ theme }) => ({
   marginRight: '12px',
}))

const Top = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'flex-start',
   marginBottom: '4px',
   marginTop: '6px',
}))

const Body = styled('div')(({ theme }) => ({
   marginTop: '12px',
}))

const NavigationContainer = styled('div')(() => ({
   marginTop: '12px',
}))

const NavigationList = styled('ul')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'space-around',
   padding: 0,
   margin: 0,
}))

const NavigationItem = styled('li')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
}))

const SkeletonTweet: React.FC = () => {
   return (
      <Wrapper>
         <Top>
            <AvatarWrapper>
               <Avatar alt='user name' sx={{ width: '48px', height: '48px' }} />
            </AvatarWrapper>
            <Box display='flex' justifyContent='space-between' width='100%'>
               <div>
                  <Skeleton variant='text' width='150px' />
                  <Skeleton variant='text' width='100px' />
               </div>
               <IconButton sx={{ p: 0 }}>
                  <MoreHorizOutlined />
               </IconButton>
            </Box>
         </Top>
         <Body>
            <div>
               <Skeleton variant='text' width='100%' />
               <Skeleton variant='text' width='100%' />
               <Skeleton variant='text' width='100%' />
            </div>
         </Body>
         <Skeleton variant='text' width='200px' />
         <NavigationContainer>
            <NavigationList>
               <NavigationItem>
                  <IconButton>
                     <ChatBubbleOutline fontSize='medium' />
                  </IconButton>
                  <Typography fontSize='14px'></Typography>
               </NavigationItem>
               <NavigationItem>
                  <IconButton>
                     <RepeatOutlined fontSize='medium' />
                  </IconButton>
                  <Typography fontSize='14px'></Typography>
               </NavigationItem>
               <NavigationItem>
                  <IconButton>
                     <FavoriteBorderOutlined fontSize='medium' />
                  </IconButton>
                  <Typography fontSize='14px'></Typography>
               </NavigationItem>
               <NavigationItem>
                  <IconButton>
                     <PresentToAllOutlined fontSize='medium' />
                  </IconButton>
                  <Typography fontSize='14px'></Typography>
               </NavigationItem>
            </NavigationList>
         </NavigationContainer>
      </Wrapper>
   )
}

export default SkeletonTweet

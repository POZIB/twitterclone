import React from 'react'
import { useDispatch } from 'react-redux'
import {
   Avatar,
   IconButton,
   LinearProgress,
   styled,
   OutlinedInput,
   Typography,
} from '@mui/material'
import LoadingButton from '@mui/lab/LoadingButton'
import { EmojiEmotionsOutlined } from '@mui/icons-material'
import { BaseEmoji } from 'emoji-mart'

import { fetchAddTweet, setIsSumbitDeleteTweet } from '../../store/tweets/actionCreators'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import CircularProgressProcent from '../UI/CircularProgressPercent'
import UploadImages from '../Upload/InputUploadImages'
import ContainerEmojiPicker from '../ContainerEmojiPicker'
import IImage from '../../models/IImage'
import ContainerImagesUpload from '../Upload/ContainerImagesUpload'

const Form = styled('div')(() => ({
   padding: '0 10px',
}))

const Body = styled('div')(() => ({
   paddingTop: '10px',
   display: 'flex',
   minHeight: '150px',
   userSelect: 'none',
}))

const WrapperAvatar = styled('div')(() => ({
   paddingRight: '10px',
}))

const ContainerTextArea = styled('div')(() => ({
   flex: '1',
}))

const InputText = styled(OutlinedInput)(({ theme }) => ({
   width: '100%',
   height: '100%',
   display: 'flex',
   alignItems: 'flex-start',
   borderRadius: '30px',
   padding: '2px',
   '&:hover  .MuiOutlinedInput-notchedOutline': {
      borderColor: 'rgba(29, 155, 240, 0.15)',
   },
   '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: 'rgba(29, 155, 240, 1)',
   },
}))

const Bottom = styled('div')(() => ({
   padding: '5px 0',
}))

const BottomBlockButtons = styled('div')(() => ({
   display: 'flex',
   justifyContent: 'space-between',
   alignItems: 'center',
}))

const LeftBlock = styled('div')(() => ({
   display: 'flex',
}))

const RightBlock = styled('div')(() => ({
   display: 'flex',
   gap: '7px',
   alignItems: 'center',
}))

const WrapperConent = styled('div')(() => ({
   flex: '1',
   display: 'flex',
   flexDirection: 'column',
}))

const ContainerAttachments = styled('div')(() => ({
   margin: '14px 0',
}))

const AttachemntsContainer = styled('div')(() => ({
   display: 'flex',
   flexWrap: 'wrap',
}))

const FormAddTweet: React.FC = (): React.ReactElement => {
   const dispatch = useDispatch()
   const { user } = useTypedSelector(state => state.auth)
   const { loadingStatusAddTweet, isSubmitAddTweet } = useTypedSelector(
      state => state.tweets
   )
   const [text, setText] = React.useState<string>('')
   const [images, setImages] = React.useState<IImage[]>([])
   const [isDisabledSubmit, setIsDisabledSubmit] = React.useState(true)

   const [anchorElEmoji, setAnchorElEmoji] = React.useState<null | HTMLElement>(null)
   const openEmoji = Boolean(anchorElEmoji)

   const MAX_LENGHT_TEXT = 280

   React.useEffect(() => {
      if (isSubmitAddTweet) {
         dispatch(setIsSumbitDeleteTweet(false))
         setImages([])
         setText('')
      }
   }, [isSubmitAddTweet])

   React.useEffect(() => {
      const isLoadingImages = !!images.find(
         item => item.status === 'LOADING' || item.status === 'ERROR'
      )
      const isContent = (images.length || text) && !isLoadingImages
      setIsDisabledSubmit(text.length > MAX_LENGHT_TEXT || !isContent)
   }, [text, images])

   const handleSubmitForm = async () => {
      dispatch(
         fetchAddTweet({
            text,
            images: images
               .filter(item => !item.url.startsWith('blob'))
               .map(item => item.url),
         })
      )
   }

   const textLimitPercent = (): number => {
      const percent = (text.length / MAX_LENGHT_TEXT) * 100
      if (percent <= 100) {
         return percent
      }
      return 100
   }

   const handleChangeTextArea = (e: React.ChangeEvent<HTMLInputElement>) => {
      if (e.currentTarget) {
         setText(e.currentTarget.value)
      }
   }

   const handleClickEmoji = (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorElEmoji(event.currentTarget)
   }
   const handleCloseEmoji = () => {
      setAnchorElEmoji(null)
   }

   const handleSelectEmoji = (e: BaseEmoji) => {
      setText(prev => prev + e.native)
   }

   return (
      <div>
         {loadingStatusAddTweet === 'LOADING' && <LinearProgress />}
         <Form>
            <Body>
               <WrapperAvatar>
                  <Avatar src={user?.avatar} />
               </WrapperAvatar>
               <WrapperConent>
                  <ContainerTextArea>
                     <InputText
                        placeholder='Что происходит?'
                        multiline
                        maxRows={25}
                        value={text}
                        onChange={handleChangeTextArea}
                        sx={{
                           ' .MuiOutlinedInput-notchedOutline': { border: 'none' },
                        }}
                     />
                  </ContainerTextArea>
                  {!!images.length && (
                     <ContainerAttachments>
                        <AttachemntsContainer>
                           <ContainerImagesUpload items={images} onChange={setImages} />
                        </AttachemntsContainer>
                     </ContainerAttachments>
                  )}
               </WrapperConent>
            </Body>
            <Bottom>
               <BottomBlockButtons>
                  <LeftBlock>
                     <UploadImages
                        multiple
                        max={4}
                        images={images}
                        onChange={setImages}
                     />
                     <IconButton color='primary' onClick={handleClickEmoji}>
                        <EmojiEmotionsOutlined fontSize='medium' />
                     </IconButton>
                  </LeftBlock>
                  <ContainerEmojiPicker
                     anchorEl={anchorElEmoji}
                     open={openEmoji}
                     onClose={handleCloseEmoji}
                     onEmojiSelect={handleSelectEmoji}
                     transformOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                     anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
                  />
                  <RightBlock>
                     {text && (
                        <>
                           <Typography fontSize={14}>
                              {MAX_LENGHT_TEXT - text.length}
                           </Typography>
                           <CircularProgressProcent valuePercent={textLimitPercent()} />
                        </>
                     )}

                     <LoadingButton
                        disabled={isDisabledSubmit}
                        loading={loadingStatusAddTweet === 'LOADING'}
                        variant='contained'
                        color='primary'
                        size='small'
                        onClick={handleSubmitForm}
                        sx={{ p: '6px 16px' }}
                     >
                        Твитнуть
                     </LoadingButton>
                  </RightBlock>
               </BottomBlockButtons>
            </Bottom>
         </Form>
      </div>
   )
}

export default FormAddTweet

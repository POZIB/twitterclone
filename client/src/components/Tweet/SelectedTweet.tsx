import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import {
   Avatar,
   Typography,
   styled,
   IconButton,
   Link,
   Box,
   MenuItem,
   Menu,
   ListItemIcon,
   ListItemText,
   LinearProgress,
   Skeleton,
} from '@mui/material'
import {
   ChatBubbleOutline,
   FavoriteBorderOutlined,
   MoreHorizOutlined,
   PresentToAllOutlined,
   RepeatOutlined,
   DeleteOutline,
} from '@mui/icons-material'

import {
   fetchDeleteTweet,
   setIsSumbitDeleteTweet,
} from '../../store/tweets/actionCreators'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import tweetsApi from '../../api/TweetsApi'
import ITweet from '../../models/ITweet'
import FormatDateString from '../../utils/FormatDateString'
import SkeletonTweet from './SkeletonTweet'
import ContainerImages from './ContainerImages'

const Wrapper = styled('div')(({ theme }) => ({
   padding: '0 16px',
   borderBottom: '1px solid #e4e4e4',
}))

const AvatarWrapper = styled(Link)(({ theme }) => ({
   marginRight: '12px',
}))

const Top = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'flex-start',
   marginBottom: '4px',
   marginTop: '6px',
}))

const BlockNames = styled('div')(({ theme }) => ({}))

const Body = styled('div')(({ theme }) => ({
   marginTop: '12px',
}))

const Content = styled('div')(() => ({}))

const ContainerText = styled('div')(() => ({}))

const WrapperAttachments = styled('div')(() => ({
   marginTop: '12px',
   display: 'flex',
   justifyContent: 'center',
}))

const ContainerAttachments = styled('div')(() => ({
   maxHeight: '500px',
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const ListImages = styled('div')(() => ({
   display: 'flex',
   flex: '1',
   borderRadius: '16px',
   border: '1px solid rgb(207, 217, 222)',
   overflow: 'hidden',
   boxSizing: 'border-box',
   margin: '-1px',
}))

const ItemImage = styled('div')(() => ({
   display: 'flex',
   flex: '1',
   minHeight: '0',
   '& img': {
      height: '100%',
      width: '100%',
      objectFit: 'cover',
   },
}))

const BlockDateInfo = styled('div')(({ theme }) => ({
   marginTop: '12px',
   marginBottom: '12px',
   display: 'flex',
}))

const NavigationContainer = styled('div')(() => ({
   marginTop: '12px',
   borderTop: '1px solid #e4e4e4',
}))

const NavigationList = styled('ul')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'space-around',
   padding: 0,
   margin: 0,
}))

const NavigationItem = styled('li')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
}))

interface IProps {
   id?: number
}

const SelectedTweet: React.FC<IProps> = ({ id }): React.ReactElement => {
   const params = useParams()
   const navigate = useNavigate()
   const dispatch = useDispatch()
   const { user } = useTypedSelector(state => state.auth)
   const { loadingStatusDeleteTweet, isSubmitDeleteTweet } = useTypedSelector(
      state => state.tweets
   )
   const [tweet, setTweet] = React.useState<ITweet | null>(null)
   const [loadingTweet, setLoadingTweet] = React.useState(true)

   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
   const open = Boolean(anchorEl)

   React.useEffect(() => {
      const _id = id || params.id
      if (_id) {
         tweetsApi.fetchOneTweet(Number(_id)).then(response => {
            setTweet(response.data)
            setLoadingTweet(false)
         })
      }
   }, [id])

   React.useEffect(() => {
      if (isSubmitDeleteTweet) {
         navigate(-1)
         dispatch(setIsSumbitDeleteTweet(false))
      }
   }, [isSubmitDeleteTweet])

   const handleClickOptions = (event: React.MouseEvent<HTMLElement>) => {
      setAnchorEl(event.currentTarget)
   }

   const handleClose = () => {
      setAnchorEl(null)
   }

   const handleDelete = () => {
      if (tweet?.id) {
         setAnchorEl(null)
         dispatch(fetchDeleteTweet(tweet.id))
      }
   }

   if (loadingTweet && !tweet) {
      return <SkeletonTweet />
   }

   if (!tweet) {
      return (
         <Wrapper>
            <Top>
               <AvatarWrapper>
                  <Avatar sx={{ width: '48px', height: '48px' }} />
               </AvatarWrapper>
               <Box display='flex' justifyContent='space-between' width='100%'>
                  <div>
                     <Skeleton animation={false} variant='text' width='150px' />
                     <Skeleton animation={false} variant='text' width='100px' />
                  </div>
               </Box>
            </Top>
            <Body>
               <div>
                  <Skeleton animation={false} variant='text' width='100%' />
                  <Skeleton animation={false} variant='text' width='100%' />
                  <Skeleton animation={false} variant='text' width='100%' />
               </div>
            </Body>
            <Box
               display='flex'
               flexDirection='column'
               alignItems='center'
               justifyContent='center'
               margin='30px auto'
            >
               <div>
                  <Typography fontSize='30px' fontWeight='400' variant='h2' mb='20px'>
                     Хм... этой страницы не существует.
                  </Typography>
                  <Typography variant='h6' fontWeight='300'>
                     Попробуйте поискать что-нибудь другое.
                  </Typography>
               </div>
            </Box>
         </Wrapper>
      )
   }

   return (
      <div>
         <Wrapper>
            <Top>
               <AvatarWrapper>
                  <Avatar
                     alt={tweet.user?.fullName}
                     src={tweet.user?.avatar}
                     sx={{ width: '48px', height: '48px' }}
                  />
               </AvatarWrapper>
               <Box display='flex' justifyContent='space-between' flex='1'>
                  <BlockNames onClick={() => navigate(`../../user/${tweet.user?.id}`)}>
                     <Link color='black'>
                        <Typography fontSize='15px' lineHeight='20px' fontWeight={700}>
                           {tweet.user?.fullName}
                        </Typography>
                     </Link>
                     <Typography variant='caption' fontSize='15px' lineHeight='20px'>
                        @{tweet.user?.userName}
                     </Typography>
                  </BlockNames>
                  <div>
                     <IconButton onClick={handleClickOptions} sx={{ p: 0 }}>
                        <MoreHorizOutlined />
                     </IconButton>
                  </div>
               </Box>
            </Top>
            <Body>
               <Content>
                  <ContainerText>
                     <Typography
                        fontSize='18px'
                        lineHeight='28px'
                        fontWeight={400}
                        whiteSpace='pre-line'
                     >
                        {tweet?.text}
                     </Typography>
                  </ContainerText>

                  {!!tweet.images?.length && (
                     <WrapperAttachments>
                        <ContainerAttachments>
                           <ContainerImages tweet={tweet} />
                        </ContainerAttachments>
                     </WrapperAttachments>
                  )}
               </Content>
            </Body>

            <BlockDateInfo>
               <Typography variant='caption'>
                  {FormatDateString.time(tweet?.date)}
               </Typography>
               <Typography p='0 4px' variant='caption'>
                  &#183;
               </Typography>
               <Typography variant='caption'>
                  {FormatDateString.date(tweet?.date)}
               </Typography>
               <Typography p='0 4px' variant='caption'>
                  &#183;
               </Typography>
               <Typography variant='caption'>Twitter</Typography>
            </BlockDateInfo>

            <NavigationContainer>
               <NavigationList>
                  <NavigationItem>
                     <IconButton>
                        <ChatBubbleOutline fontSize='medium' />
                     </IconButton>
                     <Typography fontSize='14px'>{tweet?.replies}</Typography>
                  </NavigationItem>
                  <NavigationItem>
                     <IconButton>
                        <RepeatOutlined fontSize='medium' />
                     </IconButton>
                     <Typography fontSize='14px'>{tweet?.replies}</Typography>
                  </NavigationItem>
                  <NavigationItem>
                     <IconButton>
                        <FavoriteBorderOutlined fontSize='medium' />
                     </IconButton>
                     <Typography fontSize='14px'>{tweet?.likes}</Typography>
                  </NavigationItem>
                  <NavigationItem>
                     <IconButton>
                        <PresentToAllOutlined fontSize='medium' />
                     </IconButton>
                     <Typography fontSize='14px'>{tweet?.retweets}</Typography>
                  </NavigationItem>
               </NavigationList>
            </NavigationContainer>

            <div>
               <Menu
                  MenuListProps={{
                     'aria-labelledby': 'long-button',
                  }}
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  PaperProps={{
                     style: {
                        width: '20ch',
                     },
                  }}
                  anchorOrigin={{
                     vertical: 'bottom',
                     horizontal: 'right',
                  }}
                  transformOrigin={{
                     vertical: 'top',
                     horizontal: 'right',
                  }}
               >
                  {user?.id === tweet.userId && (
                     <MenuItem onClick={handleDelete}>
                        <ListItemIcon sx={{ color: 'rgb(244, 33, 46)' }}>
                           <DeleteOutline fontSize='medium' />
                        </ListItemIcon>
                        <ListItemText
                           primaryTypographyProps={{ color: 'rgb(244, 33, 46)' }}
                        >
                           Удалить
                        </ListItemText>
                     </MenuItem>
                  )}
               </Menu>
            </div>
         </Wrapper>
         {loadingStatusDeleteTweet === 'LOADING' && <LinearProgress />}
      </div>
   )
}

export default SelectedTweet

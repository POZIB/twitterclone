import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import {
   Avatar,
   Typography,
   styled,
   IconButton,
   Menu,
   MenuItem,
   Link,
   Box,
   ListItemIcon,
   ListItemText,
   LinearProgress,
} from '@mui/material'
import {
   ChatBubbleOutline,
   FavoriteBorderOutlined,
   PresentToAllOutlined,
   RepeatOutlined,
   MoreHorizOutlined,
   DeleteOutline,
} from '@mui/icons-material'

import ITweet from '../../models/ITweet'
import FormatDateString from '../../utils/FormatDateString'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { fetchDeleteTweet } from '../../store/tweets/actionCreators'
import ContainerImages from './ContainerImages'

const Wrapper = styled('div')(({ theme }) => ({
   display: 'flex',
   padding: '6px 16px',
   cursor: 'pointer',
   borderBottom: '1px solid #e4e4e4',
   transitionProperty: 'background-color,box-shadow',
   transitionDuration: '0.22s',
   '&: hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.03)',
   },
}))

const AvatarWrapper = styled(Link)(({ theme }) => ({
   width: theme.spacing(6.5),
   height: theme.spacing(6.5),
   marginRight: '10px',
}))

const Container = styled('div')(() => ({
   width: '100%',
}))

const Body = styled('div')(() => ({
   paddingBottom: '12px',
}))

const Content = styled('div')(() => ({}))

const ContainerText = styled('div')(() => ({}))

const WrapperAttachments = styled('div')(() => ({
   marginTop: '12px',
   display: 'flex',
}))

const ContainerAttachments = styled('div')(() => ({
   alignItems: 'flex-start',
   maxHeight: '350px',
   display: 'flex',
   flexDirection: 'column',
}))

const NavigationContainer = styled('div')(() => ({
   marginTop: '12px',
}))

const NavigationList = styled('ul')(({ theme }) => ({
   maxWidth: '425px',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'space-between',
   padding: 0,
   margin: 0,
}))

const NavigationItem = styled('li')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   padding: 0,
   margin: 0,
   '& button': {
      padding: 0,
   },
}))

const ContainerProgress = styled('div')(({ theme }) => ({
   padding: '2px',
}))

interface IProps {
   item: ITweet
}

const Twit: React.FC<IProps> = ({ item }): React.ReactElement => {
   const navigate = useNavigate()
   const dispatch = useDispatch()
   const { user } = useTypedSelector(state => state.auth)
   const [loading, setLoading] = React.useState(false)
   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
   const open = Boolean(anchorEl)

   const handleClickOptions = (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation()
      setAnchorEl(event.currentTarget)
   }

   const handleClose = () => {
      setAnchorEl(null)
   }

   const handleSelectTweet = (event: React.MouseEvent<HTMLElement>) => {
      if (open) return
      event.stopPropagation()
      navigate(`/home/tweet/${item.id}`)
   }

   const handleDelete = () => {
      if (item.id) {
         setAnchorEl(null)
         setLoading(true)
         dispatch(fetchDeleteTweet(item.id))
      }
   }

   const handlClickLinkOnUser = (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation()
      if (open) return
      navigate(`/user/${item?.user?.userName}`)
   }

   return (
      <div>
         <Wrapper aria-label='wrapper-tweet' onClick={handleSelectTweet}>
            <AvatarWrapper onClick={handlClickLinkOnUser}>
               <Avatar alt={item.user?.fullName} src={item.user?.avatar} />
            </AvatarWrapper>
            <Container>
               <Box display='flex' justifyContent='space-between' alignItems='center'>
                  <Box display='flex'>
                     <Link color='black' onClick={handlClickLinkOnUser}>
                        <Typography
                           fontWeight={700}
                           fontSize='15px'
                           lineHeight='20px'
                           mr='4px'
                        >
                           {item.user?.fullName}
                        </Typography>
                     </Link>
                     <Typography
                        fontWeight={400}
                        fontSize='15px'
                        color='caption'
                        lineHeight='20px'
                     >
                        @{item.user?.userName}
                     </Typography>
                     <Typography p='0 4px'>&#183;</Typography>
                     <Typography
                        fontWeight={400}
                        fontSize='15px'
                        color='caption'
                        lineHeight='20px'
                     >
                        {FormatDateString.dateDistance(item.date)}
                     </Typography>
                  </Box>
                  <div>
                     <IconButton onClick={handleClickOptions} sx={{ p: '1px' }}>
                        <MoreHorizOutlined />
                     </IconButton>
                  </div>
               </Box>

               <Body>
                  <Content>
                     <ContainerText>
                        <Typography
                           variant='body2'
                           fontSize='15px'
                           lineHeight='20px'
                           fontWeight={400}
                           whiteSpace='pre-line'
                        >
                           {item.text}
                        </Typography>
                     </ContainerText>
                     {!!item.images?.length && (
                        <WrapperAttachments>
                           <ContainerAttachments>
                              <ContainerImages tweet={item} />
                           </ContainerAttachments>
                        </WrapperAttachments>
                     )}
                  </Content>
                  <NavigationContainer>
                     <NavigationList>
                        <NavigationItem>
                           <IconButton>
                              <ChatBubbleOutline fontSize='small' />
                           </IconButton>
                           <Typography fontSize='14px'>{item.replies}</Typography>
                        </NavigationItem>
                        <NavigationItem>
                           <IconButton>
                              <RepeatOutlined fontSize='small' />
                           </IconButton>
                           <Typography fontSize='14px'>{item.replies}</Typography>
                        </NavigationItem>
                        <NavigationItem>
                           <IconButton>
                              <FavoriteBorderOutlined fontSize='small' />
                           </IconButton>
                           <Typography fontSize='14px'>{item.likes}</Typography>
                        </NavigationItem>
                        <NavigationItem>
                           <IconButton>
                              <PresentToAllOutlined fontSize='small' />
                           </IconButton>
                           <Typography fontSize='14px'>{item.retweets}</Typography>
                        </NavigationItem>
                     </NavigationList>
                  </NavigationContainer>
               </Body>
               <Box zIndex={3}>
                  <Menu
                     MenuListProps={{
                        'aria-labelledby': 'long-button',
                     }}
                     anchorEl={anchorEl}
                     open={open}
                     onClose={handleClose}
                     onKeyDown={() => false}
                     PaperProps={{
                        style: {
                           width: '20ch',
                        },
                     }}
                     anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                     }}
                     transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                     }}
                  >
                     {user?.id === item.userId && (
                        <MenuItem onClick={handleDelete}>
                           <ListItemIcon sx={{ color: 'rgb(244, 33, 46)' }}>
                              <DeleteOutline fontSize='medium' />
                           </ListItemIcon>
                           <ListItemText
                              primaryTypographyProps={{ color: 'rgb(244, 33, 46)' }}
                           >
                              Удалить
                           </ListItemText>
                        </MenuItem>
                     )}
                  </Menu>
               </Box>
            </Container>
         </Wrapper>
         {loading ? <LinearProgress /> : <ContainerProgress />}
      </div>
   )
}

export default Twit

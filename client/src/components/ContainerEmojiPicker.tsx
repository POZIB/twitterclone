import React from 'react'
import { Menu, styled, Box, PopoverOrigin } from '@mui/material'
import { BaseEmoji } from 'emoji-mart'

import EmojiPicker from './UI/EmojiPicker'

const EmojiPickerContainer = styled(Box)(({ theme }) => ({
   width: '268px',
   overflow: 'hidden',
   borderRadius: '10px',
   '& em-emoji-picker': {
      height: '38vh',
   },
}))

interface IProps {
   anchorEl: null | HTMLElement
   open: boolean
   onClose: () => void
   onEmojiSelect: (event: BaseEmoji) => void
   transformOrigin?: PopoverOrigin | undefined
   anchorOrigin?: PopoverOrigin | undefined
}

const ContainerEmojiPicker: React.FC<IProps> = ({
   anchorEl,
   open,
   onClose,
   onEmojiSelect,
   transformOrigin,
   anchorOrigin,
}) => {
   return (
      <Menu
         anchorEl={anchorEl}
         open={open}
         onClose={onClose}
         anchorOrigin={anchorOrigin}
         transformOrigin={transformOrigin}
      >
         <EmojiPickerContainer>
            <EmojiPicker
               locale={'ru'}
               maxFrequentRows={1}
               perLine={8}
               previewPosition={'none'}
               searchPosition={'none'}
               emojiButtonSize={30}
               emojiSize={20}
               onEmojiSelect={onEmojiSelect}
            />
         </EmojiPickerContainer>
      </Menu>
   )
}

export default ContainerEmojiPicker

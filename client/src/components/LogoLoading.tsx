import React from 'react'
import { IconButton, styled } from '@mui/material'
import { Twitter } from '@mui/icons-material'

const Wrapper = styled('div')(() => ({
   height: '100%',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
}))

const LogoLoading: React.FC = (): React.ReactElement => {
   return (
      <Wrapper>
         <Twitter sx={{ width: '50vw', height: '50vh' }} color='primary' />
      </Wrapper>
   )
}

export default LogoLoading

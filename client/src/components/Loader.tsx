import React from 'react'
import { Box, CircularProgress } from '@mui/material'

interface IProps {
   flexGrow?: number
   size?: number
   padding?: string
   margin?: string
}

const Loader: React.FC<IProps> = ({ flexGrow, size, padding, margin }) => {
   return (
      <Box
         display={'flex'}
         flexDirection={'column'}
         alignItems='center'
         justifyContent='center'
         flexGrow={flexGrow}
         padding={padding}
         margin={margin}
      >
         <CircularProgress size={size || 50} />
      </Box>
   )
}

export default Loader

import React from 'react'
import { IconButton, styled, Box } from '@mui/material'
import {
   MicNoneOutlined,
   SendOutlined,
   StopCircleTwoTone,
   PlayCircleOutline,
   PauseCircleOutline,
   DeleteForeverOutlined,
} from '@mui/icons-material'

import socket from '../../socket'

import { ReactComponent as WaveSound } from '../../assets/wave.svg'
import IFile from '../../models/IFile'
import UploadFileApi from '../../api/UploadFileApi'
import FormatTimeString from '../../utils/FormatTimeString'
import { useTypedSelector } from '../../hooks/useTypedSelector'

const Body = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'space-between',
   flexWrap: 'nowrap',
}))

const IconContainer = styled(Box)(({ theme }) => ({
   marginLeft: '4px',
}))

const IconIndicationRecording = styled('div')(() => ({
   height: '10px',
   width: '10px',
   display: 'inline-block',
   backgroundColor: '#f56b6b',
   borderRadius: '50%',
   boxShadow: '0 0 7px #f56b6b',
   animation: 'opacity .6s ease-in-out infinite',
   animationDirection: 'alternate',
   opacity: '1',
   '@keyframes opacity': {
      '100% ': { opacity: '0.2', boxShadow: '0 0 3px #f56b6b' },
   },
}))

const ContainerRecording = styled('div')(() => ({
   display: 'flex',
   alignItems: 'center',
   flex: '1',
   padding: '0 12px',
   borderRadius: '30px',
   border: '1px solid #e4e4e4',
}))

const ContainerRecordingWave = styled('div')(() => ({
   maxHeight: '39px',
   display: 'flex',
   flex: '1',
   position: 'relative',
   transition: '0.5s linear',
   '& svg': {
      maxHeight: '100%',
      width: '100%',
   },
}))

const ContainerCurrentRecord = styled('div')(() => ({
   maxHeight: '39px',
   display: 'flex',
   flex: '1',
   position: 'relative',
   '& svg': {
      width: '100%',
      maxHeight: '100%',
   },
}))

const WaveMask = styled('div')(({ theme }) => ({
   height: '100%',
   width: '100%',
   display: 'flex',
   position: 'absolute',
   flex: '1',
   top: '0',
   left: '0',
   transition: '0.2s linear',
}))

interface IProps {
   isRecording: boolean
   setIsRecording: (value: React.SetStateAction<boolean>) => void
   setLoading: (value: React.SetStateAction<boolean>) => void
}

const FormRecordingVoiceMessage: React.FC<IProps> = ({
   isRecording,
   setIsRecording,
   setLoading,
}) => {
   const { selectDialog, newDialog } = useTypedSelector(state => state.dialogs)

   const [voiceMessage, setVoiceMessage] = React.useState<IFile | undefined>(undefined)
   const [mediaRecorder, setMediaRecorder] = React.useState<MediaRecorder | null>(null)
   const [progressRecording, setProgressRecording] = React.useState(100)
   const [statusRecording, setStatusRecording] = React.useState<
      'RECORD' | 'STOP' | 'SEND' | 'ERROR' | undefined
   >(undefined)

   const voiceRef = React.useRef<HTMLAudioElement | null>(null)
   const [isPlaying, setIsPlaying] = React.useState(false)
   const [progressPlaying, setProgressPlaying] = React.useState(100)
   const [duration, setDuration] = React.useState(0)
   const [idTimout, setIdTimout] = React.useState<NodeJS.Timeout | undefined>(undefined)

   const MAX_DURATION_RECORD = 60 - 1

   React.useEffect(() => {
      setStatusRecording('RECORD')
      onStartRecording()
   }, [isRecording])

   React.useEffect(() => {
      if ((voiceMessage && statusRecording === 'SEND') || statusRecording === 'ERROR') {
         if (voiceMessage?.url.startsWith('blob')) {
            setLoading(true)
            onUploadRecord()
         } else {
            onSubmit()
            setLoading(false)
            handleCloseForm()
         }
      }
   }, [voiceMessage, statusRecording])

   const onSubmit = () => {
      if (selectDialog === newDialog) {
         socket.emit('DIALOGS:NEW', {
            text: '',
            images: [],
            voice: voiceMessage?.url,
            newDialog,
         })
      } else {
         socket.emit('MESSAGES:SEND', {
            text: '',
            images: [],
            voice: voiceMessage?.url,
            dialogId: selectDialog?.id,
         })
      }
   }

   React.useEffect(() => {
      if (!voiceRef || !voiceRef.current) return
      const voiceCurrent = voiceRef.current
      voiceCurrent.addEventListener('playing', () => setIsPlaying(true))
      voiceCurrent.addEventListener('pause', () => setIsPlaying(false))
      voiceCurrent.addEventListener('ended', () => onEndPlaying(voiceCurrent))
      voiceCurrent.addEventListener('loadedmetadata', () => loadedMetaData(voiceCurrent))

      return () => {
         voiceCurrent.removeEventListener('playing', () => setIsPlaying(true))
         voiceCurrent.removeEventListener('pause', () => setIsPlaying(false))
         voiceCurrent.removeEventListener('ended', () => onEndPlaying(voiceCurrent))
         voiceCurrent.removeEventListener('loadedmetadata', () =>
            loadedMetaData(voiceCurrent)
         )
         voiceRef.current = null
      }
   }, [voiceRef, voiceMessage?.url])

   const togglePlay = () => {
      if (!isPlaying && voiceRef) {
         voiceRef.current?.play()
      } else {
         voiceRef.current?.pause()
      }
   }

   const onEndPlaying = (voiceCurrent: HTMLAudioElement) => {
      setProgressPlaying(100)
      setDuration(voiceCurrent.duration)
      setIsPlaying(false)
   }

   const loadedMetaData = (voiceCurrent: HTMLAudioElement) => {
      if (voiceCurrent.duration == Infinity) {
         voiceCurrent.currentTime = 1e101
         voiceCurrent.ontimeupdate = function () {
            this.ontimeupdate = () => {
               const duration = voiceCurrent.duration
               const current = voiceCurrent.currentTime
               setDuration(duration - current)
               setProgressPlaying((1 - current / duration) * 100)
               return
            }
            voiceCurrent.currentTime = 0
            setDuration(voiceCurrent.duration)
            return
         }
      }
   }

   React.useEffect(() => {
      if (!mediaRecorder) return
      if (duration < MAX_DURATION_RECORD && statusRecording === 'RECORD') {
         if (selectDialog) {
            socket.emit('DIALOGS:RECORDING_VOICE', selectDialog, true)
         }
         setProgressRecording((1 - duration / MAX_DURATION_RECORD) * 100)
         clearTimeout(idTimout)
         const id = setTimeout(() => setDuration(prev => prev + 1), 1000)
         setIdTimout(id)
      } else if (statusRecording === 'RECORD') {
         onStopRecording()
         clearTimeout(idTimout)
      } else if (statusRecording === 'STOP') {
         clearTimeout(idTimout)
      }
      if (!statusRecording) {
         setDuration(0)
         clearTimeout(idTimout)
      }
   }, [duration, statusRecording, mediaRecorder])

   const onStartRecording = async () => {
      if (navigator.mediaDevices.getUserMedia) {
         try {
            const stream = await navigator.mediaDevices.getUserMedia({ audio: true })
            const mediaRecorder = new MediaRecorder(stream)
            setMediaRecorder(mediaRecorder)
            mediaRecorder.start()

            mediaRecorder.onstart = () => {}

            mediaRecorder.onstop = () => {
               if (selectDialog) {
                  socket.emit('DIALOGS:RECORDING_VOICE', selectDialog, false)
               }
               stream.getTracks().forEach(function (track) {
                  track.stop()
               })
            }

            mediaRecorder.ondataavailable = e => {
               const recordUrl = URL.createObjectURL(e.data)
               const recordToFile = new File([e.data], 'audio.webm', {
                  type: 'audio/ogg',
               })
               setVoiceMessage({ url: recordUrl, file: recordToFile } as IFile)
            }
         } catch (err) {
            alert('Возникла проблема с микрофоном. Проверьте разрешение микрофона')
            handleCloseForm()
         }
      }
   }

   const onStopRecording = () => {
      if (statusRecording === 'RECORD') {
         setStatusRecording('STOP')
         mediaRecorder?.stop()
      }
   }

   const handleCloseForm = () => {
      onStopRecording()
      setIsRecording(false)
   }

   const onUploadRecord = async () => {
      if (voiceMessage?.file) {
         try {
            const res = await UploadFileApi.upload(voiceMessage.file)
            setVoiceMessage({ ...voiceMessage, url: res.data.url })
         } catch (error) {
            setStatusRecording('ERROR')
            console.log('ERROR')
         }
      }
   }

   const handleSend = () => {
      onStopRecording()
      setStatusRecording('SEND')
   }

   return (
      <Body>
         <IconContainer mr='4px'>
            <IconButton color='primary' onClick={handleCloseForm}>
               <DeleteForeverOutlined />
            </IconButton>
         </IconContainer>

         <ContainerRecording>
            {statusRecording === 'RECORD' && (
               <IconButton
                  color='error'
                  onClick={onStopRecording}
                  sx={{
                     p: '2px',
                     '&:hover': {
                        color: '#ff0000',
                        backgroundColor: 'rgb(255 0 0 / 10%)',
                     },
                  }}
               >
                  <StopCircleTwoTone />
               </IconButton>
            )}
            {!isPlaying && statusRecording !== 'RECORD' && (
               <IconButton
                  color='success'
                  onClick={togglePlay}
                  sx={{
                     p: '2px',
                     '&:hover': {
                        color: '#62bf67',
                        backgroundColor: 'rgb(98 191 103 / 10%)',
                     },
                  }}
               >
                  <PlayCircleOutline />
               </IconButton>
            )}
            {isPlaying && (
               <IconButton
                  color='default'
                  onClick={togglePlay}
                  sx={{
                     p: '2px',
                     '&:hover': {
                        color: '#3737378a',
                        backgroundColor: 'rgb(55 55 55 / 10%)',
                     },
                  }}
               >
                  <PauseCircleOutline />
               </IconButton>
            )}
            {voiceMessage ? (
               <ContainerCurrentRecord>
                  <audio ref={voiceRef} src={voiceMessage.url} />
                  <WaveSound color='#0080fd4d' preserveAspectRatio='none' />
                  <WaveMask sx={{ clipPath: `inset(0px ${progressPlaying}% 0px 0px)` }}>
                     <WaveSound color='#2b8eef' preserveAspectRatio='none' />
                  </WaveMask>
               </ContainerCurrentRecord>
            ) : (
               <ContainerRecordingWave
                  sx={{ clipPath: `inset(0px ${progressRecording}% 0px 0px)` }}
               >
                  <WaveSound color='rgb(29, 155, 240)' preserveAspectRatio='none' />
               </ContainerRecordingWave>
            )}
            <div>
               {FormatTimeString.min(duration)}
               {statusRecording === 'RECORD' && (
                  <IconIndicationRecording sx={{ ml: '4px' }} />
               )}
            </div>
         </ContainerRecording>

         <IconContainer>
            <IconButton color='primary' onClick={handleSend}>
               <SendOutlined />
            </IconButton>
         </IconContainer>
      </Body>
   )
}

export default FormRecordingVoiceMessage

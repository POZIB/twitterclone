import { DeleteOutline, MoreHorizOutlined } from '@mui/icons-material'
import {
   Avatar,
   Grid,
   IconButton,
   Link,
   ListItemIcon,
   ListItemText,
   Menu,
   MenuItem,
   styled,
   Typography,
} from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import IDialog from '../../models/IDialog'
import FormatDateString from '../../utils/FormatDateString'

const Wrapper = styled('div')(({ theme }) => ({
   height: '80px',
   display: 'flex',
   alignItems: 'center',
   transitionProperty: 'background-color , box-shadow',
   transitionDuration: '0.2s',
   boxSizing: 'border-box',
   padding: '12px',
   transition: 'background-color 0.15s',
   '& .iconButton': {
      opacity: '0',
      width: '0',
      display: 'inline-flex',
      transition: 'width 0.15s, opacity 0.15s',
   },
   '&:hover .iconButton': {
      width: 'auto',
      opacity: '1',
   },
   '&:hover': {
      cursor: 'pointer',
      backgroundColor: 'rgb(247, 249, 249)',
   },
}))

const Body = styled('div')(({ theme }) => ({
   width: '100%',
   maxWidth: '100%',
   display: 'flex',
   alignItems: 'flex-start',
   boxSizing: 'border-box',
}))

const WrapperAvatar = styled('div')(({ theme }) => ({
   marginRight: '12px',
   position: 'relative',
}))

const IsOnline = styled('div')(({ theme }) => ({
   height: '16px',
   width: '16px',
   backgroundColor: '#649b61',
   position: 'absolute',
   bottom: '2px',
   right: '3px',
   borderRadius: '50%',
   border: '2px solid rgb(247, 249, 249)',
}))

const Content = styled('div')(({ theme }) => ({
   maxWidth: '100%',
   minWidth: '0',
   display: 'flex',
   flex: '1',
}))

const BoxTextOverflow = styled('div')(({ theme }) => ({
   display: 'flex',
   overflowWrap: 'break-word',
   overflow: 'hidden',
   whiteSpace: 'nowrap',
   marginRight: '4px',
   '& span': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflowWrap: 'break-word',
   },
   '& a': {
      minWidth: 0,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflowWrap: 'break-word',
   },
}))

const BoxText = styled('div')(({ theme }) => ({
   marginRight: '4px',
   '& span': {},
}))

const BoxCountUnreadMessages = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   marginLeft: 'auto',
   padding: '0 4px',
   '& div': {
      width: '20px',
      height: '20px',
      backgroundColor: theme.palette.primary.main,
      borderRadius: '50%',
      border: '1px solid #fff',
      color: '#fff',
      fontSize: '10px',
      lineHeight: '10px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
   },
}))

interface IProps {
   item?: IDialog
   isTyping?: boolean
   isRecording?: boolean
}

const DialogItem: React.FC<IProps> = ({ item }): React.ReactElement => {
   const navigate = useNavigate()
   const { selectDialog } = useTypedSelector(state => state.dialogs)
   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
   const open = Boolean(anchorEl)

   const handleClickOptions = (event: React.MouseEvent<HTMLElement>) => {
      // event.stopPropagation()
      // event.preventDefault()
      // setAnchorEl(event.currentTarget)
      // return
   }

   const handlClickLinkOnUser = (
      e: React.MouseEvent<HTMLDivElement> | React.MouseEvent<HTMLSpanElement>
   ) => {
      e.stopPropagation()
      navigate(`/user/${item?.companion.userName}`)
   }

   const handleDeleteClick = () => {
      setAnchorEl(null)
   }

   if (!item) {
      return <></>
   }

   return (
      <Wrapper
         sx={{
            backgroundColor: item.id === selectDialog?.id ? 'rgb(247, 249, 249)' : '',
            borderRight: item.id === selectDialog?.id ? `2px solid ` : 'none',
            borderColor: theme => theme.palette.primary.main,
         }}
      >
         <Body>
            <WrapperAvatar onClick={handlClickLinkOnUser}>
               <Avatar
                  src={item.companion.avatar}
                  sx={{ width: '48px', height: '48px' }}
               />
               {item?.companion.lastSeen?.toLocaleLowerCase() === 'online' && (
                  <IsOnline />
               )}
            </WrapperAvatar>
            <Content>
               <Grid container minWidth={0} direction='column' wrap='nowrap'>
                  <Grid item display='flex'>
                     <BoxTextOverflow sx={{ minWidth: '45px' }}>
                        <Link
                           color='black'
                           fontWeight={item.lastMessage?.isRead ? 500 : 700}
                           onClick={handlClickLinkOnUser}
                        >
                           <span>{item.companion.fullName}</span>
                        </Link>
                     </BoxTextOverflow>
                     <BoxTextOverflow>
                        <Typography variant='caption'>
                           @{item.companion.userName}
                        </Typography>
                     </BoxTextOverflow>
                     <Typography p='0 4px' variant='caption'>
                        &#183;
                     </Typography>

                     <BoxText>
                        <Typography variant='caption' noWrap>
                           {FormatDateString.dateReceivedSMS(item.lastMessage?.createdAt)}
                        </Typography>
                     </BoxText>
                     {item.countUnreadMessages > 0 && (
                        <BoxCountUnreadMessages>
                           <div>{item.countUnreadMessages}</div>
                        </BoxCountUnreadMessages>
                     )}
                  </Grid>
                  <Grid item display='flex' minWidth={0}>
                     <BoxTextOverflow>
                        {!item?.isRecording && !item?.isTyping && (
                           <Typography
                              variant='body1'
                              component='span'
                              fontWeight={item.lastMessage?.isRead ? 400 : 500}
                           >
                              {item.lastMessage?.text}
                           </Typography>
                        )}
                        {item?.isRecording && (
                           <Typography variant='body1' component='span' fontWeight={500}>
                              Записывает голосовое сообщение...
                           </Typography>
                        )}
                        {item?.isTyping && (
                           <Typography variant='body1' component='span' fontWeight={500}>
                              Печатает...
                           </Typography>
                        )}
                     </BoxTextOverflow>
                  </Grid>
               </Grid>
            </Content>
            <div>
               <IconButton
                  className='iconButton'
                  onClick={handleClickOptions}
                  sx={{ p: 0, ml: 'auto' }}
               >
                  <MoreHorizOutlined />
               </IconButton>
               <div>
                  <Menu
                     anchorEl={anchorEl}
                     open={open}
                     onClose={() => setAnchorEl(null)}
                     anchorOrigin={{ vertical: 'center', horizontal: 'center' }}
                     transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                     }}
                  >
                     <MenuItem onClick={handleDeleteClick}>
                        <ListItemIcon sx={{ color: 'rgb(244, 33, 46)' }}>
                           <DeleteOutline fontSize='medium' />
                        </ListItemIcon>
                        <ListItemText
                           primaryTypographyProps={{ color: 'rgb(244, 33, 46)' }}
                        >
                           Удалить
                        </ListItemText>
                     </MenuItem>
                  </Menu>
               </div>
            </div>
         </Body>
      </Wrapper>
   )
}

export default DialogItem

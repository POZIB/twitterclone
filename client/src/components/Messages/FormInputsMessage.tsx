import React from 'react'
import { useDispatch } from 'react-redux'
import { IconButton, OutlinedInput, styled, LinearProgress } from '@mui/material'
import {
   MicNoneOutlined,
   SendOutlined,
   SentimentSatisfiedAltOutlined,
} from '@mui/icons-material'
import { Box } from '@mui/system'

import { BaseEmoji } from 'emoji-mart'
import socket from '../../socket'

import { useTypedSelector } from '../../hooks/useTypedSelector'
import {
   setIsSubmitSendMessage,
   setMutableMessage,
} from '../../store/messages/actionCreators'

import ContainerEmojiPicker from '../ContainerEmojiPicker'
import UploadImages from '../Upload/InputUploadImages'
import IImage from '../../models/IImage'
import ContainerImagesUpload from '../Upload/ContainerImagesUpload'
import FormRecordingVoiceMessage from './FormRecordingVoiceMessage'
import useDelaySend from '../../hooks/useDelaySend'

const Wrapper = styled('aside')(({ theme }) => ({
   borderTop: '1px solid #e4e4e4',
   padding: '4px',
}))

const Body = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'space-between',
   flexWrap: 'nowrap',
}))

const InputContainer = styled('div')(({ theme }) => ({
   flex: 1,
   border: '1px solid',
   borderColor: 'rgb(207, 217, 222)',
   borderRadius: '20px',
   '&:focus-within': {
      borderColor: theme.palette.primary.main,
      borderWidth: '2px',
   },
   '&:focus-within > div': {
      border: 'none',
   },
}))

const InputContainerBorder = styled('div')(({ theme }) => ({
   border: '1px solid transparent',
}))

const InputTextContainer = styled('div')(({ theme }) => ({
   padding: '4px 14px',
}))

const InputText = styled(OutlinedInput)(({ theme }) => ({
   width: '100%',
   borderRadius: '30px',
   padding: '2px',
   '&:hover  .MuiOutlinedInput-notchedOutline': {
      borderColor: 'rgba(29, 155, 240, 0.15)',
   },
   '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: 'rgba(29, 155, 240, 1)',
   },
}))

const IconContainer = styled(Box)(({ theme }) => ({
   marginLeft: '4px',
}))

const AttachemntsContainer = styled('div')(() => ({
   display: 'flex',
   padding: '10px',
}))

const FormInputsMessage: React.FC = () => {
   const dispatch = useDispatch()
   const { user } = useTypedSelector(state => state.auth)
   const { selectDialog, newDialog } = useTypedSelector(state => state.dialogs)
   const { messages, isSubmitSendMessage, mutableMessage } = useTypedSelector(
      state => state.messages
   )

   const [loading, setLoading] = React.useState(false)
   const [text, setText] = React.useState<string>('')
   const [images, setImages] = React.useState<IImage[]>([])
   const [isDontReadyAttachments, setIsDontReadyAttachments] = React.useState(false)
   const [isRecordingVoice, setIsRecordingVoice] = React.useState(false)

   const [anchorElEmoji, setAnchorElEmoji] = React.useState<null | HTMLElement>(null)
   const openEmoji = Boolean(anchorElEmoji)

   const textDelay = useDelaySend(text, 1000)

   React.useEffect(() => {
      if (textDelay) {
         socket.emit('DIALOGS:TYPING', selectDialog, true)
      }
   }, [textDelay])

   React.useEffect(() => {
      const oldImages = mutableMessage?.images?.map(item => {
         return { url: item } as IImage
      })

      setText(mutableMessage?.text || '')
      setImages(oldImages || [])
   }, [mutableMessage])

   React.useEffect(() => {
      if (mutableMessage && !text && !images.length) {
         dispatch(setMutableMessage(undefined))
      }
   }, [text, images.length])

   React.useEffect(() => {
      setIsDontReadyAttachments(
         !!images.find(item => item.status === 'LOADING' || item.status === 'ERROR')
      )
   }, [images])

   React.useEffect(() => {
      if (isSubmitSendMessage) {
         if (mutableMessage) {
            dispatch(setMutableMessage(undefined))
         }
         dispatch(setIsSubmitSendMessage(false))
         setText('')
         setImages([])
         setLoading(false)
      }
   }, [isSubmitSendMessage])

   const equalArray = () => {
      const oldImages = mutableMessage?.images
      const newImages = images.map(item => item.url)
      return (
         oldImages?.length == newImages.length &&
         oldImages?.every((value, index) => newImages[index] == value)
      )
   }

   const onSubmit = async () => {
      if ((!text && !images.length) || !selectDialog || isDontReadyAttachments) return
      setLoading(true)

      const currentImages = images.map(item =>
         !item.url.startsWith('blob') ? item.url : ''
      )

      if (mutableMessage) {
         if (mutableMessage.text !== text || !equalArray()) {
            socket.emit('MESSAGES:UPDATE', {
               id: mutableMessage.id,
               text,
               images: currentImages,
            })
         }
      } else {
         if (selectDialog.id === newDialog?.id) {
            socket.emit('DIALOGS:NEW', {
               text,
               images: currentImages,
               voice: '',
               newDialog,
            })
            return
         }
         if (selectDialog) {
            socket.emit('MESSAGES:SEND', {
               text,
               images: currentImages,
               voice: '',
               dialogId: selectDialog?.id,
            })
            return
         }
      }
   }

   const handleFocusInput = () => {
      const isUnreadMessages = messages.find(
         item => item.isRead === false && item.authorId !== user?.id
      )
      if (isUnreadMessages && selectDialog && newDialog?.id !== selectDialog.id) {
         socket.emit('MESSAGES:READING', selectDialog.id)
      }
   }

   const handleOpenEmoji = (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorElEmoji(event.currentTarget)
   }
   const handleCloseEmoji = () => {
      setAnchorElEmoji(null)
   }

   const handleSelectEmoji = (e: BaseEmoji) => {
      setText(prev => prev + e.native)
   }

   const handleChangeText = (e: React.ChangeEvent<HTMLInputElement>) => {
      setText(e.target.value)
   }

   const handleKeyUpInput = (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter' && !e.shiftKey) {
         onSubmit()
      }
   }

   return (
      <div>
         {loading && <LinearProgress />}
         <Wrapper>
            {!isRecordingVoice ? (
               <Body>
                  <IconContainer mr='4px'>
                     <UploadImages
                        multiple
                        icon='hidden'
                        max={4}
                        images={images}
                        onChange={setImages}
                     />
                  </IconContainer>

                  <InputContainer>
                     <InputContainerBorder>
                        {!!images.length && (
                           <AttachemntsContainer>
                              <ContainerImagesUpload
                                 items={images}
                                 onChange={setImages}
                              />
                           </AttachemntsContainer>
                        )}
                        <InputTextContainer>
                           <InputText
                              onKeyDown={e => {
                                 if (e.key === 'Enter' && !e.shiftKey) {
                                    e.preventDefault()
                                 }
                              }}
                              multiline
                              maxRows={8}
                              value={text}
                              onChange={handleChangeText}
                              onKeyUp={handleKeyUpInput}
                              onFocus={handleFocusInput}
                              placeholder='Начните вводить сообщение'
                              sx={{
                                 ' .MuiOutlinedInput-notchedOutline': { border: 'none' },
                              }}
                              endAdornment={
                                 <IconButton
                                    edge='end'
                                    color='primary'
                                    onClick={handleOpenEmoji}
                                    sx={{ p: 0 }}
                                 >
                                    <SentimentSatisfiedAltOutlined />
                                 </IconButton>
                              }
                           />
                        </InputTextContainer>
                        <ContainerEmojiPicker
                           anchorEl={anchorElEmoji}
                           open={openEmoji}
                           onClose={handleCloseEmoji}
                           onEmojiSelect={handleSelectEmoji}
                           transformOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                           anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                        />
                     </InputContainerBorder>
                  </InputContainer>

                  {text || images.length ? (
                     <IconContainer>
                        <IconButton
                           color='primary'
                           disabled={isDontReadyAttachments || loading}
                           onClick={onSubmit}
                        >
                           <SendOutlined />
                        </IconButton>
                     </IconContainer>
                  ) : (
                     <IconContainer>
                        <IconButton
                           color='primary'
                           onClick={() => setIsRecordingVoice(true)}
                           disabled={loading}
                        >
                           <MicNoneOutlined />
                        </IconButton>
                     </IconContainer>
                  )}
               </Body>
            ) : (
               <FormRecordingVoiceMessage
                  isRecording={isRecordingVoice}
                  setIsRecording={setIsRecordingVoice}
                  setLoading={setLoading}
               />
            )}
         </Wrapper>
      </div>
   )
}

export default FormInputsMessage

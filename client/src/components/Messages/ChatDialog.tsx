import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { CalendarMonthOutlined, InfoOutlined } from '@mui/icons-material'
import { Avatar, Box, IconButton, Link, styled, Typography } from '@mui/material'

import { fetchMessages } from '../../store/messages/actionCreators'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import FormInputsMessage from './FormInputsMessage'
import Message from './Message'
import FormatDateString from '../../utils/FormatDateString'
import Loader from '../Loader'
import RecordingIndication from './Message/RecordingIndication'
import TypingIndication from './Message/TypingIndication'

const Wrapper = styled('section')(({ theme }) => ({
   overflow: 'hidden',
   flex: 1,
   display: 'flex',
   flexDirection: 'column',
}))

const HeaderContainer = styled('div')(({ theme }) => ({
   height: '53px',
   position: 'sticky',
   zIndex: 2,
   display: 'flex',
   alignItems: 'center',
   padding: '0 16px',
   backgroundColor: 'rgba(255, 255, 255, 0.85)',
   backdropFilter: 'blur(12px)',
   backfaceVisibility: 'hidden',
   top: 0,
}))
const Header = styled('div')(({ theme }) => ({
   flex: 1,
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'space-between',
}))

const WrapperAvatar = styled('div')(({ theme }) => ({
   marginRight: '12px',
   cursor: 'pointer',
}))

const BlockNames = styled('div')(({ theme }) => ({}))

const Body = styled('div')(({ theme }) => ({
   padding: '0 16px 0 16px',
}))

const InfoUser = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   padding: '16px ',
   marginBottom: '16px',
   alignItems: 'center',
   justifyContent: 'center',
   borderBottom: '1px solid #e4e4e4',
   '&:hover': {
      cursor: 'pointer',
      backgroundColor: 'rgb(247, 249, 249)',
   },
   '& p': {
      marginBottom: '4px',
   },
}))

const ContainerMessages = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
}))

const ChatDialog: React.FC = () => {
   const dispatch = useDispatch()
   const navigate = useNavigate()
   const { messages, loadingStatusMessages } = useTypedSelector(state => state.messages)
   const { dialogs, selectDialog } = useTypedSelector(state => state.dialogs)

   const isNotReady =
      loadingStatusMessages === 'LOADING' || loadingStatusMessages === 'NEVER'
   React.useEffect(() => {
      if (selectDialog) {
         dispatch(fetchMessages(selectDialog.id))
      }
   }, [selectDialog])

   if (isNotReady && selectDialog) {
      return (
         <Wrapper>
            <Loader flexGrow={1} />
         </Wrapper>
      )
   }

   if (!selectDialog) {
      return (
         <Wrapper>
            <Box
               display='flex'
               flexDirection='column'
               alignItems='center'
               justifyContent='center'
               flexGrow='1'
            >
               <Box maxWidth='400px' p='15px'>
                  <Typography fontWeight='800' fontSize='31px' lineHeight='36px' mb='8px'>
                     Выберите диалог
                  </Typography>
                  <Typography variant='caption'>
                     Выберите из существующих разговоров, начните новый или просто
                     продолжайте серфить
                  </Typography>
               </Box>
            </Box>
         </Wrapper>
      )
   }

   const header = () => (
      <Header>
         <WrapperAvatar onClick={handlClickLinkOnUser}>
            <Avatar
               src={selectDialog?.companion.avatar}
               sx={{ width: '20px', height: '20px' }}
            />
         </WrapperAvatar>
         <BlockNames>
            <div onClick={handlClickLinkOnUser}>
               <Typography
                  fontSize='20px'
                  lineHeight='24px'
                  fontWeight={700}
                  sx={{ cursor: 'pointer' }}
               >
                  {selectDialog?.companion.fullName}
               </Typography>
            </div>
            <Typography variant='captionLight'>
               @{selectDialog?.companion.userName}
            </Typography>
         </BlockNames>
         <IconButton sx={{ ml: 'auto' }}>
            <InfoOutlined />
         </IconButton>
      </Header>
   )

   const info = () => (
      <InfoUser>
         <Typography fontSize='15px' lineHeight='25px' fontWeight={700}>
            <Link color='black' mr='6px'>
               {selectDialog?.companion.fullName}
            </Link>
            <Typography variant='captionLight'>
               @{selectDialog?.companion.userName}
            </Typography>
         </Typography>
         <Typography variant='body1'>Ваш персональный чат</Typography>
         <Typography variant='caption' display='flex' alignItems='center'>
            <CalendarMonthOutlined />
            {FormatDateString.register(selectDialog.companion.create)}
         </Typography>
      </InfoUser>
   )

   const handlClickLinkOnUser = () => {
      navigate(`/user/${selectDialog?.companion.userName}`)
   }

   return (
      <Wrapper sx={{ position: 'relative' }}>
         <Box position={'relative'} display='flex' flexDirection={'column'} flex='1'>
            <Box
               position='absolute'
               display='flex'
               flexDirection='column'
               top={0}
               left={0}
               right={0}
               bottom={0}
               overflow={{ overflowY: 'auto' }}
            >
               <HeaderContainer>{header()}</HeaderContainer>

               <Box display='flex' flexDirection={'column'} flexGrow={1}>
                  <Box
                     overflow={{ overflowX: 'hidden' }}
                     display='flex'
                     flexDirection={'column-reverse'}
                     flexGrow={1}
                     flexBasis='1px'
                     pt='53px'
                     mt='-53px'
                  >
                     <Body>
                        {info()}
                        <ContainerMessages>
                           {messages.map(item => (
                              <div key={item.id}>
                                 <Message item={item} />
                              </div>
                           ))}
                           {dialogs.find(
                              _item => _item.id === selectDialog.id && _item.isRecording
                           ) && <RecordingIndication />}
                           {dialogs.find(
                              _item => _item.id === selectDialog.id && _item.isTyping
                           ) && <TypingIndication />}
                        </ContainerMessages>
                     </Body>
                  </Box>

                  <FormInputsMessage />
               </Box>
            </Box>
         </Box>
      </Wrapper>
   )
}

export default ChatDialog

import React from 'react'
import { Avatar, Box, styled, Tab, Typography } from '@mui/material'

import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import TabContext from '@mui/lab/TabContext'
import UserApi from '../../api/UserApi'
import IUser from '../../models/IUser'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { useNavigate } from 'react-router-dom'
import MessagesApi from '../../api/MessagesApi'
import IMessage from '../../models/IMessage'
import { Email, Person } from '@mui/icons-material'
import FormatDateString from '../../utils/FormatDateString'

const Wrapper = styled('div')(({ theme }) => ({
   '& .MuiTabPanel-root': {
      padding: '0',
   },
}))

const ContainerTabs = styled('div')(({ theme }) => ({
   borderBottom: '1px solid rgb(239, 243, 244)',
   '& .MuiTabs-flexContainer': {
      justifyContent: 'center',
   },
}))

const ContainerPeople = styled('div')(({ theme }) => ({
   padding: '16px',
   cursor: 'pointer',
   display: 'flex',
   alignItems: 'center',
   transition: 'background-color 0.1s',
   '&:hover': {
      backgroundColor: 'rgb(247, 249, 249)',
   },
}))

const ContainerAvatarPeople = styled('div')(({ theme }) => ({
   flexBasis: '48px',
   flexGrow: '0',
   marginRight: '12px',
}))

const BodyContainerPeople = styled('div')(({ theme }) => ({}))

const ContainerMessages = styled('div')(({ theme }) => ({
   cursor: 'pointer',
   padding: '16px',
   transition: 'background-color 0.1s',
   '&:hover': {
      backgroundColor: 'rgb(247, 249, 249)',
   },
}))

const ContainerAvatarMessages = styled('div')(({ theme }) => ({
   marginRight: '12px',
}))

const BodyContainerMessages = styled('div')(({ theme }) => ({
   display: 'flex',
}))

const InfoMessages = styled('div')(({ theme }) => ({
   display: 'flex',
}))

const ContainerTextMessages = styled('div')(({ theme }) => ({
   padding: '2px 0 2px',
   textOverflow: 'ellipsis',
   overflow: 'hidden',
   wordWrap: 'break-word',
   '@supports (-webkit-line-clamp: 4)': {
      '-webkit-line-clamp': '4',
      display: '-webkit-box',
      '-webkit-box-orient': 'vertical',
   },
}))

const ContainerMarker = styled('div')(({ theme }) => ({
   display: 'flex',
   justifyContent: 'space-between',
   alignItems: 'center',
   padding: '4px 0 4px 16px',
   borderBottom: '1px solid rgb(239, 243, 244)',
}))

interface IProps {
   value?: string
   onChange?: (value: string) => void
}

const TabsSearch: React.FC<IProps> = props => {
   const navigate = useNavigate()
   const { dialogs, selectDialog } = useTypedSelector(state => state.dialogs)
   const [valueTabs, setValueTabs] = React.useState('all')
   const [people, setPeople] = React.useState<IUser[] | null>(null)
   const [messages, setMessages] = React.useState<IMessage[] | null>(null)

   React.useEffect(() => {
      if (!props.value) {
         setPeople(null)
         setMessages(null)
      }
      fetch(props.value)
   }, [props.value, valueTabs])

   const fetch = async (value?: string) => {
      if (!value) return
      switch (valueTabs) {
         case 'all': {
            fetchPeople(value)
            fetchMessages(value)
            break
         }
         case 'people': {
            fetchPeople(value)
            break
         }
         case 'messages': {
            fetchMessages(value)
            break
         }
      }
   }

   const fetchPeople = async (value: string) => {
      const res = await UserApi.findUsersWithDialog(value)
      setPeople(res.data)
   }

   const fetchMessages = async (value: string) => {
      const res = await MessagesApi.findWithDialog(value)
      setMessages(res.data)
   }

   const handleSelectPerson = (item: IUser) => {
      const dialogId = dialogs.find(_item => _item.companion.id === item.id)?.id
      if (dialogId) {
         navigate('/messages/' + dialogId)
      }
   }

   const handleSelectMessage = (item: IMessage) => {
      if (item) {
         navigate('/messages/' + item.dialogId)
      }
   }

   const handleChange = (event: React.SyntheticEvent<Element, Event>, value: any) => {
      setValueTabs(value)
   }

   const handleClickAvatar = (
      event: React.MouseEvent<HTMLDivElement>,
      userName?: string
   ) => {
      event.stopPropagation()
      if (userName) {
         navigate('/user/' + userName)
      }
   }

   const startMessage = () => (
      <Typography variant='caption' textAlign='center' display='block' pt='10px'>
         Попробуйте найти людей или сообщения
      </Typography>
   )

   const noResults = () => (
      <Box py='30px' mx='30px'>
         <Box mb='8px'>
            <Typography
               fontSize='31px'
               fontWeight='700'
               component='span'
               lineHeight='30px'
            >
               Нет результатов по "{props.value}"
            </Typography>
         </Box>
         <Box>
            <Typography variant='caption'>
               Введенный вами термин не дал результатов
            </Typography>
         </Box>
      </Box>
   )

   const markerPeople = () => (
      <ContainerMarker>
         <Box display='flex' alignItems='center'>
            <Box m='8px 12px 0 0'>
               <Person />
            </Box>
            <Typography fontSize='20px' lineHeight='24px' fontWeight='800' component='h2'>
               Люди
            </Typography>
         </Box>
      </ContainerMarker>
   )

   const markerMessages = () => (
      <ContainerMarker>
         <Box display='flex' alignItems='center'>
            <Box m='8px 12px 0 0'>
               <Email />
            </Box>
            <Typography fontSize='20px' lineHeight='24px' fontWeight='800' component='h2'>
               Сообщения
            </Typography>
         </Box>
      </ContainerMarker>
   )

   const renderPeople = () =>
      people?.map(item => (
         <ContainerPeople
            key={item.id}
            onClick={() => handleSelectPerson(item)}
            sx={{
               backgroundColor:
                  item.id === selectDialog?.companion.id ? 'rgb(239, 243, 244)' : '#fff',
            }}
         >
            <ContainerAvatarPeople onClick={e => handleClickAvatar(e, item.userName)}>
               <Avatar src={item.avatar} sx={{ width: '48px', height: '48px' }} />
            </ContainerAvatarPeople>
            <BodyContainerPeople>
               <Typography fontWeight={600}>{item.fullName}</Typography>
               <Typography variant='caption'>@{item.userName}</Typography>
            </BodyContainerPeople>
         </ContainerPeople>
      ))

   const renderMessages = () =>
      messages?.map(item => (
         <ContainerMessages
            key={item.id}
            onClick={() => handleSelectMessage(item)}
            sx={{
               backgroundColor:
                  item.dialogId === selectDialog?.id ? 'rgb(239, 243, 244)' : '#fff',
            }}
         >
            <BodyContainerMessages>
               <ContainerAvatarMessages
                  onClick={e => handleClickAvatar(e, item.author?.userName)}
               >
                  <Avatar
                     src={item.author?.avatar}
                     sx={{ width: '32px', height: '32px' }}
                  />
               </ContainerAvatarMessages>
               <InfoMessages>
                  <Typography fontWeight='600'>{item.author?.fullName}</Typography>
                  <Typography p='0 4px' variant='caption'>
                     &#183;
                  </Typography>
                  <Typography variant='caption'>
                     {FormatDateString.dateReceivedSMS(item.createdAt)}
                  </Typography>
               </InfoMessages>
            </BodyContainerMessages>
            <ContainerTextMessages>
               <div>{item.text}</div>
            </ContainerTextMessages>
         </ContainerMessages>
      ))

   return (
      <Wrapper>
         <TabContext value={valueTabs || 'all'}>
            <ContainerTabs>
               <TabList onChange={handleChange}>
                  <Tab label='Все' value='all' />
                  <Tab label='Люди' value='people' />
                  <Tab label='Сообщения' value='messages' />
               </TabList>
            </ContainerTabs>
            <TabPanel value='all'>
               {!people || !messages ? (
                  startMessage()
               ) : !people.length && !messages.length ? (
                  noResults()
               ) : (
                  <>
                     {people.length ? (
                        <>
                           {markerPeople()} {renderPeople()}
                        </>
                     ) : (
                        <></>
                     )}
                     {messages.length ? (
                        <>
                           {markerMessages()}
                           {renderMessages()}
                        </>
                     ) : (
                        <></>
                     )}
                  </>
               )}
            </TabPanel>
            <TabPanel value='people'>
               {!people ? startMessage() : !people.length ? noResults() : renderPeople()}
            </TabPanel>
            <TabPanel value='messages'>
               {!messages
                  ? startMessage()
                  : !messages.length
                  ? noResults()
                  : renderMessages()}
            </TabPanel>
         </TabContext>
      </Wrapper>
   )
}

export default TabsSearch

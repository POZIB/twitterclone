import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { Box, IconButton, styled, Typography } from '@mui/material'
import { ArrowBack, ForwardToInboxOutlined, Search } from '@mui/icons-material'

import useDebounce from '../../hooks/useDebounce'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { fetchDialogs, setSelectDialog } from '../../store/dialogs/actionCreators'
import IDialog from '../../models/IDialog'
import Modal from '../../components/Modal'
import DialogItem from './DialogItem'
import SearchInput from '../UI/SearchInput'
import Loader from '../Loader'
import TabsSearch from './TabsSearch'
import ModalNewMessage from './ModalNewMessage'
import socket from '../../socket'

const Wrapper = styled('section')(({ theme }) => ({
   maxWidth: '350px',
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
   borderRight: '1px solid #e4e4e4',
   position: 'relative',
}))

const ContainerHeader = styled('div')(({ theme }) => ({
   height: '53px',
   display: 'flex',
   alignItems: 'center',
   position: 'sticky',
   top: '0',
   zIndex: '2',
}))

const Header = styled('div')(({ theme }) => ({
   height: '53px',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'space-between',
   flex: '1',
   padding: '0 16px',
   backgroundColor: 'rgba(255, 255, 255, 0.85)',
   backdropFilter: 'blur(12px)',
}))

const Body = styled('div')(({ theme }) => ({
   overflowY: 'auto',
   paddingTop: '53px',
   marginTop: '-53px',
}))

const InputSearchSMS = styled('div')(({ theme }) => ({
   flex: 1,
}))

const ContainerDialogs = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
}))

const WrapperIcon = styled(IconButton)(({ theme }) => ({
   color: 'rgb(15, 20, 25)',

   '&:hover': {
      backgroundColor: 'rgba(15, 20, 25, 0.1)',
      color: 'rgb(15, 20, 25)',
   },
}))

const Dialogs: React.FC = () => {
   const params = useParams()
   const dispatch = useDispatch()
   const navigate = useNavigate()
   const { dialogs, newDialog, loadingStatusDialogs, selectDialog } = useTypedSelector(
      state => state.dialogs
   )

   const [valueSearch, setValueSearch] = React.useState('')
   const debounce = useDebounce(valueSearch, 350)

   React.useEffect(() => {
      dispatch(fetchDialogs())
   }, [])

   React.useEffect(() => {
      socket.emit('DIALOG:JOIN', selectDialog?.id)

      if (selectDialog?.id !== newDialog?.id) {
         socket.emit('MESSAGES:READING', selectDialog?.id)
      }
   }, [selectDialog])

   React.useEffect(() => {
      if (params.id && dialogs) {
         dispatch(setSelectDialog(Number(params.id)))
      } else {
         dispatch(setSelectDialog(undefined))
      }
   }, [params.id, dialogs.length])

   const handleSelectDialog = (
      event: React.MouseEvent<HTMLDivElement>,
      item?: IDialog
   ) => {
      event.stopPropagation()
      if (item) {
         navigate('/messages/' + item.id)
      }
   }

   const handleClickArrowBack = () => {
      setValueSearch('')
   }

   if (loadingStatusDialogs === 'LOADING' || loadingStatusDialogs === 'NEVER') {
      return (
         <Wrapper>
            <Loader flexGrow={1} />
         </Wrapper>
      )
   }

   return (
      <Wrapper>
         <Box display='flex' flexDirection='column' flex='1'>
            <Box
               position='absolute'
               display='flex'
               flexDirection='column'
               top={0}
               left={0}
               right={0}
               bottom={0}
               overflow={{ overflowY: 'auto' }}
            >
               <ContainerHeader>
                  <Header>
                     <Typography fontSize='20px' lineHeight='24px' fontWeight={700}>
                        Сообщения
                     </Typography>
                     <div>
                        <ModalNewMessage />
                     </div>
                  </Header>
               </ContainerHeader>
               <Body>
                  <Box display='flex' alignItems='center' padding='12px'>
                     {valueSearch && (
                        <IconButton
                           color='default'
                           onClick={handleClickArrowBack}
                           sx={{ p: 0, mr: '4px' }}
                        >
                           <ArrowBack />
                        </IconButton>
                     )}
                     <InputSearchSMS>
                        <SearchInput
                           variant='outlined'
                           value={valueSearch}
                           onChange={setValueSearch}
                        />
                     </InputSearchSMS>
                  </Box>

                  {valueSearch ? (
                     <TabsSearch value={debounce} />
                  ) : (
                     <ContainerDialogs>
                        <div onClick={e => handleSelectDialog(e, newDialog)}>
                           <DialogItem item={newDialog} />
                        </div>
                        {dialogs?.map(item => (
                           <div key={item.id} onClick={e => handleSelectDialog(e, item)}>
                              <DialogItem item={item} />
                           </div>
                        ))}
                     </ContainerDialogs>
                  )}
               </Body>
            </Box>
         </Box>
      </Wrapper>
   )
}

export default Dialogs

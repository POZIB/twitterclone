import React from 'react'
import { styled } from '@mui/material'

import recordingImg from '../../../assets/recording.gif'

const Wrapper = styled('div')(({ theme }) => ({
   marginBottom: '16px',
}))

const Container = styled('div')(({ theme }) => ({
   display: 'flex',
}))

const Content = styled('div')(({ theme }) => ({
   padding: '6px 16px',
   borderRadius: '16px 16px 16px 0',
   backgroundColor: 'rgb(235 240 243)',
   '& img': {
      height: '32px',
      width: '55px',
   },
}))

const RecordingIndication: React.FC = () => {
   return (
      <Wrapper>
         <Container>
            <Content>
               <img src={recordingImg} alt='' />
            </Content>
         </Container>
      </Wrapper>
   )
}

export default RecordingIndication

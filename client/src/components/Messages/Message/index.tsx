import React from 'react'
import { useDispatch } from 'react-redux'
import socket from '../../../socket'
import {
   IconButton,
   ListItemIcon,
   ListItemText,
   Menu,
   MenuItem,
   styled,
   Typography,
} from '@mui/material'
import {
   DeleteOutline,
   DoneAllOutlined,
   DoneOutlined,
   EditOutlined,
   MoreHorizOutlined,
} from '@mui/icons-material'

import { setMutableMessage } from '../../../store/messages/actionCreators'
import { useTypedSelector } from '../../../hooks/useTypedSelector'
import FormatDateString from '../../../utils/FormatDateString'
import IMessage from '../../../models/IMessage'
import Image from '../../Image'
import VoiceMessage from './VoiceMessage'

const Wrapper = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'flex-end',
   marginBottom: '24px',
}))

const Container = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
}))

const Content = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   transitionProperty: 'background-color , box-shadow',
   transitionDuration: '0.2s',
   '&:hover': {
      cursor: 'pointer',
   },
}))

const ContainerText = styled('div')(({ theme }) => ({
   padding: '12px 16px',
   wordBreak: 'break-word',
   transitionProperty: 'background-color, box-shadow',
   transitionDuration: '0.2s',
}))

const Text = styled('span')(({ theme }) => ({
   fontSize: '15px',
   fontWeight: '400',
   lineHeight: '20px',
   whiteSpace: 'pre-line',
}))

const ContainerImages = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   position: 'relative',
   maxWidth: '370px',
   overflow: 'hidden',
}))

const ImageList = styled('div')(({ theme }) => ({
   display: 'flex',
   flexWrap: 'wrap',
   margin: '-4px',
}))

const ImageItem = styled('div')(({ theme }) => ({
   margin: '0 4px 4px 0',
}))

const DateMessage = styled('span')(({ theme }) => ({
   marginTop: '2px',
}))

const Bottom = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'flex-end',
   color: '#536471',
   fontSize: '13px',
   fontWeight: '400',
   lineHeight: '16px',
}))

const ContainerSettings = styled('div')(({ theme }) => ({
   padding: '0 4px',
   '& .MuiIconButton-root': {
      color: 'transparent',
   },
   '&:hover .MuiIconButton-root': {
      color: 'rgba(0, 0, 0, 0.54)',
   },
}))

const CustomMenu = styled(Menu)(({ theme }) => ({}))

interface IProps {
   item: IMessage
}

const Message: React.FC<IProps> = ({ item }) => {
   const dispatch = useDispatch()
   const { user } = useTypedSelector(state => state.auth)
   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
   const open = Boolean(anchorEl)

   const IS_MI = user?.id === item?.authorId || false

   const handleDeleteClick = () => {
      handleClose()
      socket.emit('MESSAGES:DELETE', item)
   }

   const handleClickOptions = (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation()
      setAnchorEl(event.currentTarget)
   }

   const handleClose = () => {
      setAnchorEl(null)
   }

   const handleChangeClick = () => {
      handleClose()
      dispatch(setMutableMessage(item))
   }

   const renderContent = () => {
      const imageSize = () => {
         if (item.images?.length === 1 || item.images?.length === 2) {
            return 'calc(100% - 4px)'
         } else {
            return 'calc(50% - 4px)'
         }
      }

      const borderRadiusWithImages = () => {
         if (IS_MI) {
            if (!!item.images?.length) {
               return '0 0 0 16px'
            } else {
               return '16px 16px 0 16px'
            }
         } else {
            if (!!item.images?.length) {
               return '0 0 16px 0'
            } else {
               return '16px 16px 16px 0'
            }
         }
      }
      if (item.voice) {
         return <VoiceMessage item={item.voice} IS_MI={IS_MI} />
      }

      return (
         <>
            {!!item.images && (
               <ContainerImages
                  sx={{
                     borderRadius: IS_MI ? '16px 16px 0 16px' : '16px 16px 16px 0',
                  }}
               >
                  <ImageList
                     sx={{
                        flexDirection: item.images?.length <= 2 ? 'column' : 'row',
                        justifyContent: IS_MI ? 'flex-end' : 'flex-start',
                     }}
                  >
                     {item.images?.map((url, index) => (
                        <ImageItem key={url} sx={{ width: imageSize }}>
                           {/* <div></div> */}
                           <Image src={url} />
                        </ImageItem>
                     ))}
                  </ImageList>
               </ContainerImages>
            )}
            {!!item.text && (
               <ContainerText
                  sx={{
                     backgroundColor: IS_MI ? 'primary.main' : 'rgb(235 240 243)',
                     borderRadius: borderRadiusWithImages,
                     '&: hover': {
                        backgroundColor: IS_MI ? 'primary.dark' : 'rgb(215 223 228)',
                     },
                  }}
               >
                  <Text sx={{ color: IS_MI ? '#fff' : '#0f1419' }}>{item.text}</Text>
               </ContainerText>
            )}
         </>
      )
   }

   return (
      <Wrapper sx={{ flexDirection: IS_MI ? 'row-reverse' : 'row' }}>
         <Container sx={{ alignItems: IS_MI ? 'flex-end' : 'flex-start' }}>
            <Content sx={{ alignItems: IS_MI ? 'flex-end' : 'flex-start' }}>
               {renderContent()}
            </Content>
            <Bottom sx={{ flexDirection: IS_MI ? 'row' : 'row-reverse' }}>
               {item.isChanged && (
                  <Typography
                     variant='caption'
                     lineHeight='18px'
                     margin={IS_MI ? '0 4px 0 0' : '0 0 0 4px'}
                  >
                     изменено
                  </Typography>
               )}
               <DateMessage>
                  {FormatDateString.dateReceivedSMS(item.createdAt)}
               </DateMessage>
               {item.authorId === user?.id &&
                  (item.isRead ? (
                     <DoneAllOutlined sx={{ width: '16px', height: '15px' }} />
                  ) : (
                     <DoneOutlined sx={{ width: '16px', height: '15px' }} />
                  ))}
            </Bottom>
         </Container>
         <ContainerSettings>
            <IconButton onClick={handleClickOptions}>
               <MoreHorizOutlined />
            </IconButton>
            <div>
               <CustomMenu
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  anchorOrigin={{ vertical: 'top', horizontal: IS_MI ? 'right' : 'left' }}
                  transformOrigin={{
                     vertical: 'top',
                     horizontal: IS_MI ? 'right' : 'left',
                  }}
               >
                  {IS_MI && (
                     <div>
                        <MenuItem onClick={handleDeleteClick}>
                           <ListItemIcon sx={{ color: 'rgb(244, 33, 46)' }}>
                              <DeleteOutline fontSize='medium' />
                           </ListItemIcon>
                           <ListItemText
                              primaryTypographyProps={{ color: 'rgb(244, 33, 46)' }}
                           >
                              Удалить
                           </ListItemText>
                        </MenuItem>
                        {!item.voice && (
                           <MenuItem onClick={handleChangeClick}>
                              <ListItemIcon>
                                 <EditOutlined fontSize='medium' />
                              </ListItemIcon>
                              <ListItemText>Редактировать</ListItemText>
                           </MenuItem>
                        )}
                     </div>
                  )}
               </CustomMenu>
            </div>
         </ContainerSettings>
      </Wrapper>
   )
}

export default Message

import React from 'react'
import { styled } from '@mui/material'

import typingImg from '../../../assets/typing.gif'

const Wrapper = styled('div')(({ theme }) => ({
   marginBottom: '16px',
}))

const Container = styled('div')(({ theme }) => ({
   display: 'flex',
}))

const Content = styled('div')(({ theme }) => ({
   padding: '10px 18px',
   borderRadius: '16px 16px 16px 0',
   backgroundColor: 'rgb(235 240 243)',
   '& img': {
      width: '44px',
      height: '22px',
   },
}))

const TypingIndication: React.FC = () => {
   return (
      <Wrapper>
         <Container>
            <Content>
               <img src={typingImg} alt='' />
            </Content>
         </Container>
      </Wrapper>
   )
}

export default TypingIndication

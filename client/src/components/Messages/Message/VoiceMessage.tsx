import React from 'react'
import { IconButton, Typography, styled } from '@mui/material'
import { PauseCircleOutline, PlayCircleOutline } from '@mui/icons-material'

import { ReactComponent as WaveSound } from '../../../assets/wave.svg'
import FormatTimeString from '../../../utils/FormatTimeString'

const ContainerAudio = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   padding: '2px 12px',
   position: 'relative',
}))

const ContainerWave = styled('div')(({ theme }) => ({
   height: '50px',
   display: 'flex',
   position: 'relative',
   '& svg': {
      maxWidth: '100%',
      maxHeight: '100%',
   },
}))

const WaveMask = styled('div')(({ theme }) => ({
   height: '100%',
   display: 'flex',
   position: 'absolute',
   flex: '1',
   top: '0',
   left: '0',
   transition: '0.2s linear',
}))

const DurationAudio = styled('span')(({ theme }) => ({}))

interface IProps {
   item: string
   IS_MI: boolean
}

const VoiceMessage: React.FC<IProps> = ({ item, IS_MI }) => {
   const voiceRef = React.useRef<HTMLAudioElement>(null)
   const [isPlaying, setIsPlaying] = React.useState(false)
   const [progress, setProgress] = React.useState(100)
   const [duration, setDuration] = React.useState(0)

   const togglePlay = () => {
      if (!isPlaying && voiceRef) {
         voiceRef.current?.play()
      } else {
         voiceRef.current?.pause()
      }
   }

   React.useEffect(() => {
      if (!voiceRef || !voiceRef.current) return
      const voiceCurrent = voiceRef.current
      voiceCurrent.addEventListener('playing', () => setIsPlaying(true))
      voiceCurrent.addEventListener('pause', () => setIsPlaying(false))
      voiceCurrent.addEventListener('ended', () => onEndPlaying(voiceCurrent))
      voiceCurrent.addEventListener('loadedmetadata', () => loadedMetaData(voiceCurrent))

      return () => {
         voiceCurrent.removeEventListener('playing', () => setIsPlaying(true))
         voiceCurrent.removeEventListener('pause', () => setIsPlaying(false))
         voiceCurrent.removeEventListener('ended', () => onEndPlaying(voiceCurrent))
         voiceCurrent.removeEventListener('loadedmetadata', () =>
            loadedMetaData(voiceCurrent)
         )
      }
   }, [voiceRef])

   const onEndPlaying = (voiceCurrent: HTMLAudioElement) => {
      setProgress(100)
      setDuration(voiceCurrent.duration)
      setIsPlaying(false)
   }

   const loadedMetaData = (voiceCurrent: HTMLAudioElement) => {
      if (voiceCurrent.duration == Infinity) {
         voiceCurrent.currentTime = 1e101
         voiceCurrent.ontimeupdate = function () {
            this.ontimeupdate = () => {
               const duration = voiceCurrent.duration
               const current = voiceCurrent.currentTime
               setDuration(duration - current)
               setProgress((1 - current / duration) * 100)
               return
            }
            voiceCurrent.currentTime = 0
            setDuration(voiceCurrent.duration)
            return
         }
      }
   }

   return (
      <ContainerAudio
         sx={{
            flexDirection: IS_MI ? 'row' : 'row-reverse',
            backgroundColor: IS_MI ? 'primary.main' : 'rgb(235 240 243)',
            borderRadius: IS_MI ? '16px 16px 0 16px' : '16px 16px 16px 0',
            '&: hover': {
               backgroundColor: IS_MI ? 'primary.dark' : 'rgb(215 223 228)',
            },
         }}
      >
         <audio ref={voiceRef} src={item} />
         <IconButton
            sx={{
               p: '3px',
               color: IS_MI ? '#fff' : '#5f5f5f',
               '&:hover': { color: IS_MI ? '#e2e2e2' : '#3e3e3e' },
            }}
            onClick={togglePlay}
         >
            {isPlaying && <PauseCircleOutline />}
            {!isPlaying && <PlayCircleOutline />}
         </IconButton>
         <ContainerWave>
            <WaveSound color={IS_MI ? 'rgba(255, 255, 255, 0.3)' : '#0080fd7a'} />
            <WaveMask sx={{ clipPath: `inset(0px ${progress}% 0px 0px)` }}>
               <WaveSound color={IS_MI ? '#fff' : '#2b8eef'} />
            </WaveMask>
         </ContainerWave>
         <DurationAudio sx={{ margin: IS_MI ? '0 0 0 2px' : '0 2px 0 0' }}>
            <Typography
               color={IS_MI ? '#fff' : '#0f1419'}
               fontSize='12px'
               fontWeight={400}
            >
               {FormatTimeString.min(duration)}
            </Typography>
         </DurationAudio>
      </ContainerAudio>
   )
}

export default VoiceMessage

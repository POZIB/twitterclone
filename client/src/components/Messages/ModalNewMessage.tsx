import React from 'react'
import { styled, IconButton, Avatar, Typography, LinearProgress } from '@mui/material'
import { ForwardToInboxOutlined, Search } from '@mui/icons-material'

import Modal from '../Modal'
import IUser from '../../models/IUser'
import UserApi from '../../api/UserApi'
import useDebounce from '../../hooks/useDebounce'
import { display } from '@mui/system'
import { useNavigate } from 'react-router-dom'
import DialogsApi from '../../api/DialogsApi'
import { useDispatch } from 'react-redux'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { newDialog } from '../../store/dialogs/actionCreators'

const Wrapper = styled('div')(({ theme }) => ({}))

const WrapperIconOpenModal = styled(IconButton)(({ theme }) => ({
   color: 'rgb(15, 20, 25)',
   '&:hover': {
      backgroundColor: 'rgba(15, 20, 25, 0.1)',
      color: 'rgb(15, 20, 25)',
   },
}))

const WrapperIconSearch = styled('div')(({ theme }) => ({
   display: 'flex',
   paddingLeft: '12px',
   color: 'rgb(83, 100, 113)',
}))

const WrapperForm = styled('form')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   flex: '1',
}))

const WrapperInput = styled('div')(({ theme }) => ({}))

const ContainerInput = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   flex: '1',

   '& input': {
      width: '100%',
      outline: 'none',
      border: 'none',
      padding: '12px',
   },
}))

const ContainerListItems = styled('div')(({ theme }) => ({
   minHeight: '400px',
   maxHeight: '0',
   display: 'flex',
   flexDirection: 'column',
   borderTop: '1px solid rgb(207, 217, 222)',
}))

const ListItems = styled('div')(({ theme }) => ({
   overflowY: 'auto',
}))

const WrapperItem = styled('div')(({ theme }) => ({}))

const ContainerItem = styled('div')(({ theme }) => ({
   display: 'flex',
   padding: '12px 16px',
   cursor: 'pointer',
   transition: 'background-color 0.2s',
   '&:hover': {
      backgroundColor: 'rgb(247, 249, 249)',
   },
}))

const WrapperAvatar = styled('div')(({ theme }) => ({
   marginRight: '12px',
}))

const ContentItem = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
}))

const MockLinearProgress = styled('div')(({ theme }) => ({
   marginBottom: '4px',
}))

const WrapperNoResults = styled('div')(({ theme }) => ({
   margin: '20px',
}))

const ModalNewMessage: React.FC = () => {
   const navigate = useNavigate()
   const dispatch = useDispatch()
   const { user: myProfile } = useTypedSelector(state => state.auth)
   const [isOpenNewMessage, setIsOpenNewMessage] = React.useState(false)
   const [loading, setLoading] = React.useState(true)
   const [users, setUsers] = React.useState<IUser[]>([])
   const [valueSearch, setValueSearch] = React.useState('')
   const debounce = useDebounce(valueSearch, 300)

   React.useEffect(() => {
      if (!isOpenNewMessage) return

      if (debounce) {
         fetchPeople(debounce)
      } else {
         fetchPeopleWithDialog()
      }
   }, [debounce, isOpenNewMessage])

   const fetchPeople = async (value: string) => {
      try {
         const res = await UserApi.findUsers(value)
         setUsers(res.data)
      } catch (error) {
         setUsers([])
      } finally {
         setLoading(false)
      }
   }

   const fetchPeopleWithDialog = async () => {
      try {
         const res = await UserApi.findUsersWithDialog('')
         setUsers(res.data)
      } catch (error) {
         setUsers([])
      } finally {
         setLoading(false)
      }
   }

   const handleSelectUser = async (user: IUser) => {
      if (!user || !user.id) return
      try {
         const res = await DialogsApi.openDialog(user.id)
         if (res.data) {
            navigate('/messages/' + res.data)
         } else {
            if (myProfile && myProfile.id) {
               const id = dispatch(newDialog(myProfile.id, user)).payload?.id
               navigate('/messages/' + id)
            }
         }
         setIsOpenNewMessage(false)
      } catch (error) {}
   }

   const handleChangeText = (event: React.ChangeEvent<HTMLInputElement>) => {
      setLoading(true)
      setValueSearch(event?.target.value)
   }

   const NoResultsPeople = () => (
      <WrapperNoResults>
         <Typography variant='h6' fontWeight='600' fontSize='20px'>
            Нет результатов по "{valueSearch}"
         </Typography>
         <Typography variant='caption'>Попробуйте изменить запрос</Typography>
      </WrapperNoResults>
   )

   const NoResultsPeopleWithDialog = () => (
      <WrapperNoResults>
         <Typography variant='h6' fontWeight='600' fontSize='20px'>
            У вас нет диалогов
         </Typography>
         <Typography variant='caption'>
            Найдите друга и напишите ему первое сообщение
         </Typography>
      </WrapperNoResults>
   )

   return (
      <Wrapper>
         <WrapperIconOpenModal onClick={() => setIsOpenNewMessage(true)}>
            <ForwardToInboxOutlined />
         </WrapperIconOpenModal>
         {isOpenNewMessage && (
            <Modal
               title='Новое сообщение'
               maxWidth='sm'
               maxHeight='90vh'
               padding='0'
               isOpen={isOpenNewMessage}
               onClose={() => setIsOpenNewMessage(false)}
            >
               {
                  <WrapperForm>
                     <WrapperInput>
                        <ContainerInput>
                           <WrapperIconSearch>
                              <Search />
                           </WrapperIconSearch>
                           <input
                              type='text'
                              placeholder='Поиск пользователей'
                              value={valueSearch}
                              onChange={handleChangeText}
                           />
                        </ContainerInput>
                     </WrapperInput>
                     {loading ? (
                        <LinearProgress />
                     ) : (
                        <MockLinearProgress></MockLinearProgress>
                     )}

                     <ContainerListItems>
                        {debounce && !users.length && !loading && NoResultsPeople()}

                        {!debounce &&
                           !users.length &&
                           !loading &&
                           NoResultsPeopleWithDialog()}
                        <ListItems>
                           {users.map(item => (
                              <WrapperItem
                                 key={item.id}
                                 onClick={() => handleSelectUser(item)}
                              >
                                 <ContainerItem>
                                    <WrapperAvatar>
                                       <Avatar src={item.avatar} />
                                    </WrapperAvatar>
                                    <ContentItem>
                                       <Typography
                                          variant='caption'
                                          color='black'
                                          fontWeight='700'
                                       >
                                          {item.fullName}
                                       </Typography>
                                       <Typography variant='caption'>
                                          @{item.userName}
                                       </Typography>
                                    </ContentItem>
                                 </ContainerItem>
                              </WrapperItem>
                           ))}
                        </ListItems>
                     </ContainerListItems>
                  </WrapperForm>
               }
            </Modal>
         )}
      </Wrapper>
   )
}

export default ModalNewMessage

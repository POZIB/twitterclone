import React from 'react'
import { Avatar, Box, styled, Typography } from '@mui/material'

import UserApi from '../../../api/UserApi'
import useDebounce from '../../../hooks/useDebounce'
import IUser from '../../../models/IUser'
import SearchInput from '../../UI/SearchInput'
import { Link } from 'react-router-dom'
import Loader from '../../Loader'

const Wrapper = styled('div')(({ theme }) => ({
   position: 'relative',
}))

const WrapperList = styled('div')(({ theme }) => ({
   width: '100%',
   display: 'flex',
   flexDirection: 'column',
   position: 'absolute',
   top: '2px',
   right: '0',
   zIndex: 15,
}))

const ContainerList = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   maxHeight: 'calc(80vh - 53px)',
   minHeight: '100px',
   boxShadow:
      'rgb(101 119 134 / 20%) 0px 0px 15px, rgb(101 119 134 / 15%) 0px 0px 3px 1px',
   backgroundColor: '#fff',
   borderRadius: '8px',
   overflowY: 'auto',
   overflowX: 'hidden',
}))

const ItemList = styled('div')(({ theme }) => ({
   transition: 'background-color 0.2s',
   '&:hover': {
      backgroundColor: 'rgb(247, 249, 249)',
   },
   '&:hover a': {
      textDecoration: 'none',
   },
}))

const ContainerItem = styled('div')(({ theme }) => ({
   display: 'flex',
   padding: '12px 16px',
}))

const WrapperAvater = styled('div')(({ theme }) => ({
   marginRight: '12px',
}))

const Content = styled('div')(({ theme }) => ({
   display: 'flex',
   flexDirection: 'column',
   minWidth: '0',
}))

const BlockNames = styled('div')(({ theme }) => ({}))

const BlockAbout = styled('div')(({ theme }) => ({
   display: 'flex',
   minWidth: '0px',
   '& span': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflowWrap: 'break-word',
   },
}))

const WrapperLoader = styled('div')(({ theme }) => ({
   padding: '20px 0',
}))

const WrapperNoResults = styled('div')(({ theme }) => ({
   padding: '16px 25px',
}))

const BlockSearchUsers: React.FC = () => {
   const [value, setValue] = React.useState('')
   const [loading, setLoading] = React.useState(false)
   const [focusInput, setFocusInput] = React.useState(false)
   const [users, setUsers] = React.useState<IUser[]>([])
   const debounce = useDebounce(value, 350)

   React.useEffect(() => {
      setLoading(true)
      if (!value) {
         setUsers([])
         setLoading(false)
      }
   }, [value])

   React.useEffect(() => {
      fetchUsers()
   }, [debounce])

   const fetchUsers = async () => {
      if (!value) return
      try {
         setUsers([])
         const res = await UserApi.findUsers(value)
         setUsers(res.data)
      } catch (error) {
         setUsers([])
      } finally {
         setLoading(false)
      }
   }

   const NoResults = () => (
      <WrapperNoResults>
         <Typography variant='h6' fontWeight='600' fontSize='18px' mb='4px'>
            Нет результатов по "{value}"
         </Typography>
         <Typography variant='caption'>Попробуйте поискать что-нибудь другое</Typography>
      </WrapperNoResults>
   )

   return (
      <Wrapper>
         <SearchInput
            variant='contained'
            value={value}
            onChange={setValue}
            onFocus={setFocusInput}
         />
         <Box sx={{ position: 'relative' }}>
            {value && focusInput && (
               <WrapperList>
                  <ContainerList>
                     {loading && (
                        <WrapperLoader>
                           <Loader size={26} />
                        </WrapperLoader>
                     )}
                     {!users?.length && !loading
                        ? NoResults()
                        : users?.map(item => (
                             <ItemList key={item.id}>
                                <Link to={'/user/' + item.userName}>
                                   <ContainerItem>
                                      <WrapperAvater>
                                         <Avatar
                                            src={item.avatar}
                                            sx={{ width: '56px', height: '56px' }}
                                         />
                                      </WrapperAvater>
                                      <Content>
                                         <BlockNames>
                                            <Typography
                                               variant='body1'
                                               color='black'
                                               fontWeight='700'
                                            >
                                               {item.fullName}
                                            </Typography>
                                            <Typography variant='caption'>
                                               @{item.userName}
                                            </Typography>
                                         </BlockNames>
                                         <BlockAbout>
                                            <Typography variant='caption'>
                                               {item.about}
                                            </Typography>
                                         </BlockAbout>
                                      </Content>
                                   </ContainerItem>
                                </Link>
                             </ItemList>
                          ))}
                  </ContainerList>
               </WrapperList>
            )}
         </Box>
      </Wrapper>
   )
}

export default BlockSearchUsers

import React from 'react'
import { useNavigate, Link } from 'react-router-dom'
import { Avatar, styled, Typography, Skeleton, Box } from '@mui/material'

import FollowApi from '../../../api/FollowApi'
import ButtonFollow from '../../UI/ButtonFollow'
import WhomToReadApi from '../../../api/WhomToReadApi'
import { IUsersFollow } from '../../../models/IFollow'
import ButtonUnFollow from '../../UI/ButtonUnFollow'

const Wrapper = styled('div')(() => ({
   backgroundColor: 'rgb(247, 249, 249)',
   borderRadius: '15px',
   overflow: 'hidden',
   marginTop: '20px',
}))

const HeaderBlock = styled('div')(() => ({
   textAlign: 'left',
   padding: '12px 16px',
   borderBottom: '1px solid #eeeeee',
}))

const Body = styled('div')(() => ({}))

const List = styled('ul')(() => ({}))

const ItemList = styled('li')(() => ({
   cursor: 'pointer',
   minHeight: '50px',
   display: 'flex',
   alignItems: 'center',
   padding: '12px 16px',
   transition: ' background-color 0.2s',
   '&: not(:last-child)': {
      borderBottom: '1px solid #eeeeee',
   },
   '&:hover': {
      backgroundColor: '#edf3f6',
   },
}))

const AvatarWrapper = styled('div')(() => ({
   marginRight: '10px',
}))

const Content = styled('div')(() => ({
   display: 'flex',
   justifyContent: 'space-between',
   flex: '1',
   minWidth: '0',
}))

const BlockWhomToRead: React.FC = (): React.ReactElement => {
   const navigate = useNavigate()
   const [items, setItems] = React.useState<IUsersFollow[]>([])

   React.useEffect(() => {
      fetchWhomToRead()
   }, [])

   const fetchWhomToRead = async () => {
      try {
         const res = await WhomToReadApi.fetchWhomToRead()
         setItems(res.data)
      } catch (error) {
         setItems([])
      }
   }

   const handleClickNavigate = (
      event: React.MouseEvent<HTMLLIElement>,
      userName: string
   ) => {
      event.stopPropagation()
      navigate('/user/' + userName)
   }

   const handleFollow = async (
      event: React.MouseEvent<HTMLDivElement>,
      userId?: number
   ) => {
      event.stopPropagation()
      if (!userId) return
      const res = await FollowApi.follow(userId)
      if (res.data) {
         setItems(
            items.map(item =>
               item.id === userId ? { ...item, isFollowing: true } : item
            )
         )
      }
   }

   const handleUnFollow = async (
      event: React.MouseEvent<HTMLDivElement>,
      userId?: number
   ) => {
      event.stopPropagation()
      if (!userId) return
      try {
         const res = await FollowApi.unFollow(userId)
         if (res.data) {
            setItems(
               items.map(item =>
                  item.id === userId ? { ...item, isFollowing: false } : item
               )
            )
         }
      } catch (error) {}
   }

   return (
      <Wrapper>
         <HeaderBlock>
            <Typography fontWeight={800} fontSize='20px' lineHeight='24px'>
               Кого читать
            </Typography>
         </HeaderBlock>
         <Body>
            <List>
               {items.length
                  ? items.map(item => (
                       <ItemList
                          key={item.id}
                          onClick={e => handleClickNavigate(e, item.userName)}
                       >
                          <AvatarWrapper>
                             <Link
                                to={'/user/' + item.userName}
                                onClick={e => e.stopPropagation()}
                             >
                                <Avatar
                                   src={item.avatar}
                                   sx={{ height: '48px', width: '48px' }}
                                />
                             </Link>
                          </AvatarWrapper>
                          <Content>
                             <Box
                                display='flex'
                                flexDirection='column'
                                flex='1'
                                minWidth='0'
                             >
                                <Typography
                                   fontSize='15px'
                                   lineHeight='20px'
                                   fontWeight={700}
                                >
                                   <Link
                                      to={'/user/' + item.userName}
                                      onClick={e => e.stopPropagation()}
                                   >
                                      {item.fullName}
                                   </Link>
                                </Typography>
                                <Box display='flex'>
                                   <Typography
                                      variant='caption'
                                      fontSize='15px'
                                      fontWeight={400}
                                      lineHeight='20px'
                                      overflow='hidden'
                                      textOverflow='ellipsis'
                                      whiteSpace='nowrap'
                                   >
                                      @{item.userName}
                                   </Typography>
                                </Box>
                             </Box>
                             <Box ml='6px'>
                                {item.isFollowing ? (
                                   <div onClick={e => handleUnFollow(e, item.id)}>
                                      <ButtonUnFollow />
                                   </div>
                                ) : (
                                   <div onClick={e => handleFollow(e, item.id)}>
                                      <ButtonFollow />
                                   </div>
                                )}
                             </Box>
                          </Content>
                       </ItemList>
                    ))
                  : [...Array(3)].map((item, index) => (
                       <ItemList key={index}>
                          <AvatarWrapper>
                             <Avatar sx={{ height: '48px', width: '48px' }} />
                          </AvatarWrapper>
                          <div>
                             <Skeleton variant='text' width='100px' />
                             <Skeleton variant='text' width='70px' />
                          </div>
                       </ItemList>
                    ))}
            </List>
         </Body>
      </Wrapper>
   )
}

export default BlockWhomToRead

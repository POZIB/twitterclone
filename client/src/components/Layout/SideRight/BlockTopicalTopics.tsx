import React from 'react'
import { Link } from 'react-router-dom'

import { styled, Typography } from '@mui/material'

import { useTypedSelector } from '../../../hooks/useTypedSelector'

const Wrapper = styled('div')(() => ({
   backgroundColor: 'rgb(247, 249, 249)',
   borderRadius: '15px',
   overflow: 'hidden',
   marginTop: '20px',
}))

const HeaderBlock = styled('div')(() => ({
   textAlign: 'left',
   padding: '12px 16px',
   borderBottom: '1px solid #eeeeee',
}))

const ListItem = styled('ul')(() => ({}))

const ItemTopic = styled('li')(() => ({
   cursor: 'pointer',
   minHeight: '50px',
   display: 'flex',
   flexDirection: 'column',
   justifyContent: 'center',
   padding: '0 10px',
   '&: not(:last-child)': {
      borderBottom: '1px solid #eeeeee',
   },
   '&: hover': {
      backgroundColor: '#edf3f6',
   },
}))

const BlockTopicalTopics: React.FC = (): React.ReactElement => {
   const { topicals } = useTypedSelector(state => state.topicals)

   return (
      <Wrapper>
         <HeaderBlock>
            <Typography fontWeight={800} fontSize='20px' lineHeight='24px'>
               Актуальные темы
            </Typography>
         </HeaderBlock>
         <ListItem>
            {topicals.map(item => (
               <ItemTopic key={item.id}>
                  <Link to={'/home/search/'}>
                     <Typography fontSize={14} fontWeight={700}>
                        {item.headline}
                     </Typography>
                  </Link>
                  <Typography fontSize={12} fontWeight={100}>
                     Твитов: {item.count}
                  </Typography>
               </ItemTopic>
            ))}
         </ListItem>
      </Wrapper>
   )
}

export default BlockTopicalTopics

import React from 'react'
import { styled } from '@mui/material'

import BlockTopicalTopics from './BlockTopicalTopics'
import BlockWhomToRead from './BlockWhomToRead'
import BlockSearchUsers from './BlockSearchUsers'

const Wrapper = styled('section')(() => ({
   paddingTop: '5px',
   position: 'sticky',
   top: 0,
}))

const SideRight: React.FC = (): React.ReactElement => {
   return (
      <Wrapper>
         <BlockSearchUsers />
         <BlockTopicalTopics />
         <BlockWhomToRead />
      </Wrapper>
   )
}

export default SideRight

import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import {
   Avatar,
   Box,
   IconButton,
   styled,
   Typography,
   Menu,
   MenuItem,
   ListItemIcon,
   Divider,
} from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import { Check, Logout, Settings } from '@mui/icons-material'

import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { logout } from '../../../store/auth/actionCreators'

const Wrapper = styled('div')(({ theme }) => ({
   position: 'fixed',
   bottom: '15px',
   backgroundColor: '#f1f1f142',
   borderRadius: 30,
   display: 'flex',
   alignItems: 'center',
   padding: '5px 7px',
   '&:hover': {
      cursor: 'pointer',
      backgroundColor: 'rgba(25, 118, 210, 0.04)',
   },
}))

const BlockNames = styled('div')(({ theme }) => ({
   marginLeft: '10px',
}))

const BlockButton = styled('div')(({ theme }) => ({
   marginLeft: 'auto',
}))

const UserSideProfile: React.FC = (): React.ReactElement => {
   const { user } = useTypedSelector(state => state.auth)
   const navigate = useNavigate()
   const dispatch = useDispatch()

   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
   const open = Boolean(anchorEl)

   const handleClickOptions = (event: React.MouseEvent<HTMLElement>) => {
      setAnchorEl(event.currentTarget)
   }
   const handleClose = () => {
      setAnchorEl(null)
   }

   const handleClickLogout = () => {
      dispatch(logout())
      navigate('/i/flow')
   }

   const handleClickMyProfile = () => {
      navigate('/user/' + user?.userName)
   }

   return (
      <Wrapper sx={{ with: { xl: '200px', xs: '50px' } }}>
         <Avatar
            sx={{ width: 35, height: 35 }}
            src={user?.avatar}
            onClick={handleClickOptions}
         />
         <BlockNames sx={{ display: { xl: 'block', xs: 'none' } }}>
            <b onClick={handleClickMyProfile}>{user?.fullName}</b>
            <Typography fontSize={14}>@{user?.userName}</Typography>
         </BlockNames>
         <BlockButton sx={{ display: { xl: 'block', xs: 'none' } }}>
            <IconButton color='primary' onClick={handleClickOptions}>
               <KeyboardArrowDownIcon />
            </IconButton>
         </BlockButton>

         <Menu
            anchorEl={anchorEl}
            id='account-menu'
            open={open}
            onClose={handleClose}
            onClick={handleClose}
            PaperProps={{
               elevation: 0,
               sx: {
                  overflow: 'visible',
                  borderRadius: 2,
                  filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                  mt: 1.5,
                  '& .MuiAvatar-root': {
                     width: 32,
                     height: 32,
                     ml: -0.5,
                     mr: 1,
                  },
                  '&:before': {
                     content: '""',
                     display: 'block',
                     position: 'absolute',
                     bottom: -10,
                     left: '33%',
                     width: 10,
                     height: 10,
                     bgcolor: 'background.paper',
                     transform: 'translateY(-50%) rotate(45deg)',
                     zIndex: 0,
                  },
               },
            }}
            transformOrigin={{ horizontal: 'center', vertical: 'bottom' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
         >
            <MenuItem onClick={handleClickMyProfile}>
               <Avatar sx={{ width: 35, height: 35 }} src={user?.avatar} />
               <Box display='flex' alignItems='center'>
                  <Box mr={'10px'}>
                     <Typography fontWeight={600}>{user?.fullName}</Typography>
                     <Typography fontSize={14}>@{user?.userName}</Typography>
                  </Box>
                  {user?.confirmed && <Check color='primary' />}
               </Box>
            </MenuItem>

            <Divider />
            <MenuItem>
               <ListItemIcon>
                  <Settings fontSize='small' />
               </ListItemIcon>
               Настройки
            </MenuItem>
            <MenuItem onClick={handleClickLogout}>
               <ListItemIcon>
                  <Logout fontSize='small' />
               </ListItemIcon>
               Выйти
            </MenuItem>
         </Menu>
      </Wrapper>
   )
}

export default UserSideProfile

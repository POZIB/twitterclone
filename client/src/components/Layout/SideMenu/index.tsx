import React from 'react'
import { Link } from 'react-router-dom'

import { IconButton, styled } from '@mui/material'
import { Twitter } from '@mui/icons-material'

import ListMenu from './ListMenu'
import UserSideProfile from './UserSideProfile'

const Wrapper = styled('header')(({ theme }) => ({
   maxWidth: '230px',
   paddingTop: '5px',
   position: 'sticky',
   top: 0,
   display: 'flex',
   flexDirection: 'column',
}))

const Logo = styled('div')(({ theme }) => ({
   padding: '10px',
   '& svg': {
      fontSize: '30px',
      color: theme.palette.primary.main,
   },
}))

const SideMenu: React.FC = (): React.ReactElement => {
   return (
      <Wrapper sx={{ alignItems: { lg: 'flex-start', xs: 'flex-end' } }}>
         <Logo>
            <Link to={'/home'}>
               <IconButton>
                  <Twitter />
               </IconButton>
            </Link>
         </Logo>
         <ListMenu />

         <UserSideProfile />
      </Wrapper>
   )
}

export default SideMenu

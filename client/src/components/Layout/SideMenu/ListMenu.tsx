import React from 'react'
import { useNavigate, Link } from 'react-router-dom'
import { Button, styled, Typography } from '@mui/material'
import {
   BookmarkBorderOutlined,
   BungalowOutlined,
   Create,
   ListAltOutlined,
   MarkunreadOutlined,
   NotificationsOutlined,
   PermIdentityOutlined,
   Search,
} from '@mui/icons-material'

import Modal from '../../Modal'
import FormAddTweet from '../../Tweet/FormAddTweet'
import { useTypedSelector } from '../../../hooks/useTypedSelector'
import { useDispatch } from 'react-redux'
import { setIsSumbitDeleteTweet } from '../../../store/tweets/actionCreators'

const List = styled('ul')(({ theme }) => ({
   listStyle: 'none',
   padding: 0,
   margin: 0,
}))

const ListItem = styled('li')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   '& a': {
      display: 'inline-flex',
      alignItems: 'flex-start',
      '&:hover': {
         textDecoration: 'none',
      },
   },
}))

const WrapperButton = styled('div')(({ theme }) => ({
   [theme.breakpoints.up('xl')]: {
      padding: '12px 20px',
   },
   [theme.breakpoints.down('xl')]: {
      padding: '12px',
   },
   display: 'flex',
   alignItems: 'center',
   borderRadius: '30px',
   height: '50px',
   marginBottom: '5px',
   position: 'relative',
   '&:hover': {
      backgroundColor: 'rgba(15, 20, 25, 0.1)',
      cursor: 'pointer',
      transition: 'background-color .1s ease-in-out',
   },

   '& svg': {
      fontSize: '30px',
   },
   '&  > span': {
      marginLeft: '15px',
   },
}))

const ContainerNotification = styled('div')(({ theme }) => ({
   display: 'flex',
   position: 'relative',
}))

const NotificationIcon = styled('div')(({ theme }) => ({
   minWidth: '16px',
   height: '16px',
   position: 'absolute',
   backgroundColor: theme.palette.primary.main,
   border: '1px solid #fff',
   borderRadius: '50%',
   top: '-4px',
   left: '16px',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
   boxSizing: 'content-box',
   '& span': {
      minWidth: '0px',
      fontSize: '11px',
      lineHeight: '12px',
      color: '#fff',
   },
}))

const TweetButton = styled('div')(({ theme }) => ({
   marginTop: '15px',
}))

const SideMenu: React.FC = (): React.ReactElement => {
   const dispatch = useDispatch()
   const { user } = useTypedSelector(state => state.auth)
   const { isSubmitAddTweet } = useTypedSelector(state => state.tweets)
   const { countUnreadLastSMS } = useTypedSelector(state => state.messages)
   const [isOpenModalNewTweet, setIsOpenModalNewTweet] = React.useState<boolean>(false)

   React.useEffect(() => {
      if (isSubmitAddTweet) {
         dispatch(setIsSumbitDeleteTweet(false))
         setIsOpenModalNewTweet(false)
      }
   }, [isSubmitAddTweet])

   const handleChangeModalNewTweet = (flag: boolean) => {
      setIsOpenModalNewTweet(flag)
   }

   return (
      <List>
         <ListItem>
            <Link to={'/home'}>
               <WrapperButton>
                  <ContainerNotification>
                     <BungalowOutlined />
                  </ContainerNotification>
                  <Typography
                     variant='menu'
                     sx={{ display: { xs: 'none', xl: 'block' } }}
                  >
                     Главная
                  </Typography>
               </WrapperButton>
            </Link>
         </ListItem>
         <ListItem>
            <Link to={''}>
               <WrapperButton>
                  <ContainerNotification>
                     <Search />
                  </ContainerNotification>
                  <Typography
                     variant='menu'
                     sx={{ display: { xs: 'none', xl: 'block' } }}
                  >
                     Поиск
                  </Typography>
               </WrapperButton>
            </Link>
         </ListItem>
         <ListItem>
            <Link to={''}>
               <WrapperButton>
                  <ContainerNotification>
                     <NotificationsOutlined />
                  </ContainerNotification>
                  <Typography
                     variant='menu'
                     sx={{ display: { xs: 'none', xl: 'block' } }}
                  >
                     Уведомления
                  </Typography>
               </WrapperButton>
            </Link>
         </ListItem>
         <ListItem>
            <Link to={'/messages'}>
               <WrapperButton>
                  <ContainerNotification>
                     <MarkunreadOutlined />
                     {countUnreadLastSMS && (
                        <NotificationIcon>
                           <span> {countUnreadLastSMS}</span>
                        </NotificationIcon>
                     )}
                  </ContainerNotification>
                  <Typography
                     variant='menu'
                     sx={{ display: { xs: 'none', xl: 'block' } }}
                  >
                     Сообщения
                  </Typography>
               </WrapperButton>
            </Link>
         </ListItem>
         <ListItem>
            <Link to={''}>
               <WrapperButton>
                  <ContainerNotification>
                     <BookmarkBorderOutlined />
                  </ContainerNotification>
                  <Typography
                     variant='menu'
                     sx={{ display: { xs: 'none', xl: 'block' } }}
                  >
                     Закладки
                  </Typography>
               </WrapperButton>
            </Link>
         </ListItem>
         <ListItem>
            <Link to={''}>
               <WrapperButton>
                  <ContainerNotification>
                     <ListAltOutlined />
                  </ContainerNotification>
                  <Typography
                     variant='menu'
                     sx={{ display: { xs: 'none', xl: 'block' } }}
                  >
                     Список
                  </Typography>
               </WrapperButton>
            </Link>
         </ListItem>
         <ListItem>
            <Link to={'/user/' + user?.userName}>
               <WrapperButton>
                  <ContainerNotification>
                     <PermIdentityOutlined />
                  </ContainerNotification>
                  <Typography
                     variant='menu'
                     sx={{ display: { xs: 'none', xl: 'block' } }}
                  >
                     Профиль
                  </Typography>
               </WrapperButton>
            </Link>
         </ListItem>
         <TweetButton>
            <Button
               variant='contained'
               fullWidth
               onClick={() => handleChangeModalNewTweet(true)}
            >
               <Typography
                  variant='inherit'
                  sx={{ display: { xs: 'none', xl: 'block' }, mr: '10px' }}
               >
                  Твитнуть
               </Typography>
               <Create sx={{ display: { xs: 'block', xl: 'none' } }} />
            </Button>
            <Modal
               maxWidth='sm'
               padding='0'
               isOpen={isOpenModalNewTweet}
               onClose={() => handleChangeModalNewTweet(false)}
            >
               <FormAddTweet />
            </Modal>
         </TweetButton>
      </List>
   )
}

export default SideMenu

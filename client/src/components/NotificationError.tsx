import React from 'react'
import { Snackbar, SnackbarProps } from '@mui/material'

interface IProps {
   message: string
}

const NotificationError: React.FC<IProps & SnackbarProps> = ({ message, ...other }) => {
   return (
      <Snackbar
         anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
         autoHideDuration={6200}
         message={message}
         {...other}
         sx={{ '& .MuiSnackbarContent-message': { flex: '1', textAlign: 'center' } }}
      />
   )
}

export default NotificationError

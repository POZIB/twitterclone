import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { useTypedSelector } from '../hooks/useTypedSelector'
import { privateRoutes, publicRoutes } from '../router'

import SendLinkResetPasswordModal from './Auth/SendLinkResetPasswordModal'
import Auth from '../pages/Auth'
import Layout from '../pages/Layout'
import SignInModal from './Auth/SignInModal'
import SignUpModal from './Auth/SignUpModal'
import ResetPasswordModal from './Auth/ResetPasswordModal'

const AppRouter: React.FC = () => {
   const { user } = useTypedSelector(state => state.auth)

   return (
      <>
         {user ? (
            <Layout>
               <Routes>
                  {publicRoutes.map(route => (
                     <Route
                        path={route.path}
                        element={<route.element />}
                        key={route.path}
                     />
                  ))}
               </Routes>
            </Layout>
         ) : (
            <Routes>
               <Route path='*' element={<Auth />} />
               <Route path='/i/flow' element={<Auth />}>
                  <Route path='password_reset' element={<SendLinkResetPasswordModal />} />
                  <Route
                     path='password_reset/confirmed'
                     element={<ResetPasswordModal />}
                  />
                  <Route path='sign_in' element={<SignInModal />} />
                  <Route path='sign_up' element={<SignUpModal />} />
               </Route>
            </Routes>
         )}
      </>
   )
}

export default AppRouter

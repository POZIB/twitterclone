import React, { useState } from 'react'
import { Dialog, DialogContent, DialogTitle, styled, IconButton } from '@mui/material'
import { Close } from '@mui/icons-material'

const ContainerTitle = styled('div')(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   padding: '6px',
}))

interface IProps {
   title?: string
   maxWidth?: 'xs' | 'sm' | 'md' | 'lg' | 'xl'
   maxHeight?: string
   padding?: string
   children?: React.ReactNode
   isOpen?: boolean
   onClose: () => void
}

const Modal: React.FC<IProps> = ({
   title,
   children,
   isOpen = false,
   onClose,
   padding,
   maxHeight,
   maxWidth = 'xs',
}): React.ReactElement => {
   return (
      <Dialog fullWidth maxWidth={maxWidth} open={isOpen} onClose={onClose}>
         <ContainerTitle>
            <IconButton color='inherit' onClick={onClose}>
               <Close />
            </IconButton>
            <DialogTitle variant='h6' fontWeight='500'>
               {title}
            </DialogTitle>
         </ContainerTitle>
         <DialogContent sx={{ padding, maxHeight }}>{children}</DialogContent>
      </Dialog>
   )
}

export default Modal

import React from 'react'

import data from '@emoji-mart/data'
import { Picker, PickerProps } from 'emoji-mart'

const EmojiPicker = (props: PickerProps | Readonly<PickerProps> | any) => {
   const ref = React.useRef() as React.MutableRefObject<HTMLInputElement>

   React.useEffect(() => {
      new Picker({ ...props, data, ref })
   }, [props])

   return <div ref={ref} />
}

export default EmojiPicker

import React from 'react'
import { Box, IconButton, InputBase } from '@mui/material'
import { Cancel, Search } from '@mui/icons-material'

interface IProps {
   variant?: 'outlined' | 'contained'
   value?: string
   onChange?: (value: string) => void
   onFocus?: (value: boolean) => void
   onClick?: (event: React.MouseEvent<HTMLDivElement>) => void
}

const SearchInput: React.FC<IProps> = ({
   variant,
   value,
   onChange,
   onFocus,
   onClick,
}): React.ReactElement => {
   const [focus, setFocus] = React.useState(false)

   const backgroundColor =
      (variant === 'outlined' && 'transparent') ||
      (variant === 'contained' && 'rgb(247, 249, 249)') ||
      'transparent'

   const borderColor =
      (variant === 'outlined' && 'rgb(207, 217, 222)') ||
      (variant === 'contained' && 'transparent') ||
      'rgb(207, 217, 222)'

   const handleChange = (
      e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
   ) => {
      if (onChange) {
         onChange(e.target.value)
      }
   }

   const handleFocus = () => {
      if (onFocus) {
         onFocus(true)
         setFocus(true)
      }
   }

   const handleBlur = () => {
      if (onFocus) {
         setTimeout(() => {
            onFocus(false)
            setFocus(false)
         }, 150)
      }
   }

   const handleClear = (e: React.MouseEvent<HTMLButtonElement>) => {
      if (onChange) {
         onChange('')
      }
   }

   return (
      <Box sx={{ overflow: 'hidden', p: '1px' }}>
         <Box
            sx={{
               padding: '0 6px',
               borderRadius: '30px',
               backgroundColor: backgroundColor,
               display: 'flex',
               alignItems: 'center',
               userSelect: 'none',
               border: '1px solid ',
               borderColor: borderColor,
               overflow: 'hidden',
               transition: 'border-color 0.18s, border-color 0.18s, box-shadow 0.18s',
               '&: hover': {
                  background: 'transparent',
                  borderColor: 'primary.main',
                  boxShadow: 'rgb(29 155 240) 0px 0px 0px 1px',
               },
               '&:focus-within': {
                  borderColor: 'primary.main',
                  boxShadow: 'rgb(29 155 240) 0px 0px 0px 1px',
               },
               '& input': {
                  padding: '0',
               },
            }}
         >
            <IconButton type='submit' sx={{ p: '3px' }}>
               <Search />
            </IconButton>

            <InputBase
               size='small'
               placeholder='Поиск '
               value={value}
               onChange={handleChange}
               onFocus={handleFocus}
               onBlur={handleBlur}
               onClick={onClick}
               sx={{ padding: '10px 10px', fontSize: '14px', width: '100%' }}
            />

            {value && focus && (
               <IconButton onClick={handleClear} sx={{ p: '0px' }}>
                  <Cancel />
               </IconButton>
            )}
         </Box>
      </Box>
   )
}

export default SearchInput

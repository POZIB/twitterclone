import React from 'react'

import { styled, InputBase, InputBaseProps } from '@mui/material'

const Wrapper = styled('div')(({ theme }) => ({
   '&:focus-within  .containerMaxLenght': {
      display: 'flex',
   },

   '&:focus-within .containerLabel': {
      paddingTop: '8px',
      lineHeight: '16px',
      fontSize: '12px',
   },
}))

const Body = styled('div')(({ theme }) => ({
   overflow: 'hidden',
   border: '1px solid rgb(207, 217, 222)',
   borderRadius: '4px',
   position: 'relative',
   transition: 'border-color 0.1s, box-shadow 0.1s',
}))

const ContainerMaxLenght = styled('div')(() => ({
   display: 'none',
   flex: '1',
   justifyContent: 'flex-end',
   padding: '8px 8px 0 ',
}))

const MaxLenght = styled('div')(() => ({
   boxShadow: 'none',
   color: 'rgb(83, 100, 113)',
}))

const ContainerInput = styled('div')(() => ({
   marginTop: '16px',
   padding: '12px 8px 8px',
   '& > div': {
      display: 'flex',
      alignItems: 'flex-start',
   },
   '& input': {
      padding: '0',
   },
}))

const InputText = styled(InputBase)(() => ({
   width: '100%',
   padding: '0',
   border: 'none',
}))

const ContainerTop = styled('div')(() => ({
   width: '100%',
   position: 'absolute',
   display: 'flex',
}))

const ContainerLabel = styled('div')(() => ({
   padding: '16px 8px 0 ',
   transition: 'padding 0.1s, font-size 0.1s, color 0.1s',
   color: 'rgb(83, 100, 113)',
}))

const ContainerError = styled('div')(() => ({
   color: '#ff2b2b',
   padding: '2px 8px 0 8px',
   fontSize: '13px',
   lineHeight: '16px',
}))

interface IProps {
   label?: string
   maxLenght?: number
   textArea?: boolean
   value?: string
   onChange?: (value: React.SetStateAction<string>) => void
   error?: boolean
   helperText?: string
   inputBaseProps?: InputBaseProps
}

const TextInput: React.FC<IProps> = ({
   label,
   maxLenght,
   value,
   onChange,
   error,
   helperText,
   inputBaseProps,
}) => {
   const LENGHT_VALUE = value?.length || (inputBaseProps?.value as string)?.length

   const handleChangeText = (
      event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
   ) => {
      if (!onChange) return

      if (maxLenght) {
         if (event?.target.value.length <= maxLenght) {
            onChange(event?.target.value)
         }
      } else {
         onChange(event?.target.value)
      }
   }

   return (
      <Wrapper
         sx={{
            '& label:focus-within > div': {
               borderColor: error ? 'rgb(244, 33, 46)' : 'primary.main',
               boxShadow: error
                  ? 'rgb(244, 33, 46) 0px 0px 0px 1px'
                  : 'rgb(29 155 240) 0px 0px 0px 1px',
            },
            '& label:focus-within  .containerLabel': {
               color: error ? 'rgb(244, 33, 46)' : 'primary.main',
            },
         }}
      >
         <label>
            <Body
               sx={{
                  borderColor: error ? 'rgb(244, 33, 46)' : 'rgb(207, 217, 222)',
               }}
            >
               <ContainerTop>
                  <ContainerLabel
                     className='containerLabel'
                     sx={{
                        pt: LENGHT_VALUE ? '8px' : '',
                        fontSize: LENGHT_VALUE ? '12px' : '17px',
                        lineHeight: LENGHT_VALUE ? '16px' : '',
                        color: error ? 'rgb(244, 33, 46)' : 'rgb(83, 100, 113)',
                     }}
                  >
                     <span>{label}</span>
                  </ContainerLabel>
                  {maxLenght && (
                     <ContainerMaxLenght className='containerMaxLenght'>
                        <MaxLenght>
                           {LENGHT_VALUE} /{maxLenght}
                        </MaxLenght>
                     </ContainerMaxLenght>
                  )}
               </ContainerTop>
               <ContainerInput>
                  <InputText
                     type='text'
                     value={value || ''}
                     onChange={handleChangeText}
                     {...inputBaseProps}
                  />
               </ContainerInput>
            </Body>
         </label>
         {error && <ContainerError>{helperText}</ContainerError>}
      </Wrapper>
   )
}

export default TextInput

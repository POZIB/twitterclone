import React from 'react'
import { styled } from '@mui/material'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   alignItems: 'center',
   border: '1px solid rgb(207, 217, 222)',
   borderRadius: '9999px',
   cursor: 'pointer',
   transition: 'background-color 0.2s, border-color 0.2s, color 0.2s',
   backgroundColor: 'transparent',
   color: 'rgb(15, 20, 25)',
   fontSize: '13px',
   '&:hover': {
      backgroundColor: 'rgb(243, 33, 46, 0.1)',
      borderColor: 'rgb(253, 201, 206)',
      color: 'rgb(244, 33, 46)',
   },
}))

const Content = styled('div')(() => ({
   minHeight: '32px',
   minWidth: '32px',
   display: 'flex',
   alignItems: 'center',
   fontSize: '14px',
   fontWeight: '700',
   lineHeight: '16px',
   padding: '0 16px',
}))

interface IProps {
   fontSize?: string
   onClick?: () => void
}

const ButtonUnFollow: React.FC<IProps> = ({ onClick, fontSize }) => {
   return (
      <Wrapper onClick={onClick} sx={{ fontSize }}>
         <Content>
            <span>Отписаться</span>
         </Content>
      </Wrapper>
   )
}

export default ButtonUnFollow

import React from 'react'
import { Link, To } from 'react-router-dom'

import { ArrowBack } from '@mui/icons-material'
import { IconButton } from '@mui/material'

interface IProps {
   margin?: string
   padding?: string
}

const GoBackButton: React.FC<IProps> = ({ margin, padding }): React.ReactElement => {
   return (
      <>
         <Link to={-1 as To}>
            <IconButton color='primary' sx={{ margin: { margin }, padding: { padding } }}>
               <ArrowBack />
            </IconButton>
         </Link>
      </>
   )
}

export default GoBackButton

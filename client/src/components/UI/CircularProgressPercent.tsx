import React from 'react'
import { Box, CircularProgress } from '@mui/material'

interface IProps {
   valuePercent: number
}

const CircularProgressProcent: React.FC<IProps> = (props): React.ReactElement => {
   const getColorByPercentage = (): string => {
      if (props.valuePercent >= 50 && props.valuePercent <= 90) {
         return '#fc9803'
      } else if (props.valuePercent >= 80) {
         return '#fc0303'
      } else {
         return 'fff'
      }
   }
   const colorProgress = React.useMemo(() => getColorByPercentage(), [props])

   return (
      <Box sx={{ position: 'relative', display: 'flex', alignItems: 'center' }}>
         <CircularProgress
            variant='determinate'
            sx={{
               color: theme => theme.palette.grey[300],
            }}
            size={15}
            value={100}
         />
         <CircularProgress
            variant='determinate'
            value={props.valuePercent}
            sx={{
               color: colorProgress,
               position: 'absolute',
               left: 0,
            }}
            size={15}
         />
      </Box>
   )
}

export default CircularProgressProcent

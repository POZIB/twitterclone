import { styled } from '@mui/material'
import React from 'react'

const Wrapper = styled('div')(() => ({
   display: 'flex',
   alignItems: 'center',
   borderRadius: '9999px',
   border: '1px solid',
   backgroundColor: 'rgb(15, 20, 25)',
   transition: ' background-color 0.2s',
   color: 'rgb(255, 255, 255)',
   cursor: 'pointer',
   fontSize: '13px',
   '&:hover': {
      backgroundColor: 'rgb(39, 44, 48)',
   },
}))

const Content = styled('div')(() => ({
   minHeight: '32px',
   display: 'flex',
   alignItems: 'center',
   fontWeight: '700',
   padding: '0 16px',
}))

interface IProps {
   fontSize?: string
   onClick?: () => void
}

const ButtonFollow: React.FC<IProps> = ({ onClick, fontSize }) => {
   return (
      <Wrapper onClick={onClick} sx={{ fontSize }}>
         <Content>
            <span>Читать</span>
         </Content>
      </Wrapper>
   )
}

export default ButtonFollow

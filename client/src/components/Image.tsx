import React from 'react'
import { styled } from '@mui/material'
// import noimage from '../assets/images.png'

const Wrapper = styled('div')(({ theme }) => ({
   height: '100%',
}))

const Item = styled('div')(({ theme }) => ({
   // display: 'inline-block',
   backgroundSize: 'cover',
   backgroundRepeat: 'no-repeat',
   backgroundPosition: 'center center',
   // '&:before': { content: '" "' },
   // '&:before': { content: 'url(' + src + ')' }
}))

const Img = styled('img')(({ theme }) => ({
   height: '100%',
   objectFit: 'cover',
}))

interface IProps {
   src?: string
}

const Image: React.FC<IProps> = ({ src }) => {
   return (
      <Wrapper>
         <Img src={src} />
         {/* <Item sx={{ background: `url(${src})` }} /> */}
         {/* <Img sx={{ '&:before': { content: 'url(' + String(noimage) + ')' } }} /> */}
      </Wrapper>
   )
}

export default Image
